--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_rast_total_clip(integer, regclass)

-- DROP FUNCTION @extschema@.fn_get_rast_total_clip(integer, regclass);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass)
RETURNS TABLE (
	ldsity_type integer,
	attribute integer,
	total float
)
AS
$function$
BEGIN
RETURN QUERY EXECUTE
'
WITH
	-- cutting geometry on the fly by 50x50 inspire grid
	w_cell AS MATERIALIZED (
		SELECT
			t2.gid,
			ST_Intersection(t1.geom, t2.geom) AS geom
		FROM
			@extschema@.f_a_cell AS t1
		INNER JOIN
			@extschema@.f_a_cell AS t2
		ON ST_Intersects(t1.geom,t2.geom)
		WHERE
			t1.gid = $1 AND
			t2.cell_type = 300
	),
	-- clipping raster to the geometry area
	w_clip AS MATERIALIZED (
			SELECT
				t2.rid,
				ST_Clip(t2.rast, NULL::integer[], t1.geom, ARRAY[255,255,255,255], TRUE) AS rast
			FROM 
				w_cell AS t1,
				'|| table_name ||' AS t2
			WHERE
				-- make sure that previous intersection is polygon, not only point or linestring
				(ST_GeometryType(t1.geom) = ''ST_Polygon'' OR
				ST_GeometryType(t1.geom) = ''ST_MultiPolygon'') AND
				ST_Intersects(t1.geom, t2.rast)
		)
		-- reclassification of raster and dump bands to separate rasters
		, w_raster_reclass AS MATERIALIZED (
			SELECT
				rid,
				ldsity_type,
				attribute,
				ST_Reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, 255) rast
			FROM
				(SELECT
				   	t1.rid, t1.rast, t2.band_number
				FROM
				   w_clip AS t1
				CROSS JOIN
					-- only  4 bands exists
					unnest(ARRAY[1,2,3,4]) AS t2(band_number)
				)AS t1
			INNER JOIN
				-- table of desired ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
				(
				SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
					UNION ALL
				SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
					UNION ALL
				SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
					UNION ALL
				SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
				) AS t2
			ON
				t1.band_number = t2.band_number
		)
		, w_value_count AS MATERIALIZED (
			-- calculation of present values in raster
			SELECT
				rid,
				ldsity_type,
				attribute,
				(ST_ValueCount(rast, 1, true)) AS val_count,
				(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
			FROM
				w_raster_reclass
			ORDER BY rid, ldsity_type, attribute
		)
		-- summation of values
		SELECT
			ldsity_type,
			attribute,
			sum(pixarea * (val_count).value * (val_count).count)
		FROM
			  w_value_count
		GROUP BY
			ldsity_type,
			attribute
		ORDER BY
			ldsity_type,
			attribute'
			USING cell_gid;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Clip (pixel belongs to the geometry if the geometry covers at least 50% of it).';
