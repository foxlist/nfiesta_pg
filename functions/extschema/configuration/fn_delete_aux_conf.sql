--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_aux_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_aux_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_aux_conf(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_g_beta
DELETE FROM @extschema@.t_g_beta WHERE aux_conf = $1;
RAISE NOTICE 'Deleting t_g_betas...';
-- t_panel2aux_conf
DELETE FROM @extschema@.t_panel2aux_conf WHERE aux_conf = $1;
RAISE NOTICE 'Deleting panel to auxiliary configuration mapping...';
-- t_total_estimate_conf
PERFORM	@extschema@.fn_delete_total_estimate_conf(id)
FROM	@extschema@.t_total_estimate_conf
WHERE	aux_conf = $1;

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.t_aux_conf WHERE id = $1;
RAISE NOTICE 'Deleting auxiliary configuration = %.', $1;

RETURN;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_aux_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';
