--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _aux_conf integer, _force_synthetic boolean default False)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
BEGIN

-- test for existing g_betas
-- otherwise the configuration cannot be done (sometimes the g_betas cannot be computed)
-- so this prevents to configure non-computable estimates

IF (SELECT count(*) FROM @extschema@.t_g_beta WHERE aux_conf = $6) = 0
THEN
	RAISE EXCEPTION 'G-betas for required aux_conf are not available. The computation of it was not run or is not able to compute (mostly the problem of matrix inversion).';
END IF;

-- create the label of estimate
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $6);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $6);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- reset sequence (usually when previous atempt to configure fails, the sequence remains shifted)
PERFORM setval('@extschema@.t_total_estimate_conf_id_seq', (SELECT coalesce(max(id),0)+1 FROM @extschema@.t_total_estimate_conf), FALSE);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type, force_synthetic, aux_conf)
VALUES
	($1, $2, $3, concat('2p;T=',_target_label,';Cell=',_cell,';PA_code=',_param_area_code, ';model=',_model,_note), $5, 2, $7, $6)
RETURNING id
INTO _total_estimate_conf;

-- test on param_area_coverage
	SELECT
		array_agg(t1.id ORDER BY t1.id)
	FROM
		@extschema@.t_stratum AS t1
	INNER JOIN
		@extschema@.f_a_param_area AS t2
	ON
		-- buffered stratum?
		-- no, if only buffer of the stratum would intersect the cell, 
		-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
		ST_Intersects(t1.geom, t2.geom)
	WHERE
		t2.gid = _param_area
	INTO _stratas;

	IF _stratas IS NULL
	THEN
		RAISE EXCEPTION 'The specified cell is not intersected by any stratum. Choose another estimation cell.';
	END IF;

-- existing panels configured in panel2aux_conf
	SELECT
		array_agg(panel ORDER BY panel)
	FROM
		@extschema@.t_panel2aux_conf AS t1
	WHERE
		t1.aux_conf = $6
	INTO _panels_aux;


-- check of panel2total_2ndph
-- and addition of panels from param_area - is the target variable available not only in cell?

	WITH w_data AS MATERIALIZED (
		SELECT
			t1.id AS stratum, t2.id AS panel, t6.reference_year_set, count(*) AS total
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.t_panel AS t2
		ON
			t1.id = t2.stratum
		INNER JOIN
			@extschema@.cm_cluster2panel_mapping AS t3
		ON
			t2.id = t3.panel
		INNER JOIN
			@extschema@.t_cluster AS t4
		ON
			t3.cluster = t4.id
		INNER JOIN
			@extschema@.f_p_plot AS t5
		ON
			t4.id = t5.cluster
		INNER JOIN
			@extschema@.t_target_data AS t6
		ON
			t5.gid = t6.plot
		INNER JOIN
			@extschema@.t_variable AS t7
		ON
			t6.target_variable = t7.target_variable AND
			CASE WHEN t6.area_domain_category IS NULL THEN t7.area_domain_category IS NULL
			ELSE t6.area_domain_category = t7.area_domain_category END AND
			CASE WHEN t6.sub_population_category IS NULL THEN t7.sub_population_category IS NULL
			ELSE t6.sub_population_category = t7.sub_population_category END
		INNER JOIN
			@extschema@.t_reference_year_set AS t8
		ON
			t6.reference_year_set = t8.id
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t9
		ON
			t2.id = t9.panel AND
			t8.id = t9.reference_year_set
		WHERE
			array[t1.id] <@ _stratas AND
			t7.id = $5 AND 
			(t8.reference_date_begin >= $2 AND
			t8.reference_date_end <= $3)
		GROUP BY
			t1.id, t2.id, t6.reference_year_set
	)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel ORDER BY panel) AS panels,
		array_agg(reference_year_set ORDER BY panel) AS refyearsets
	FROM
		(SELECT
			stratum, panel, reference_year_set,
			total,
			max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
		FROM
			w_data
		) AS t1
	WHERE
		-- pick up the most dense panel with target variable
		total = max_total
	INTO _stratas_wp, _panels, _refyearsets;

	IF _panels != _panels_aux OR _panels IS NULL
	THEN
		RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable! total_estimate_conf: %, panels: %, panels_aux: %', _total_estimate_conf, _panels, _panels_aux;
	END IF;

-- insert into table t_panel2total_2ndph_estimate_conf
INSERT INTO @extschema@.t_panel2total_2ndph_estimate_conf (total_estimate_conf, panel, reference_year_set)
SELECT
	_total_estimate_conf, panel, reference_year_set
FROM
	unnest(_panels) WITH ORDINALITY AS t1(panel, id)
INNER JOIN
	unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
ON
	t1.id = t2.id;

-- insert into table t_estimate_conf
INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
SELECT 1, _total_estimate_conf, NULL;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';
