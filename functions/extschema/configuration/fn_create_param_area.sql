--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_create_param_area(integer, regclass)
--DROP FUNCTION @extschema@.fn_create_param_area(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_create_param_area(_param_area_type integer, _estimation_cells integer[], _param_area_code varchar DEFAULT NULL)
RETURNS integer
AS
$function$
DECLARE
	_param_area integer;
	_not_known integer[];
BEGIN
	-- find out, if there are estimation cells given in parameter
	_not_known := (
		SELECT array_agg(estimation_cell ORDER BY estimation_cell)
		FROM
			(SELECT unnest(_estimation_cells) AS estimation_cell
			EXCEPT
			SELECT id FROM @extschema@.c_estimation_cell) AS t1
	);

	-- if there are not -> exception
	IF _not_known IS NOT NULL
	THEN
		RAISE EXCEPTION 'Estimation cell does not exist in lookup c_estimation_cell (%)!', _not_known;
	END IF;

	-- insert new param area with unioned geometry
	INSERT INTO @extschema@.f_a_param_area (param_area_type, param_area_code, geom)
	SELECT
		_param_area_type,
		CASE
		WHEN _param_area_code IS NOT NULL THEN _param_area_code
		ELSE string_agg(label,'+')
		END AS param_area_code,
		ST_Multi(ST_union(geom))
	FROM
		@extschema@.f_a_cell AS t1
	INNER JOIN @extschema@.c_estimation_cell AS t2
	ON	t1.estimation_cell = t2.id
	WHERE
		_estimation_cells @> array[estimation_cell]
	RETURNING gid
	INTO _param_area;

	-- insert mapping between cells and param_area
	INSERT INTO @extschema@.cm_cell2param_area_mapping(estimation_cell, param_area)
	SELECT unnest(_estimation_cells), _param_area;

	-- insert mapping between plots and param_area
	INSERT INTO @extschema@.cm_plot2param_area_mapping(plot, param_area)
	SELECT
		plot, _param_area
	FROM
		@extschema@.cm_plot2cell_mapping
	WHERE
		array[estimation_cell] <@ _estimation_cells;

RETURN _param_area;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_create_param_area(_param_area_type integer, _estimation_cells integer[], _param_area_code varchar) IS 'Function for creation of new parametrization area from given estimation cells.';
