--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_param_area(integer)
--DROP FUNCTION @extschema@.fn_delete_param_area(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_param_area(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_aux_conf
PERFORM	@extschema@.fn_delete_aux_conf(id)
FROM	@extschema@.t_aux_conf
WHERE	param_area = $1;

-- cm_cell2param_area_mapping
DELETE FROM @extschema@.cm_cell2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting cell to parametrization area mapping...';
-- cm_plot2param_area_mapping
DELETE FROM @extschema@.cm_plot2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting plot to parametrization area mapping...';

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.f_a_param_area WHERE gid = $1;
RAISE NOTICE 'Deleting parametrization area = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_param_area(integer) IS 'Function for deleting the parametrization area and all records in referenced tables.';

