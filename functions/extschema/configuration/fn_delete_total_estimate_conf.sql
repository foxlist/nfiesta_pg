--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_total_estimate_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_total_estimate_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_total_estimate_conf(_total_estimate_conf integer)
RETURNS void
AS
$function$
DECLARE
_estimate_conf integer[];
BEGIN

-- 3rd order tables
-- t_result
DELETE FROM @extschema@.t_result WHERE estimate_conf IN (
	SELECT 	id
	FROM 	@extschema@.t_estimate_conf
	WHERE	total_estimate_conf = $1 OR
		denominator = $1)
	;

-- 2nd order tables
-- t_panel2total_1stph_estimate_conf, 2ndph
DELETE FROM @extschema@.t_panel2total_2ndph_estimate_conf WHERE total_estimate_conf = $1;
DELETE FROM @extschema@.t_panel2total_1stph_estimate_conf WHERE total_estimate_conf = $1;
RAISE NOTICE 'Deleting panel to total_estimate_conf mapping...';
-- t_estimate_conf
WITH w AS MATERIALIZED (DELETE FROM @extschema@.t_estimate_conf WHERE total_estimate_conf = $1 OR denominator = $1 RETURNING id)
SELECT array_agg(id) FROM w INTO _estimate_conf;
RAISE NOTICE 'Deleting estimate configurations (%).', _estimate_conf;

-- 1st order table
-- t_total_estimate_conf
DELETE FROM @extschema@.t_total_estimate_conf WHERE id = $1;
RAISE NOTICE 'Deleting total estimate configuration = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_total_estimate_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';
