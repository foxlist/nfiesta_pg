--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_2p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data AS MATERIALIZED (
		select * from @extschema@.fn_2p_data(' || conf_id || ') where is_target
	)
	, w_I AS MATERIALIZED (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data
                order by r, c
	)
	, w_SIGMA AS MATERIALIZED (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data
		order by r, c
	)
	, w_PI AS MATERIALIZED (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data
                order by r, c
	)
	, w_SIGMA_PI AS MATERIALIZED (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA as A inner join w_PI as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T AS MATERIALIZED (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data
                order by r, c
	)
	, w_total_1p AS MATERIALIZED (
                with w_I_PI AS MATERIALIZED (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I      AS A 
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI as A, w_Y_T as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta AS MATERIALIZED (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data
	)
	, w_correction_2p AS MATERIALIZED (
                with w_DELTA_T__G_beta_PI AS MATERIALIZED (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta AS A
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI as A, w_Y_T as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p AS MATERIALIZED (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p 		AS A
                inner join  w_correction_2p    	AS B    on (A.c = B.c)
		order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESIDUALS----------------------------------
-------------------------------------------------------------------------------
	, w_e AS MATERIALIZED (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data
                order by r, c
	)
	, w_I_e AS MATERIALIZED (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e AS MATERIALIZED (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_PHI AS MATERIALIZED (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e        		AS A
                inner join  w_DELTA_T__G_beta__e  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
	, w_zeroResidualsTotalTest AS MATERIALIZED (
                select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                        w_SIGMA_PI as A, (select c as r, r as c, val_d_plus from w_e) as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_phi AS MATERIALIZED (
	select w_data.*, w_PHI.val as phi from w_data inner join w_PHI on (cluster = c and attribute = r)
) 
, w_data_agg AS MATERIALIZED (
        SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(phi order by cluster) as residuals,
		array_agg(sweight order by cluster) as sweights
        FROM
            w_data_phi
        group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1p_stratum AS MATERIALIZED (
        SELECT 
		stratum, attribute, (htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est1p AS MATERIALIZED (
	SELECT
	    attribute, sum(total) as point1p, sum(var) AS var1p 
	from w_est1p_stratum group by attribute order by attribute
)
, w_est2p_stratum AS MATERIALIZED (
        SELECT 
		stratum, attribute, (htc_compute(cids, residuals, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est2p AS MATERIALIZED (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	from w_est2p_stratum group by attribute order by attribute
)	
, w_units_json AS MATERIALIZED (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		select
			stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data
		group by stratum, attribute
		order by stratum, attribute
	) as t 
)	
select 
	w_est1p.attribute, 
	point1p,  var1p,
	val as point2p, var2p,
	s_units
from w_est1p
inner join w_est2p on (w_est1p.attribute = w_est2p.attribute) 
inner join w_total_2p on (w_est2p.attribute = w_total_2p.c),
w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p_total_var(integer) IS 'Function computing total and variance using regression estimate.';
