--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_rast_total_intersection(integer, regclass)

-- DROP FUNCTION @extschema@.fn_get_rast_total_intersection(integer, regclass);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass)
RETURNS TABLE (
	ldsity_type integer,
	attribute integer,
	total float
)
AS
$function$
BEGIN
RETURN QUERY EXECUTE '
WITH w_cell_raster_reclass AS MATERIALIZED (
		SELECT
			t1.rid,
			t1.gid,
			t2.ldsity_type,
			t2.attribute,
			t1.geom,
			covers,
			-- all rasters have only 1 band, the original one was expanded
			-- reclass all, case when noreclass was slowing down the calculation
			ST_reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, NULL) rast
		FROM
			-- rasters covering whole calculated cell
			(SELECT
				t1.rid,
				t2.gid,
				t2.geom,
				t3.band AS band_number,
				ST_Covers(t2.geom, ST_Convexhull(t1.rast)) AS covers,
				-- expand raster bands into separate rasters
				--ST_Band(t1.rast,t3.band) AS rast
				t1.rast
			FROM					
				'|| table_name ||' AS t1
			INNER JOIN
				(
				SELECT
					gid,
					geom
				FROM
					@extschema@.f_a_cell
					--order by gid
				WHERE
					gid = $1
				) AS t2
			ON
				ST_Intersects(t2.geom, t1.rast)
			CROSS JOIN
				-- only  4 bands exists
				unnest(ARRAY[1,2,3,4]) AS t3(band)
			) t1
		INNER JOIN
		-- table of disared ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
			(
			SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
				UNION ALL
			SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
				UNION ALL
			SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
				UNION ALL
			SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
			) AS t2
		ON
			t1.band_number = t2.band_number
	-- rasters which are not fully within the cell
	),w_not_covers AS MATERIALIZED (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_Intersection(rast, geom)) AS intersection,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			NOT covers
		)
	-- rasters which are fully within the cell
	,w_covers AS MATERIALIZED (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_ValueCount(rast, 1, true)) AS val_count,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			covers
	)
	--INSERT INTO @extschema@.t_aux_total(cell, ldsity_type, attribute, aux_total)
	SELECT
		ldsity_type,
		attribute,
		coalesce(sum(rid_sum), 0)	-- 1 min 30 sec
	FROM 
		(
		-- sum of values for each raster covering the cell
		-- speed up solution for rasters completely within cell (no need for st_intersection)
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * count * val)
			FROM 
				(SELECT
					(val_count).value AS val,
					(val_count).count AS count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
				) AS t1
			) AS rid_sum
		FROM
			w_covers
		UNION ALL
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * val)
			FROM
				(SELECT
					(intersection).val AS val,
					ST_Area((intersection).geom) / 10000 AS pixarea
				) AS t2
			) AS rid_sum
		FROM
			w_not_covers		
		) AS w_rid_sum
	GROUP BY
		ldsity_type,
		attribute
	ORDER BY
		ldsity_type,
		attribute' USING cell_gid;
END;
$function$

LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Intersection (pixel is cutted by geometry boundary and only its exact portion belongs to the geometry).';
