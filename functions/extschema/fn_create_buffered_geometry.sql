--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_create_buffered_geometry
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon), integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION
@extschema@.fn_create_buffered_geometry
(
	_geom			geometry(MultiPolygon),
	_tract_type		integer,
	_point_distance_m	integer
)

RETURNS geometry(MultiPolygon)
AS
$$
DECLARE
	_geom_buff		geometry(MultiPolygon);
	_axis_shift		double precision;
BEGIN

	CASE
	WHEN _tract_type = 100
	THEN
		 RETURN ST_Multi(ST_Buffer(_geom,_point_distance_m));

	WHEN _tract_type = 200
	THEN
		_axis_shift := ( (_point_distance_m^2) / 2.0 )^(1/2.0);


		WITH w_only_shifted AS MATERIALIZED (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu, na opacnou stratnu, nez se vytvari trakt
					ST_Translate(_geom, -_axis_shift, -_axis_shift)
				) AS geom
			)
		,w_original_points AS MATERIALIZED (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS MATERIALIZED (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS MATERIALIZED (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS MATERIALIZED (
			SELECT
				path,
				x, y, geom,
				x-_point_distance_m AS x_shift,
				y-_point_distance_m AS y_shift,
				ST_SetSRID(ST_MakePoint(x-_axis_shift, y-_axis_shift), st_srid(geom)) AS geom_shift
			FROM
				w_coordinates
		)
		,w_make_line AS MATERIALIZED (
			SELECT
				ST_Union(ST_MakeLine(geom,geom_shift)) AS line
			FROM 
				w_shift_points
			)
		,w_union_all AS MATERIALIZED (
			SELECT
				ST_Union(t1.line, ST_Boundary(t2.geom)) AS all_lines
			FROM
				w_make_line AS t1,
				w_only_shifted AS t2
			)
		SELECT
			ST_Multi(ST_buildarea(all_lines))
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;

	WHEN _tract_type = 300
	THEN

		WITH w_only_shifted AS MATERIALIZED (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu doleva
					ST_Translate(_geom, -(x_shift*_point_distance_m), -(y_shift*_point_distance_m))
				) AS geom_unioned
			FROM
				(SELECT unnest(ARRAY[1,1,0]) AS x_shift, unnest(ARRAY[0,1,1]) AS y_shift) AS t1
			)
		,w_union_only_shifted AS MATERIALIZED (
			SELECT ST_Union(ST_MakeValid(geom_unioned)) AS geom_unioned
			FROM w_only_shifted
		)
		,w_original_points AS MATERIALIZED (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS MATERIALIZED (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS MATERIALIZED (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS MATERIALIZED (
			SELECT
				path,
				geom,
				ST_SetSRID(ST_MakePoint(x-(x_shift[1]*_point_distance_m), y-(y_shift[1]*_point_distance_m)),st_srid(geom)) AS geom_shift_l,
				ST_SetSRID(ST_MakePoint(x-(x_shift[2]*_point_distance_m), y-(y_shift[2]*_point_distance_m)),st_srid(geom)) AS geom_shift_ld,
				ST_SetSRID(ST_MakePoint(x-(x_shift[3]*_point_distance_m), y-(y_shift[3]*_point_distance_m)),st_srid(geom)) AS geom_shift_d
			FROM
				w_coordinates AS t1,
				(SELECT ARRAY[1,1,0] AS x_shift, ARRAY[0,1,1] AS y_shift) AS t2
		)
		,w_make_line AS MATERIALIZED (
			-- make the square from 4 lines
			SELECT	ST_MakeLine(geom,geom_shift_l) AS line			FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_l,geom_shift_ld) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_ld,geom_shift_d) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_d,geom) AS line			FROM w_shift_points
			-- diagonal line to prevent st_buildarea to exclude squares
			UNION ALL
			SELECT	ST_MakeLine(geom,geom_shift_ld) AS line			FROM w_shift_points
		)
		,w_union_lines AS MATERIALIZED (
			SELECT ST_Collect(line) AS line FROM w_make_line
		)
		,w_area_lines AS MATERIALIZED (
			SELECT
					ST_BuildArea(line) as area_lines
			FROM
					w_union_lines
		)
		,w_area_geoms AS MATERIALIZED (
			SELECT
				ST_BuildArea(ST_MakeValid(geom_unioned)) AS area_geoms
			FROM
				w_union_only_shifted
		),
		w_union_all AS MATERIALIZED (
			SELECT
				ST_union(t1.area_lines,t2.area_geoms) AS geom
			FROM
				w_area_lines AS t1,
				w_area_geoms AS t2
		)
		SELECT
			ST_Multi(geom)
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;
	ELSE
		RAISE EXCEPTION 'Not known tract type!';
	END CASE;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon), integer, integer) IS
'Functions generates a buffered geometry of original geographical domain for specified tract_type.
100 -- 2p, 1p rotated -- Two points, the second is rotated around the first.
200 -- 2p, no rotation -- N-E, Two points, no rotation, the second located North-East.
300 -- 4p in square, N-E -- Four points in square shape, no rotation. North, North-East, Eeast locations.
';
