--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data AS MATERIALIZED (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
	, w_data_agg AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum AS MATERIALIZED (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).*
		from w_data_agg
	)
	, w_est1p AS MATERIALIZED (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p
		from w_est1p_stratum group by attribute order by attribute
	)
	, w_units_json AS MATERIALIZED (
		select array_to_json(array_agg(row_to_json(t))) as s_units from
		(
			select
				stratum, attribute,
				count(*) as s_units_param_area, 
				sum(cluster_is_in_cell::int) as s_units_cell,
				sum((ldsity_d != 0)::int) as s_units_cell_nonzero
			from w_data
			group by stratum, attribute
			order by stratum, attribute
		) as t 
	)	
	select 
	w_est1p.attribute, 
	point1p,  var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	s_units
	from w_est1p
	, w_units_json
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS 'Function computing total and variance using regression estimate.';
