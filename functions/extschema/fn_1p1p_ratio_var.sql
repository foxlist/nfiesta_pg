--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf AS MATERIALIZED (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom AS MATERIALIZED (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS MATERIALIZED (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS MATERIALIZED (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom AS MATERIALIZED (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS MATERIALIZED (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS MATERIALIZED (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS MATERIALIZED (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS MATERIALIZED (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS MATERIALIZED (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS MATERIALIZED (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS MATERIALIZED (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS MATERIALIZED (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_units_json AS MATERIALIZED (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		(select
			''nominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		union all
		(select
			''denominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
	) as t
)	
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	s_units
from w_1p_ratio_var
, w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';
