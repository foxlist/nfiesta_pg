--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_get_estimation_cell(integer, regclass)
--DROP FUNCTION @extschema@.fn_get_estimation_cell(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_estimation_cell(estimation_cell_collection integer)
RETURNS TABLE (
	estimation_cell integer,
	geom 		geometry(Polygon)
) AS
AS
$function$
DECLARE
_cell				varchar;
BEGIN

RETURN QUERY
SELECT
	estimation_cell, t2.label, ST_Multi(geom)
FROM
	@extschema@.f_a_cell AS t1
INNER JOIN
	@extschema@.c_estimation_cell AS t2
ON
	t1.estimation_cell = t2.id
WHERE
	t2.estimation_cell_collection = $1
GROUP BY
	estimation_cell, label
ORDER BY
	estimation_cell, label;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_get_estimation_cell() IS '.';
