--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_inverse(double precision[])

-- DROP FUNCTION @extschema@.fn_inverse(double precision[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_inverse(input double precision[][])
  RETURNS double precision[][] AS
$BODY$
	#import numpy as np
	from numpy import matrix

	#test to singularity
	# if the rank of matrix is the same value as the row dimension (no 0 rows or columns, rows linearly independent)
	# than it should be able to compute inversion

	#def is_invertible(a):
	#	return a.shape[0] == a.shape[1] and np.linalg.matrix_rank(a) == a.shape[0]	

	#if is_invertible(a):

	#test deprecated
	#matrix_rank uses svd decomposition but np.linalg.inv(a) uses LU factorization (LAPACK)

	a = matrix(input)
	try:
		inversed=a.I.tolist()
	except Exception:
		plpy.info("not able to compute inversion of matrix", input)
		return None
	return list(inversed)

	#else:
	#	raise ValueError('Input matrix is singular, inversion of it does not exist!')

$BODY$
  LANGUAGE plpython3u VOLATILE STRICT
  COST 100;

COMMENT ON FUNCTION @extschema@.fn_inverse(double precision[][]) IS 'Function for computing inverse of matrix using python3.';
--------------TEST
/*
select @extschema@.fn_inverse ('{{222477.383519094,7489538.83305858},{7489538.83305858,537848864.980823}}');
-- "{{8.46126431190308e-006,-1.17823001528562e-007},{-1.17823001528562e-007,3.49994221042523e-009}}"
*/
