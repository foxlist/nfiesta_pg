--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_make_estimate(integer)

-- DROP FUNCTION @extschema@.fn_make_estimate(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_make_estimate(
    _estimate_conf integer
)
  RETURNS TABLE(estimate_conf integer) AS
$BODY$
BEGIN

RETURN QUERY EXECUTE
	'WITH w_res AS (
		SELECT * FROM @extschema@.fn_make_estimate_table($1)
	)
	INSERT INTO @extschema@.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, sampling_units)
	SELECT
		estimate_conf,
		point,
		var,
		version,
		calc_started,
		calc_duration,
		est_info
	FROM w_res
	RETURNING estimate_conf'
	USING _estimate_conf;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
;

COMMENT ON FUNCTION @extschema@.fn_make_estimate(integer) IS 'Wrapper function for fn_make_estimate with storage of results';
