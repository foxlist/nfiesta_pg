\name{fnTractLocalDensity}
\alias{fnTractLocalDensity}
\title{Cluster level local densities for parametrization area}
\description{}
\usage{fnTractLocalDensity(data, aux = NA)}
\arguments{
	\item{data}{Input data (list of data.frames obtained for estimate identifier by function fnSelectData)}
	\item{aux}{Auxiliary variable identifier, default NA value codes main variable}
}
\details{}
\value{
Function returns cluster level local densities as data.frame with columns:
	\item{cluster_cid}{Composite cluster identifier (cluster identifier + panel identifier)}
	\item{ytP}{Cluster level local density for parametrization area}
	\item{ytD}{Cluster level local density for estimation cell}
	\item{k}{Number of inventory plots of the cluster}
	\item{m}{Number of inventory plots of the cluster which are located inside parametrization area}
	\item{insideP}{Boolean - cluster is inside parametrization area, when at least one of its plots is inside parametrization area}
	\item{insideD}{Boolean - cluster is inside estimation cell, when at least one of its plots is inside estimation cell}
	\item{ItP}{Indicator value - (0 - cluster is outside parametrization area, 1- cluster is inside parametrization area)}
	\item{ItD}{Indicator value - (0 - cluster is outside estimation cell, 1- cluster is inside estimation cell)}
	\item{cell_id}{List of cells hits by cluster plots (text divided by pipe character)}
	\item{plot_id}{List of cluster plots (text divided by pipe character)}
}
\references{Adolt,R., Fejfar,J., Lanz,A. 2019. nFIESTA (new Forest Inventory ESTimation and Analysis) Estimation methods}
\author{\packageAuthor{nfiesta}}
\note{}
\seealso{}
\examples{
	head ( fnTractLocalDensity(data = fnSelectData(data = NFiestaData, estimate =   533)), n = 20);
}