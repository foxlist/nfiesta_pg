#
# Copyright 2017, 2020 ÚHÚL
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.
#

fnClusterLocalDensity <-
function(data, stratum = NA, aux = NA)
{
#Function parameters:
#data    <- fnSelectData(data = NFiestaData, estimate = 1);# Input data (list of data.frames obtained for estimate identifier by function fnSelectData)
#stratum <- 2;# Stratum identifier, default NA value determines cluster level local densities computation for whole estimation cell (without intersection with strata)
#aux     <- NA;# Auxiliary variable identifier, default NA value codes main variable

#rm (data, stratum, aux);
# rm (agg, plots, result, vals);

# Main variable plot level local densities
if ( is.na(aux) ) {
vals <- data$Target; }
# Auxiliary variable plot level local densities
else {
vals <- data$Aux[data$Aux$var_id == aux,];
vals <- vals[,c("plot_id", "ldsity")];
}

# This codes merges inventory plot list with its plot level local densities (by plot identifier)
plots <- data$Plots;
plots <- merge(plots, vals, by = "plot_id", all.x = TRUE);
if ( any(is.na(plots$ldsity)) ) { warning ("There are some plot level local densities missing for some plots!"); }

# This code calculates cluster level local densities for estimation cell and stratum intersection
if ( !is.na(stratum) ) {
plots$IS <- ifelse(plots$stratum_id == stratum, 1, 0);
# Inventory plots aggregation by composite cluster identifier
agg <- by(plots, plots$cluster_cid,
FUN = function(df) { return( c(
cluster_cid = (df$cluster_cid[1]),
# [Equation number 27]
yt      = (sum(df$IS * df$ID * df$ldsity)/df$k[1]),
inside  = (sum(df$IS * df$ID) > 0),
cell_id = paste(df$cell_id, collapse = "|"),
plot_id = paste(df$plot_id, collapse = "|")
));});
}
# This code calculates cluster level local densities for whole estimation cell
else {
# Inventory plots aggregation by composite cluster identifier
agg <- by( plots, plots$cluster_cid, FUN = function(df) { return( c(
cluster_cid = (df$cluster_cid[1]),
# [Equation number 28]
yt      = (sum(df$ID * df$ldsity)/df$k[1]),
inside  = (sum(df$ID) > 0),
cell_id = paste(df$cell_id, collapse = "|"),
plot_id = paste(df$plot_id, collapse = "|")
));});
}

# This code converts list returned by aggregation function "by" into data.frame
result <- as.data.frame(do.call(rbind, agg));
result <- data.frame(
cluster_cid = as.character(result$cluster_cid),
yt          = as.numeric(as.character(result$yt)),
inside= as.logical(as.character(result$inside)),
cell_id     = as.character(result$cell_id),
plot_id= as.character(result$plot_id));

return(result);

# Function returns cluster level local densities as data.frame with columns:
# cluster_cidComposite cluster identifier (cluster identifier + panel identifier)
# ytCluster level local density
# insideBoolean - cluster is inside estimate cell and stratum intersection (or inside estimate cell when stratum is NA)
#Cluster is inside intersection (or cell), when at least one of its plots is inside intersection (or cell)
# cell_idList of cells hits by cluster plots (text divided by pipe character)
# plot_idList of cluster plots (text divided by pipe character)
}
