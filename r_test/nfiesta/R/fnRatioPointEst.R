#
# Copyright 2017, 2020 ÚHÚL
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.
#

fnRatioPointEst <-
function(data, estimate, verbose = FALSE, inList = FALSE)
{
# Function parameters:
#data <- NFiestaData;# Input data (list of data.frames obtained by function fnLoadData)
#estimate <- 1197;# Estimate identifier
#verbose <- FALSE;# Boolean - TRUE|FALSE - function prints (does not print) its results on R console and returns NULL
#inList  <- FALSE;# Boolean - TRUE - function returns (does not return) all partial results in list object for testing nfiesta system
#    - FALSE (default) - function returns only main result (ratio estimator) in data.frame object

#rm(data, estimate, verbose, inList);
#rm(a, numData, denom, denomData);

########################################################################################
# Initialization of the list with partial results

a<- list();
a$theta1<- NA;# Numerator - total estimator (list)
a$theta2<- NA;# Denominator - total estimator (list)
a$idsity<- NA;# Data.frame with cluster level inclusion densities
a$residual<- NA;# Data.frame with cluster level values of residual variable z(x)
a$sum_var_a<- NA;# First part of the sum in variance estimation  [Equation number 30]
a$sum_var_b<- NA;# Second part of the sum in variance estimation [Equation number 30]
a$estimate_id<- NA;# Estimate identifier
a$time_start<- NA;# Time when estimate computation starts
a$time_end<- NA;# Time when estimate computation ends
a$est_time<- NA;# Length of time for estimate computation
a$ratio<- NA;# Ratio estimator (point estimate)
a$var<- NA;# Ratio estimator (variance)

########################################################################################

a$time_start<- Sys.time();
a$estimate_id<- estimate;

# Numerator data
numData <- fnSelectData(data = data, estimate = a$estimate_id);
if ( numData$Conf$type_id != 2 ) {
stop( paste("Estimate type is not ratio estimate! ",
"(est_id = ", a$estimate_id, ")", sep = ""));
}

# Denominator data
denom<- data$Conf[(data$Conf$total_id == numData$Conf$denom_id) & (data$Conf$type_id == 1),]$est_id;
denomData <- fnSelectData(data = data, estimate = denom);
if ( numData$Conf$cell_id != denomData$Conf$cell_id ) {
stop( paste( "Estimation cell of total estimator in numerator have to be equal to ",
       "estimation cell of total estimator in denominator! ",
 "(est_id = ", a$estimate_id, ")", sep = "") );
}

########################################################################################
# Ratio of single-phase estimator of the total (numerator) and single-phase estimator of the total (denominator)

if ( (numData$Conf$phase_id == 1) & (denomData$Conf$phase_id == 1) ) {
a$theta1 <- fnHtcTotalEst(data = data, estimate = estimate, verbose = FALSE, inList = TRUE);
a$theta2 <- fnHtcTotalEst(data = data, estimate = denom, verbose = FALSE, inList = TRUE);

if ( nrow(a$theta1$clusters) != nrow(a$theta2$clusters) ) {
warning(paste("Numbers of clusters in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any(as.character(sort(a$theta1$clusters$cluster_cid)) !=
   as.character(sort(a$theta2$clusters$cluster_cid))) ) {
warning(paste("Sets of clusters in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

if ( nrow(a$theta1$clusters[a$theta1$clusters$insideD,]) != nrow(a$theta2$clusters[a$theta2$clusters$insideD,]) ) {
warning(paste("Numbers of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any(as.character(sort(a$theta1$clusters[a$theta1$clusters$insideD,]$cluster_cid)) !=
   as.character(sort(a$theta2$clusters[a$theta2$clusters$insideD,]$cluster_cid))) ) {
warning(paste("Sets of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

a$idsity   <- a$theta1$idsity;
a$residual <- merge(a$theta1$clusters, a$theta2$clusters, by = "cluster_cid", all = TRUE);

# This code adds default zero values, if sets of clusters in numerator and denominator are different
a$residual$ytD.x <- ifelse(is.na(a$residual$ytD.x),         0, a$residual$ytD.x);
a$residual$ytD.y <- ifelse(is.na(a$residual$ytD.y),         0, a$residual$ytD.y);
a$residual$insideD.x <- ifelse(is.na(a$residual$insideD.x), FALSE, a$residual$insideD.x);
a$residual$insideD.y <- ifelse(is.na(a$residual$insideD.y), FALSE, a$residual$insideD.y);
a$residual$phi.x <- ifelse(is.na(a$residual$phi.x),         0, a$residual$phi.x);
a$residual$phi.y <- ifelse(is.na(a$residual$phi.y),         0, a$residual$phi.y);

a$residual$insideD <- (a$residual$insideD.x & a$residual$insideD.y);
a$residual         <- a$residual[,c("cluster_cid", "ytD.x", "ytD.y", "insideD.x", "insideD.y", "insideD", "phi.x", "phi.y")];
names(a$residual)  <- c("cluster_cid", "ytD1", "ytD2", "insideD1", "insideD2", "insideD", "phi1", "phi2");
a$residual$inside  <- a$residual$insideD;

} else {

########################################################################################
# Ratio of generalized regression estimator of the total (numerator) and generalized regression estimator of the total (denominator)

if  ( (numData$Conf$phase_id == 2) & (denomData$Conf$phase_id == 2) ) {
a$theta1 <- fnGregTotalEst(data = data, estimate = estimate, verbose = FALSE, inList = TRUE);
a$theta2 <- fnGregTotalEst(data = data, estimate = denom, verbose = FALSE, inList = TRUE);

if ( nrow(a$theta1$clusters) != nrow(a$theta2$clusters) ) {
warning(paste("Numbers of clusters in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any(as.character(sort(a$theta1$clusters$cluster_cid)) !=
   as.character(sort(a$theta2$clusters$cluster_cid))) ) {
warning(paste("Sets of clusters in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

if ( nrow(a$theta1$clusters[a$theta1$clusters$insideD,]) != nrow(a$theta2$clusters[a$theta2$clusters$insideD,]) ) {
warning(paste("Numbers of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any(as.character(sort(a$theta1$clusters[a$theta1$clusters$insideD,]$cluster_cid)) !=
   as.character(sort(a$theta2$clusters[a$theta2$clusters$insideD,]$cluster_cid))) ) {
warning(paste("Sets of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

if ( nrow(a$theta1$clusters[a$theta1$clusters$insideP,]) != nrow(a$theta2$clusters[a$theta2$clusters$insideP,]) ) {
warning(paste("Numbers of clusters inside parametrization area in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any(as.character(sort(a$theta1$clusters[a$theta1$clusters$insideP,]$cluster_cid)) !=
   as.character(sort(a$theta2$clusters[a$theta2$clusters$insideP,]$cluster_cid))) ) {
warning(paste("Sets of clusters inside parametrization area in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

a$idsity   <- a$theta1$idsity;
a$residual <- merge(a$theta1$clusters, a$theta2$clusters, by = "cluster_cid", all = TRUE);

# This code adds default zero values, if sets of clusters in numerator and denominator are different
a$residual$ytD.x <- ifelse(is.na(a$residual$ytD.x),         0, a$residual$ytD.x);
a$residual$ytD.y <- ifelse(is.na(a$residual$ytD.y),         0, a$residual$ytD.y);
a$residual$insideD.x <- ifelse(is.na(a$residual$insideD.x), FALSE, a$residual$insideD.x);
a$residual$insideD.y <- ifelse(is.na(a$residual$insideD.y), FALSE, a$residual$insideD.y);
a$residual$insideP.x <- ifelse(is.na(a$residual$insideP.x), FALSE, a$residual$insideP.x);
a$residual$insideP.y <- ifelse(is.na(a$residual$insideP.y), FALSE, a$residual$insideP.y);
a$residual$phi.x <- ifelse(is.na(a$residual$phi.x),         0, a$residual$phi.x);
a$residual$phi.y <- ifelse(is.na(a$residual$phi.y),         0, a$residual$phi.y);

a$residual$insideD <- (a$residual$insideD.x & a$residual$insideD.y);
a$residual$insideP <- (a$residual$insideP.x & a$residual$insideP.y);
a$residual         <- a$residual[,c("cluster_cid", "ytD.x", "ytD.y", "insideD.x", "insideD.y", "insideD", "insideP.x", "insideP.y", "insideP", "phi.x", "phi.y")];
names(a$residual)  <- c("cluster_cid", "ytD1", "ytD2", "insideD1", "insideD2", "insideD", "insideP1", "insideP2", "insideP", "phi1", "phi2");
a$residual$inside  <- a$residual$insideP;

} else {

########################################################################################
# Ratio of single-phase estimator of the total (numerator) and generalized regression estimator of the total (denominator)

if  ( (numData$Conf$phase_id == 1) & (denomData$Conf$phase_id == 2) ) {
a$theta1 <- fnHtcTotalEst(data = data, estimate = estimate, verbose = FALSE, inList = TRUE);
a$theta2 <- fnGregTotalEst(data = data, estimate = denom, verbose = FALSE, inList = TRUE);

if ( !all( as.character(sort(a$theta1$clusters$cluster_cid)) %in%
     as.character(sort(a$theta2$clusters$cluster_cid))) ) {
warning(paste("Set of clusters in numerator (single-phase estimator) is not a subset ",
  "of set of clusters in denominator (regression estimator)! ",
        "(estimate_id = ", estimate, ")", sep = ""));
}

if ( nrow(a$theta1$clusters[a$theta1$clusters$insideD,]) != nrow(a$theta2$clusters[a$theta2$clusters$insideD,]) ) {
warning(paste("Numbers of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any( as.character(sort(a$theta1$clusters[a$theta1$clusters$insideD,]$cluster_cid)) !=
    as.character(sort(a$theta2$clusters[a$theta2$clusters$insideD,]$cluster_cid)))  ) {
warning(paste("Sets of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

a$idsity   <- a$theta2$idsity;
a$residual <- merge(a$theta1$clusters, a$theta2$clusters, by = "cluster_cid", all = TRUE);

# This code adds default zero values, if sets of clusters in numerator and denominator are different
a$residual$ytD.x <- ifelse(is.na(a$residual$ytD.x),         0, a$residual$ytD.x);
a$residual$ytD.y <- ifelse(is.na(a$residual$ytD.y),         0, a$residual$ytD.y);
a$residual$insideD.x <- ifelse(is.na(a$residual$insideD.x), FALSE, a$residual$insideD.x);
a$residual$insideD.y <- ifelse(is.na(a$residual$insideD.y), FALSE, a$residual$insideD.y);
a$residual$insideP <- ifelse(is.na(a$residual$insideP),   FALSE, a$residual$insideP);
a$residual$phi.x <- ifelse(is.na(a$residual$phi.x),         0, a$residual$phi.x);
a$residual$phi.y <- ifelse(is.na(a$residual$phi.y),         0, a$residual$phi.y);

a$residual$insideD <- (a$residual$insideD.x & a$residual$insideD.y);
a$residual         <- a$residual[,c("cluster_cid", "ytD.x", "ytD.y", "insideD.x", "insideD.y", "insideD", "insideP", "phi.x", "phi.y")];
names(a$residual)  <- c("cluster_cid", "ytD1", "ytD2", "insideD1", "insideD2", "insideD", "insideP", "phi1", "phi2");
a$residual$inside  <- a$residual$insideP;

} else {

########################################################################################
# Ratio of generalized regression estimator of the total (numerator) and single-phase estimator of the total (denominator)

if  ( (numData$Conf$phase_id == 2) & (denomData$Conf$phase_id == 1) ) {
a$theta1 <- fnGregTotalEst(data = data, estimate = estimate, verbose = FALSE, inList = TRUE);
a$theta2 <- fnHtcTotalEst(data = data, estimate = denom, verbose = FALSE, inList = TRUE);

if ( !all( as.character(sort(a$theta2$clusters$cluster_cid)) %in%
     as.character(sort(a$theta1$clusters$cluster_cid))) ) {
warning(paste("Set of clusters in denominator (regression estimator) is not a subset ",
  "of set of clusters in numerator (single-phase estimator)! ",
        "(estimate_id = ", estimate, ")", sep = ""));
}

if ( nrow(a$theta1$clusters[a$theta1$clusters$insideD,]) != nrow(a$theta2$clusters[a$theta2$clusters$insideD,]) ) {
warning(paste("Numbers of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( any( as.character(sort(a$theta1$clusters[a$theta1$clusters$insideD,]$cluster_cid)) !=
    as.character(sort(a$theta2$clusters[a$theta2$clusters$insideD,]$cluster_cid))) ) {
warning(paste("Sets of clusters inside estimation cell in numerator and denominator are different! ",
"(estimate_id = ", estimate, ")", sep = ""));
}}

a$idsity   <- a$theta1$idsity;
a$residual <- merge(a$theta1$clusters, a$theta2$clusters, by = "cluster_cid", all = TRUE);

# This code adds default zero values, if sets of clusters in numerator and denominator are different
a$residual$ytD.x <- ifelse(is.na(a$residual$ytD.x),         0, a$residual$ytD.x);
a$residual$ytD.y <- ifelse(is.na(a$residual$ytD.y),         0, a$residual$ytD.y);
a$residual$insideD.x <- ifelse(is.na(a$residual$insideD.x), FALSE, a$residual$insideD.x);
a$residual$insideD.y <- ifelse(is.na(a$residual$insideD.y), FALSE, a$residual$insideD.y);
a$residual$insideP <- ifelse(is.na(a$residual$insideP),   FALSE, a$residual$insideP);
a$residual$phi.x <- ifelse(is.na(a$residual$phi.x),         0, a$residual$phi.x);
a$residual$phi.y <- ifelse(is.na(a$residual$phi.y),         0, a$residual$phi.y);

a$residual$insideD <- (a$residual$insideD.x & a$residual$insideD.y);
a$residual         <- a$residual[,c("cluster_cid", "ytD.x", "ytD.y", "insideD.x", "insideD.y", "insideD", "insideP", "phi.x", "phi.y")];
names(a$residual)  <- c("cluster_cid", "ytD1", "ytD2", "insideD1", "insideD2", "insideD", "insideP", "phi1", "phi2");
a$residual$inside  <- a$residual$insideP;

########################################################################################
# Not defined values of estimate phase identifier

} else {
if ( !(numData$Conf$phase_id %in% c(1,2)) ) {
stop( paste ("Value of estimate phase identifier in numerator is not defined!",
       "(estimate_id = ", estimate, ")", sep = ""));
} else {
if ( !(denomData$Conf$phase_id %in% c(1,2)) ) {
stop( paste ("Value of estimate phase identifier in denominator is not defined!",
       "(estimate_id = ", estimate, ")", sep = ""));
}}}}}}

########################################################################################
# Calculation of ratio estimator and residual variable z(x)

if ( !(is.na(a$theta1$total) | is.na(a$theta2$total)) ) {
if (a$theta2$total != 0) {

# [Equation number 82]
a$ratio  <- a$theta1$total/a$theta2$total;

# [Equations number 85, 86, 87, 88]
a$residual$z <- a$residual$phi1 - (a$ratio * a$residual$phi2);

}
else {
# Division by zero is not allowed
a$ratio <- NA;
a$residual$z <- NA;
}
}
else {
# NA value of total estimator in numerator or denominator is not allowed
a$ratio <- NA;
a$residual$z <- NA;
}

########################################################################################
# Length of time for estimate computation

a$time_end <- Sys.time();
a$est_time <- round(difftime(a$time_end, a$time_start, units="secs"), 3);

########################################################################################

if (verbose) {
cat(paste("\n"));
cat(paste("------------------------------------------------------------\n"));
cat(paste("estimate_id:             ",   a$theta1$estimate_id,        "\n"));
cat(paste("estimate_name:           ",   a$theta1$estimate_name,      "\n"));
cat(paste("cell_id:                 ",   a$theta1$cell_id,            "\n"));
cat(paste("cell_name:               ",   a$theta1$cell_name,          "\n"));
cat(paste("var_id:                  ",   a$theta1$var_id,             "\n"));
cat(paste("var_name:                ",   a$theta1$var_name,           "\n"));
cat(paste("------------------------------------------------------------\n"));
cat(paste("denominator_id:          ",   a$theta2$estimate_id,        "\n"));
cat(paste("denominator_name:        ",   a$theta2$estimate_name,      "\n"));
cat(paste("denominator_var_id:      ",   a$theta2$var_id,             "\n"));
cat(paste("denominator_var_name:    ",   a$theta2$var_name,           "\n"));
cat(paste("------------------------------------------------------------\n"));
cat(paste("numerator:               ",   a$theta1$total,              "\n"));
cat(paste("denominator:             ",   a$theta2$total,              "\n"));
cat(paste("ratio:                   ",   a$ratio,                     "\n"));
cat(paste("------------------------------------------------------------\n"));
cat(paste(a$est_time, "seconds\n"));
cat(paste("\n"));
}
else {
if (inList) { return(a); }
else {
return(
data.frame(
cell_id     = a$theta1$cell_id,
cell_name   = a$theta1$cell_name,
numerator   = a$theta1$total,
denominator = a$theta2$total,
ratio       = a$ratio)
);
}
}
}
