--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


ALTER TABLE @extschema@.t_total_estimate_data DROP CONSTRAINT check__t_total_estimate_data__cor_phase;

ALTER TABLE @extschema@.t_total_estimate_data ADD CONSTRAINT check__t_total_estimate_data__cor_phase
	CHECK (CASE WHEN phase = 2 THEN cor_phase IS NULL ELSE cor_phase > phase END);

