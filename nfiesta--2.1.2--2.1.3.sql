--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
	unique constraint on table t_auxiliary_data
*/

ALTER TABLE @extschema@.t_auxiliary_data ADD CONSTRAINT uidx__t_auxiliary_data__plot_aux_cat UNIQUE (plot, auxiliary_variable_category);

ALTER TABLE @extschema@.t_target_data ALTER COLUMN target_variable SET NOT NULL;
CREATE UNIQUE INDEX uidx__t_target_data__all_columns ON @extschema@.t_target_data (plot, target_variable, coalesce(area_domain_category,0), coalesce(sub_population_category,0), coalesce(reference_year_set,0));

