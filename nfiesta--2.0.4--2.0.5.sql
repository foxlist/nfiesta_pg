--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--t_aux_conf
alter table @extschema@.t_aux_conf ADD column sigma boolean;
update @extschema@.t_aux_conf SET sigma = true;
insert into @extschema@.t_aux_conf (param_area, model, description, sigma ) 
	select param_area, model, '2p total with sigma = 1' as description, false as sigma 
	from @extschema@.t_aux_conf order by id;
alter table @extschema@.t_aux_conf alter COLUMN sigma set not null ;

--t_panel2aux_conf
insert into @extschema@.t_panel2aux_conf (aux_conf, panel, reference_year_set)
select new.id as aux_conf, t_panel2aux_conf.panel, t_panel2aux_conf.reference_year_set
from @extschema@.t_panel2aux_conf
inner join @extschema@.t_aux_conf as orig on (t_panel2aux_conf.aux_conf = orig.id)
inner join (select * from @extschema@.t_aux_conf where sigma = false) as new on (orig.param_area = new.param_area and orig.model = new.model)
order by t_panel2aux_conf.id;

--t_total_estimate_conf
with w_two_phase as (
	select t_total_estimate_conf.id
	from @extschema@.t_total_estimate_conf
	inner join @extschema@.t_total_estimate_data on (t_total_estimate_conf.id = t_total_estimate_data.total_estimate_conf)
	group by t_total_estimate_conf.id having count(*) = 2
)
insert into @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf)
select
	estimation_cell, estimate_date_begin, estimate_date_end, 
	concat(total_estimate_conf, '; sigma = 1') as total_estimate_conf
from @extschema@.t_total_estimate_conf
inner join w_two_phase on (t_total_estimate_conf.id = w_two_phase.id)
order by t_total_estimate_conf.id
;

--t_estimate_conf
with w_tec as (
	select t_total_estimate_conf_orig.*, t_total_estimate_conf_new.id as t_total_estimate_conf_new_id
	from @extschema@.t_total_estimate_conf as t_total_estimate_conf_orig
	inner join (select * from @extschema@.t_total_estimate_conf where substring(total_estimate_conf from '...........$') = '; sigma = 1' )
		as t_total_estimate_conf_new using (estimation_cell, estimate_date_begin, estimate_date_end)
	where t_total_estimate_conf_orig.total_estimate_conf = trim(trailing '; sigma = 1' from t_total_estimate_conf_new.total_estimate_conf)
)
insert into @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
select
	t_estimate_conf.estimate_type, 
	w_tec.t_total_estimate_conf_new_id as total_estimate_conf, w_tec_denom.t_total_estimate_conf_new_id as denominator
from @extschema@.t_estimate_conf
inner join w_tec on (w_tec.id = t_estimate_conf.total_estimate_conf)
left join w_tec as w_tec_denom on (w_tec_denom.id = t_estimate_conf.denominator)
order by t_estimate_conf.id;

--t_total_estimate_data
with w_tec as (
	select t_total_estimate_conf_orig.*, t_total_estimate_conf_new.id as t_total_estimate_conf_new_id 
	from @extschema@.t_total_estimate_conf as t_total_estimate_conf_orig
	inner join (select * from @extschema@.t_total_estimate_conf where substring(total_estimate_conf from '...........$') = '; sigma = 1' ) 
		as t_total_estimate_conf_new using (estimation_cell, estimate_date_begin, estimate_date_end)
	where t_total_estimate_conf_orig.total_estimate_conf = trim(trailing '; sigma = 1' from t_total_estimate_conf_new.total_estimate_conf)
)
, w_ac as (
	select t_aux_conf_orig.*, t_aux_conf_new.id as t_aux_conf_new_id
	from 		(select * from @extschema@.t_aux_conf where sigma = true) as t_aux_conf_orig
	inner join 	(select * from @extschema@.t_aux_conf where sigma = false) as t_aux_conf_new using (param_area, model)
)
insert into @extschema@.t_total_estimate_data (total_estimate_conf, variable, phase, aux_phase_type, cor_phase, aux_conf)
select 
	--w_tec.id,
	w_tec.t_total_estimate_conf_new_id,
	t_total_estimate_data.variable,
	t_total_estimate_data.phase,
	t_total_estimate_data.aux_phase_type,
	t_total_estimate_data.cor_phase,
	--w_ac.id,
	w_ac.t_aux_conf_new_id
from @extschema@.t_total_estimate_data 
inner join w_tec on (w_tec.id = t_total_estimate_data.total_estimate_conf)
left join w_ac on (w_ac.id = t_total_estimate_data.aux_conf)
order by t_total_estimate_data.id;

--t_panel2total_1p_estimate_data
insert into @extschema@.t_panel2total_2ndph_est_data (total_estimate_data, panel, reference_year_set)
with w_tec as (
	select t_total_estimate_conf_orig.*, t_total_estimate_conf_new.id as t_total_estimate_conf_new_id
	from @extschema@.t_total_estimate_conf as t_total_estimate_conf_orig
	inner join (select * from @extschema@.t_total_estimate_conf where substring(total_estimate_conf from '...........$') = '; sigma = 1' )
		as t_total_estimate_conf_new using (estimation_cell, estimate_date_begin, estimate_date_end)
	where t_total_estimate_conf_orig.total_estimate_conf = trim(trailing '; sigma = 1' from t_total_estimate_conf_new.total_estimate_conf)
)
select --t_panel2total_2ndph_est_data.id, t_total_estimate_data_orig.id, w_tec.id, t_total_estimate_data_new.id
t_total_estimate_data_new.id as t_total_estimate_data_new_id, t_panel2total_2ndph_est_data.panel, t_panel2total_2ndph_est_data.reference_year_set
from @extschema@.t_panel2total_2ndph_est_data
inner join @extschema@.t_total_estimate_data as t_total_estimate_data_orig on (t_panel2total_2ndph_est_data.total_estimate_data = t_total_estimate_data_orig.id)
inner join w_tec on (w_tec.id = t_total_estimate_data_orig.total_estimate_conf)
inner join @extschema@.t_total_estimate_data as t_total_estimate_data_new on (
	w_tec.t_total_estimate_conf_new_id = t_total_estimate_data_new.total_estimate_conf
	and t_total_estimate_data_orig.variable = t_total_estimate_data_new.variable
	and t_total_estimate_data_orig.phase = t_total_estimate_data_new.phase
)
order by t_panel2total_2ndph_est_data.id;

-------------------------------------------views
-- <view view_name="v_conf_overview" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--drop view if exists @extschema@.v_conf_overview;
create view @extschema@.v_conf_overview as (
	select
		t_estimate_conf.id as conf_id,
		t_total_estimate_conf.id as tec_id,
		ted_second_ph.id as ted_secph_id,
		ted_zero_ph.id as ted_zerph_id,
		t_aux_conf.id as tac_id,
		t_total_estimate_conf_denom.id as tec_d_id,
		ted_second_ph_denom.id as ted_secph_d_id,
		ted_zero_ph_denom.id as ted_zerph_d_id,
		t_aux_conf_denom.id as tac_d_id,
		case 	when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null 
			then '1p_total'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is null 
			then '1p_ratio'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null and
				t_aux_conf.sigma = false
			then '2p_total_no_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p_total_with_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is not null and 
				t_aux_conf.sigma = false
			then '2p_ratio_no_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is not null and
				t_aux_conf.sigma = true
			then '2p_ratio_with_sigma'
			else 'unknown'
		end as estimate_type_str,
------------------------additional info begin-----------------------------------
		t_aux_conf.param_area, f_a_param_area.param_area_code,
		t_aux_conf.model, t_model.description as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
        inner join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_second_ph ON ted_second_ph.total_estimate_conf = t_total_estimate_conf.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_zero_ph ON ted_zero_ph.total_estimate_conf = t_total_estimate_conf.id
	left join @extschema@.t_aux_conf ON t_aux_conf.id = ted_zero_ph.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
        left join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_second_ph_denom ON ted_second_ph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_zero_ph_denom ON ted_zero_ph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = ted_zero_ph_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	inner join @extschema@.f_a_cell on (t_total_estimate_conf.estimation_cell = f_a_cell.gid)
	inner join @extschema@.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (ted_second_ph.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	inner join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
);
--select * from @extschema@.v_conf_overview;

-- </view>

-------------------------------------------functions

-- <function function_name="fn_1p_data" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint, 3035),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	left join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	left join @extschema@.t_model ON t_model.id = t_aux_conf.model
	left join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select 
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2total_2ndph_est_data.reference_year_set,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.f_a_cell ON (f_a_cell.gid = cm_plot2cell_mapping.estimation_cell and w_configuration.cell = f_a_cell.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			t_target_data.sub_population_category = t_variable.sub_population_category
			)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_total_estimate_data on t_total_estimate_data.total_estimate_conf = ANY (w_configuration.t_total_estimate_conf__id)
	inner join @extschema@.t_panel2total_2ndph_est_data ON t_panel2total_2ndph_est_data.total_estimate_data = t_total_estimate_data.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_1p_data as (
	with w_ldsity_cluster as (select * from w_ldsity_cluster)
	select 
		/*w_ldsity_cluster.conf_id, */w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, 
		w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix, w_pix.sweight, NULL::double precision as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	--INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function function_name="fn_1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p_ratio_var.sql">
-- Function: @extschema@.fn_1p_ratio_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_ratio_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
from w_1p_ratio_var
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function function_name="fn_1p_total_var" function_schema="extschema" src="functions/extschema/fn_1p_total_var.sql">
-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
	, w_data_agg AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg
	)
	, w_est1p AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum group by attribute order by attribute
	)	
	select 
	w_est1p.attribute, 
	point1p,  var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
	from w_est1p;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS 'Function computing total and variance using regression estimate.';

-- </function>

DROP FUNCTION @extschema@.fn_2p_data(integer);
-- <function function_name="fn_2p_data" function_schema="extschema" src="functions/extschema/fn_2p_data.sql">
-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint, 3035),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
        pix double precision,
        sweight double precision,
        DELTA_T__G_beta double precision,
        nb_sampling_units integer, 
        sweight_strata_sum double precision, 
        lambda_d_plus double precision,
        sigma boolean
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2aux_conf.reference_year_set,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_configuration.param_area = f_a_param_area.gid)
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_auxiliary_data.value as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot 
		inner join @extschema@.t_variable on t_variable.auxiliary_variable_category = t_auxiliary_data.auxiliary_variable_category
		inner join w_configuration on t_variable.id = ANY (w_configuration.aux_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			t_target_data.sub_population_category = t_variable.sub_population_category
			)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
, w_X AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_Y AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_I AS (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS (
	select 
		w_cell_selection.conf_id,
		attribute as r,
		1 as c,
		aux_total as val 
	from
	(select estimation_cell as cell, v_ldsity_conf.id as attribute, aux_total 
		from @extschema@.t_aux_total 
		inner join @extschema@.v_ldsity_conf on (t_aux_total.auxiliary_variable_category = v_ldsity_conf.auxiliary_variable_category)
	) as t_aux_total
	inner join w_cell_selection on (w_cell_selection.cell_gid = t_aux_total.cell)
	inner join (select distinct conf_id, r from w_X) as foo on (foo.r = attribute and foo.conf_id = w_cell_selection.conf_id)
)
, w_DELTA_T AS (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_t_G_beta AS (
	select 
		w_configuration.id as conf_id, 
		t_g_beta.r, t_g_beta.c, t_g_beta.val 
	from @extschema@.t_g_beta 
	inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
)
, w_DELTA_G_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_Y_T AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_Y_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_X_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
, w_residuals_plot AS ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS (
	with
	w_residuals_plot as (select * from w_residuals_plot),
	w_ldsity_plot as (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster as (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
, w_2p_data as (
	with w_ldsity_residuals_cluster as (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma as (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell, w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function function_name="fn_2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p_ratio_var.sql">
-- Function: @extschema@.fn_2p_ratio_var(integer)

-- DROP FUNCTION @extschema@.fn_2p_ratio_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.phi * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p, var2p 
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function function_name="fn_2p_total_var" function_schema="extschema" src="functions/extschema/fn_2p_total_var.sql">
-- Function: @extschema@.fn_2p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_2p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_2p_data(' || conf_id || ') where is_target
	)
	, w_I AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data
                order by r, c
	)
	, w_SIGMA AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data
		order by r, c
	)
	, w_PI AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data
                order by r, c
	)
	, w_SIGMA_PI AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA as A inner join w_PI as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data
                order by r, c
	)
	, w_total_1p AS (
                with w_I_PI AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I      AS A 
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI as A, w_Y_T as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data
	)
	, w_correction_2p AS (
                with w_DELTA_T__G_beta_PI AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta AS A
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI as A, w_Y_T as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p 		AS A
                inner join  w_correction_2p    	AS B    on (A.c = B.c)
		order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESIDUALS----------------------------------
-------------------------------------------------------------------------------
	, w_e AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data
                order by r, c
	)
	, w_I_e AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_PHI AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e        		AS A
                inner join  w_DELTA_T__G_beta__e  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
	, w_zeroResidualsTotalTest AS (
                select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                        w_SIGMA_PI as A, (select c as r, r as c, val_d_plus from w_e) as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_phi AS (
	select w_data.*, w_PHI.val as phi from w_data inner join w_PHI on (cluster = c and attribute = r)
) 
, w_data_agg AS        (
        SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(phi order by cluster) as residuals,
		array_agg(sweight order by cluster) as sweights
        FROM
            w_data_phi
        group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est1p AS    (
	SELECT
	    attribute, sum(total) as point1p, sum(var) AS var1p 
	from w_est1p_stratum group by attribute order by attribute
)
, w_est2p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, residuals, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est2p AS    (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	from w_est2p_stratum group by attribute order by attribute
)
select 
	w_est1p.attribute, 
	point1p,  var1p,
	val as point2p, var2p 
from w_est1p
inner join w_est2p on (w_est1p.attribute = w_est2p.attribute) 
inner join w_total_2p on (w_est2p.attribute = w_total_2p.c)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p_total_var(integer) IS 'Function computing total and variance using regression estimate.';

-- </function>

-- <function function_name="fn_create_buffered_geometry" function_schema="extschema" src="functions/extschema/fn_create_buffered_geometry.sql">
--------------------------------------------------------------------------------
-- fn_create_buffered_geometry
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon,3035), integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION
@extschema@.fn_create_buffered_geometry
(
	_geom			geometry(MultiPolygon,3035),
	_tract_type		integer,
	_point_distance_m	integer
)

RETURNS geometry(MultiPolygon,3035)
AS
$$
DECLARE
	_geom_buff		geometry(MultiPolygon,3035);
	_axis_shift		double precision;
BEGIN

	CASE
	WHEN _tract_type = 100
	THEN
		 RETURN ST_Multi(ST_Buffer(_geom,_point_distance_m));

	WHEN _tract_type = 200
	THEN
		_axis_shift := ( (_point_distance_m^2) / 2.0 )^(1/2.0);


		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu, na opacnou stratnu, nez se vytvari trakt
					ST_Translate(_geom, -_axis_shift, -_axis_shift)
				) AS geom
			)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				x, y, geom,
				x-_point_distance_m AS x_shift,
				y-_point_distance_m AS y_shift,
				ST_SetSRID(ST_MakePoint(x-_axis_shift, y-_axis_shift),3035) AS geom_shift
			FROM
				w_coordinates
		)
		,w_make_line AS (
			SELECT
				ST_Union(ST_MakeLine(geom,geom_shift)) AS line
			FROM 
				w_shift_points
			)
		,w_union_all AS (
			SELECT
				ST_Union(t1.line, ST_Boundary(t2.geom)) AS all_lines
			FROM
				w_make_line AS t1,
				w_only_shifted AS t2
			)
		SELECT
			ST_Multi(ST_buildarea(all_lines))
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;

	WHEN _tract_type = 300
	THEN

		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu doleva
					ST_Translate(_geom, -(x_shift*_point_distance_m), -(y_shift*_point_distance_m))
				) AS geom_unioned
			FROM
				(SELECT unnest(ARRAY[1,1,0]) AS x_shift, unnest(ARRAY[0,1,1]) AS y_shift) AS t1
			)
		,w_union_only_shifted AS (
			SELECT ST_Union(ST_MakeValid(geom_unioned)) AS geom_unioned
			FROM w_only_shifted
		)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				geom,
				ST_SetSRID(ST_MakePoint(x-(x_shift[1]*_point_distance_m), y-(y_shift[1]*_point_distance_m)),3035) AS geom_shift_l,
				ST_SetSRID(ST_MakePoint(x-(x_shift[2]*_point_distance_m), y-(y_shift[2]*_point_distance_m)),3035) AS geom_shift_ld,
				ST_SetSRID(ST_MakePoint(x-(x_shift[3]*_point_distance_m), y-(y_shift[3]*_point_distance_m)),3035) AS geom_shift_d
			FROM
				w_coordinates AS t1,
				(SELECT ARRAY[1,1,0] AS x_shift, ARRAY[0,1,1] AS y_shift) AS t2
		)
		,w_make_line AS (
			-- make the square from 4 lines
			SELECT	ST_MakeLine(geom,geom_shift_l) AS line			FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_l,geom_shift_ld) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_ld,geom_shift_d) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_d,geom) AS line			FROM w_shift_points
			-- diagonal line to prevent st_buildarea to exclude squares
			UNION ALL
			SELECT	ST_MakeLine(geom,geom_shift_ld) AS line			FROM w_shift_points
		)
		,w_union_lines AS (
			SELECT ST_Collect(line) AS line FROM w_make_line
		)
		,w_area_lines AS (
			SELECT
					ST_BuildArea(line) as area_lines
			FROM
					w_union_lines
		)
		,w_area_geoms AS (
			SELECT
				ST_BuildArea(ST_MakeValid(geom_unioned)) AS area_geoms
			FROM
				w_union_only_shifted
		),
		w_union_all AS (
			SELECT
				ST_union(t1.area_lines,t2.area_geoms) AS geom
			FROM
				w_area_lines AS t1,
				w_area_geoms AS t2
		)
		SELECT
			ST_Multi(geom)
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;
	ELSE
		RAISE EXCEPTION 'Not known tract type!';
	END CASE;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon,3035), integer, integer) IS
'Functions generates a buffered geometry of original geographical domain for specified tract_type.
100 -- 2p, 1p rotated -- Two points, the second is rotated around the first.
200 -- 2p, no rotation -- N-E, Two points, no rotation, the second located North-East.
300 -- 4p in square, N-E -- Four points in square shape, no rotation. North, North-East, Eeast locations.
';

-- </function>

-- <function function_name="fn_g_beta" function_schema="extschema" src="functions/extschema/fn_g_beta.sql">
-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	r integer,
	c integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum, f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		t_auxiliary_data.value as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot
	inner join @extschema@.t_variable on t_auxiliary_data.auxiliary_variable_category = t_variable.auxiliary_variable_category
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	WHERE t_variable.id = ANY (w_configuration.aux_attributes)
)
, w_ldsity_cluster AS (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, cluster from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r, c, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by r(ow) and c(column) indices.';

-- </function>

-- <function function_name="fn_get_rast_total_clip" function_schema="extschema" src="functions/extschema/fn_get_rast_total_clip.sql">
-- Function: @extschema@.fn_get_rast_total_clip(integer, regclass)

-- DROP FUNCTION @extschema@.fn_get_rast_total_clip(integer, regclass);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass)
RETURNS TABLE (
	ldsity_type integer,
	attribute integer,
	total float
)
AS
$function$
BEGIN
RETURN QUERY EXECUTE
'
WITH
	-- cutting geometry on the fly by 50x50 inspire grid
	w_cell AS (
		SELECT
			t2.gid,
			ST_Intersection(t1.geom, t2.geom) AS geom
		FROM
			@extschema@.f_a_cell AS t1
		INNER JOIN
			@extschema@.f_a_cell AS t2
		ON ST_Intersects(t1.geom,t2.geom)
		WHERE
			t1.gid = $1 AND
			t2.cell_type = 300
	),
	-- clipping raster to the geometry area
	w_clip AS (
			SELECT
				t2.rid,
				ST_Clip(t2.rast, NULL::integer[], t1.geom, ARRAY[255,255,255,255], TRUE) AS rast
			FROM 
				w_cell AS t1,
				'|| table_name ||' AS t2
			WHERE
				-- make sure that previous intersection is polygon, not only point or linestring
				(ST_GeometryType(t1.geom) = ''ST_Polygon'' OR
				ST_GeometryType(t1.geom) = ''ST_MultiPolygon'') AND
				ST_Intersects(t1.geom, t2.rast)
		)
		-- reclassification of raster and dump bands to separate rasters
		, w_raster_reclass AS (
			SELECT
				rid,
				ldsity_type,
				attribute,
				ST_Reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, 255) rast
			FROM
				(SELECT
				   	t1.rid, t1.rast, t2.band_number
				FROM
				   w_clip AS t1
				CROSS JOIN
					-- only  4 bands exists
					unnest(ARRAY[1,2,3,4]) AS t2(band_number)
				)AS t1
			INNER JOIN
				-- table of desired ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
				(
				SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
					UNION ALL
				SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
					UNION ALL
				SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
					UNION ALL
				SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
				) AS t2
			ON
				t1.band_number = t2.band_number
		)
		, w_value_count AS (
			-- calculation of present values in raster
			SELECT
				rid,
				ldsity_type,
				attribute,
				(ST_ValueCount(rast, 1, true)) AS val_count,
				(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
			FROM
				w_raster_reclass
			ORDER BY rid, ldsity_type, attribute
		)
		-- summation of values
		SELECT
			ldsity_type,
			attribute,
			sum(pixarea * (val_count).value * (val_count).count)
		FROM
			  w_value_count
		GROUP BY
			ldsity_type,
			attribute
		ORDER BY
			ldsity_type,
			attribute'
			USING cell_gid;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Clip (pixel belongs to the geometry if the geometry covers at least 50% of it).';

-- </function>

-- <function function_name="fn_get_rast_total_intersection" function_schema="extschema" src="functions/extschema/fn_get_rast_total_intersection.sql">
-- Function: @extschema@.fn_get_rast_total_intersection(integer, regclass)

-- DROP FUNCTION @extschema@.fn_get_rast_total_intersection(integer, regclass);
CREATE OR REPLACE FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass)
RETURNS TABLE (
	ldsity_type integer,
	attribute integer,
	total float
)
AS
$function$
BEGIN
RETURN QUERY EXECUTE '
WITH w_cell_raster_reclass AS (
		SELECT
			t1.rid,
			t1.gid,
			t2.ldsity_type,
			t2.attribute,
			t1.geom,
			covers,
			-- all rasters have only 1 band, the original one was expanded
			-- reclass all, case when noreclass was slowing down the calculation
			ST_reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, NULL) rast
		FROM
			-- rasters covering whole calculated cell
			(SELECT
				t1.rid,
				t2.gid,
				t2.geom,
				t3.band AS band_number,
				ST_Covers(t2.geom, ST_Convexhull(t1.rast)) AS covers,
				-- expand raster bands into separate rasters
				--ST_Band(t1.rast,t3.band) AS rast
				t1.rast
			FROM					
				'|| table_name ||' AS t1
			INNER JOIN
				(
				SELECT
					gid,
					geom
				FROM
					@extschema@.f_a_cell
					--order by gid
				WHERE
					gid = $1
				) AS t2
			ON
				ST_Intersects(t2.geom, t1.rast)
			CROSS JOIN
				-- only  4 bands exists
				unnest(ARRAY[1,2,3,4]) AS t3(band)
			) t1
		INNER JOIN
		-- table of disared ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
			(
			SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
				UNION ALL
			SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
				UNION ALL
			SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
				UNION ALL
			SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
			) AS t2
		ON
			t1.band_number = t2.band_number
	-- rasters which are not fully within the cell
	),w_not_covers AS (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_Intersection(rast, geom)) AS intersection,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			NOT covers
		)
	-- rasters which are fully within the cell
	,w_covers AS (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_ValueCount(rast, 1, true)) AS val_count,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			covers
	)
	--INSERT INTO @extschema@.t_aux_total(cell, ldsity_type, attribute, aux_total)
	SELECT
		ldsity_type,
		attribute,
		coalesce(sum(rid_sum), 0)	-- 1 min 30 sec
	FROM 
		(
		-- sum of values for each raster covering the cell
		-- speed up solution for rasters completely within cell (no need for st_intersection)
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * count * val)
			FROM 
				(SELECT
					(val_count).value AS val,
					(val_count).count AS count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
				) AS t1
			) AS rid_sum
		FROM
			w_covers
		UNION ALL
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * val)
			FROM
				(SELECT
					(intersection).val AS val,
					ST_Area((intersection).geom) / 10000 AS pixarea
				) AS t2
			) AS rid_sum
		FROM
			w_not_covers		
		) AS w_rid_sum
	GROUP BY
		ldsity_type,
		attribute
	ORDER BY
		ldsity_type,
		attribute' USING cell_gid;
END;
$function$

LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Intersection (pixel is cutted by geometry boundary and only its exact portion belongs to the geometry).';

-- </function>

-- <function function_name="fn_inverse" function_schema="extschema" src="functions/extschema/fn_inverse.sql">
-- Function: @extschema@.fn_inverse(double precision[])

-- DROP FUNCTION @extschema@.fn_inverse(double precision[]);

CREATE OR REPLACE FUNCTION @extschema@.fn_inverse(input double precision[][])
  RETURNS double precision[][] AS
$BODY$
	#import numpy as np
	from numpy import matrix

	#test to singularity
	# if the rank of matrix is the same value as the row dimension (no 0 rows or columns, rows linearly independent)
	# than it should be able to compute inversion

	#def is_invertible(a):
	#	return a.shape[0] == a.shape[1] and np.linalg.matrix_rank(a) == a.shape[0]	

	#if is_invertible(a):

	#test deprecated
	#matrix_rank uses svd decomposition but np.linalg.inv(a) uses LU factorization (LAPACK)

	a = matrix(input)
	try:
		inversed=a.I.tolist()
	except Exception:
		plpy.info("not able to compute inversion of matrix", input)
		return None
	return list(inversed)

	#else:
	#	raise ValueError('Input matrix is singular, inversion of it does not exist!')

$BODY$
  LANGUAGE plpython3u VOLATILE STRICT
  COST 100;

COMMENT ON FUNCTION @extschema@.fn_inverse(double precision[][]) IS 'Function for computing inverse of matrix using python3.';
--------------TEST
/*
select @extschema@.fn_inverse ('{{222477.383519094,7489538.83305858},{7489538.83305858,537848864.980823}}');
-- "{{8.46126431190308e-006,-1.17823001528562e-007},{-1.17823001528562e-007,3.49994221042523e-009}}"
*/

-- </function>

-- <function function_name="fn_raise_notice" function_schema="extschema" src="functions/extschema/fn_raise_notice.sql">
DROP FUNCTION IF EXISTS @extschema@.fn_raise_notice(text);
create or replace function @extschema@.fn_raise_notice(message text) returns boolean as $$
begin
    raise notice '%	%', clock_timestamp(), message;
    return true;
end;
$$ language plpgsql;
--select @extschema@.fn_raise_notice('start of ETL')
-- </function>
