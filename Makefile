EXTENSION = nfiesta # the extensions name

DATA =	nfiesta--2.0.4.sql \
	nfiesta--2.0.4--2.0.5.sql \
	nfiesta--2.0.5--2.0.6.sql \
	nfiesta--2.0.6--2.0.7.sql \
	nfiesta--2.0.7--2.0.8.sql \
	nfiesta--2.0.8--2.0.9.sql \
	nfiesta--2.0.9--2.0.10.sql \
	nfiesta--2.0.10--2.0.11.sql \
	nfiesta--2.0.11--2.0.12.sql \
	nfiesta--2.0.12--2.0.13.sql \
	nfiesta--2.0.13--2.0.14.sql \
	nfiesta--2.0.14--2.0.15.sql \
	nfiesta--2.0.15--2.0.16.sql \
	nfiesta--2.0.16--2.0.17.sql \
	nfiesta--2.0.17--2.0.18.sql \
	nfiesta--2.0.18--2.0.19.sql \
	nfiesta--2.0.19--2.1.0.sql \
	nfiesta--2.1.0--2.1.1.sql \
	nfiesta--2.1.1--2.1.2.sql \
	nfiesta--2.1.2--2.1.3.sql \
	nfiesta--2.1.3--2.1.4.sql \
	nfiesta--2.1.4--2.2.0.sql \
	nfiesta--2.2.0--2.2.1.sql \
	nfiesta--2.2.1--2.2.2.sql \
	nfiesta--2.2.2--2.2.3.sql \
	nfiesta--2.2.3--2.2.4.sql \
	nfiesta--2.2.4--2.2.5.sql \
	nfiesta--2.2.5--2.2.6.sql \
	nfiesta--2.2.6--2.2.7.sql \
	nfiesta--2.2.7--2.2.8.sql \
	nfiesta--2.2.8--2.2.9.sql \
	nfiesta--2.2.9--2.2.10.sql

REGRESS = install_test so_version \
	fn_inverse \
	full_stack_test_data full_stack_test_conf \
	full_stack_test_analyze \
	full_stack_test_1p_est_conf \
	full_stack_test_1p_v_conf_overview \
	full_stack_test_1p_data full_stack_test_1p_total full_stack_test_1p_ratio \
	full_stack_test_2p_gbeta \
	full_stack_test_2p_est_conf \
	full_stack_test_2p_v_conf_overview \
	full_stack_test_2p_data full_stack_test_2p_total full_stack_test_2p_ratio \
	full_stack_test_remove_param_area \

REGRESS_COMPLETE = install_test so_version \
	fn_inverse \
	full_stack_test_data full_stack_test_conf \
	full_stack_test_analyze \
	full_stack_test_1p_est_conf \
	full_stack_test_1p_v_conf_overview \
	full_stack_test_1p_data full_stack_test_1p_total full_stack_test_1p_ratio \
	full_stack_test_2p_gbeta \
	full_stack_test_2p_est_conf \
	full_stack_test_2p_v_conf_overview \
	full_stack_test_2p_data_complete full_stack_test_2p_total_complete full_stack_test_2p_ratio_complete \
	full_stack_test_remove_param_area \

installcheck-complete:
	make installcheck REGRESS="$(REGRESS_COMPLETE)"

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
