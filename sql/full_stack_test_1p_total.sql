--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
with w_conf as (
		select * from nfiesta_test.v_conf_overview where estimate_type_str = '1p_total'
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, (select count(*) as cnt from w_conf) from w_conf
)
	SELECT nfiesta_test.fn_make_estimate(estimate_conf)
	FROM w_conf_info
;
select v_conf_overview.estimate_conf, round(point::numeric, 10) as point, round(var::numeric, 10) as var, sampling_units
from nfiesta_test.t_result 
inner join nfiesta_test.v_conf_overview on (t_result.estimate_conf = v_conf_overview.estimate_conf) 
where estimate_type_str = '1p_total' 
order by v_conf_overview.estimate_conf;
