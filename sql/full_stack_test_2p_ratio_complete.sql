--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
refresh materialized view nfiesta_test.v_ldsity_conf;
with w_conf as (
		select * from nfiesta_test.v_conf_overview where estimate_type_str = '1p2p_ratio' and sigma = False
			--and param_area <= 48
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select 
		estimate_conf, clock_timestamp() as t_start, 
		(select (point2p, var2p, est_info)::nfiesta_test.estimate_result from nfiesta_test.fn_1p2p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res, 
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta_test.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta_test.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).est_info
from w_res
;
with w_conf as (
		select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p1p_ratio' and sigma = False
			--and param_area <= 48
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select 
		estimate_conf, clock_timestamp() as t_start, 
		(select (point2p, var2p, est_info)::nfiesta_test.estimate_result from nfiesta_test.fn_2p1p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res, 
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta_test.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta_test.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).est_info
from w_res
;
with w_conf as (
		select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p2p_ratio' and sigma = False
			--and param_area <= 48
)
, w_conf_info as (
		select row_number() over() as i, estimate_conf, total_estimate_conf, total_estimate_conf__denom, (select count(*) as cnt from w_conf) from w_conf
)
, w_res as (
	select 
		estimate_conf, clock_timestamp() as t_start, 
		(select (point2p, var2p, est_info)::nfiesta_test.estimate_result from nfiesta_test.fn_2p2p_ratio_var(total_estimate_conf, total_estimate_conf__denom)) as res, 
		clock_timestamp() as t_stop, version
	from w_conf_info
	, (select version from pg_available_extension_versions where name = 'nfiesta' and installed) as extension_version
	--where nfiesta_test.fn_raise_notice(concat('computing estimate, configuration: '::text, estimate_conf::text, ', ',i::text,' / ', cnt::text)) --PROGRESS MONITORING	
)
insert into nfiesta_test.t_result (estimate_conf, point, var, extension_version, calc_started, calc_duration, sampling_units)
select estimate_conf, (res).point, (res).var, version, t_start as calc_started, t_stop - t_start as calc_duration, (res).est_info
from w_res
;
select v_conf_overview.estimate_conf, round(point::numeric, 10) as point, round(var::numeric, 10) as var, sampling_units
from nfiesta_test.t_result 
inner join nfiesta_test.v_conf_overview on (t_result.estimate_conf = v_conf_overview.estimate_conf) 
where estimate_type_str in ('1p2p_ratio', '2p1p_ratio', '2p2p_ratio') and sigma = False
order by v_conf_overview.estimate_conf;
