--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
refresh materialized view nfiesta_test.v_ldsity_conf;
insert into nfiesta_test.t_g_beta(aux_conf, r, c, val)
select conf_id, r, c, val
from (
	select conf_id, clock_timestamp() as t_start, (nfiesta_test.fn_g_beta(conf_id)).*
	from (
		select t_aux_conf.id as conf_id, t_aux_conf.description
		from nfiesta_test.t_aux_conf
		order by id
	) as confs
) as est
;
select aux_conf, count(*), sum(r) as sum_r, sum(c) as sum_c, round(sum(val)::numeric, 10) as sum_val
from nfiesta_test.t_g_beta group by aux_conf
order by aux_conf
;
