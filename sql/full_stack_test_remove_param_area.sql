--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
SELECT nfiesta_test.fn_delete_param_area(gid)
FROM nfiesta_test.f_a_param_area
WHERE param_area_code = 'NFR27';

SELECT nfiesta_test.fn_create_param_area(3, estimation_cells, 'NFRDs')
FROM
	(SELECT array_agg(id) AS estimation_cells 
	FROM nfiesta_test.c_estimation_cell
	WHERE array[id] <@ array[44,45,46,47,48]) AS t1
;

SELECT nfiesta_test.fn_create_param_area(3, ARRAY[47,48,131], 'NFRDs')
;


