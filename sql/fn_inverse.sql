--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-----------------------------------------
-- all 0 in one row
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,0,3],
			array[3,2,4],
			array[0,0,0]
		] AS mat
)
, w_res AS (
	SELECT nfiesta_test.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- all 0 in one iumn
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[0,2,3],
			array[0,2,4],
			array[0,6,5]
		] AS mat
)
, w_res AS (
	SELECT nfiesta_test.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- 1 row is a linear combination of 2 others
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[1,2,3],
			array[2,2,1],
			array[3,4,4]
		] AS mat
)
, w_res AS (
	SELECT nfiesta_test.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- 1 iumn is a linear combination of 2 others
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,2,0],
			array[4,2,2],
			array[1,6,-5]
		] AS mat
)
, w_res AS (
	SELECT nfiesta_test.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
-----------------------------------------
-- OK
-----------------------------------------
WITH w AS (
	SELECT	array[
			array[2,2,1],
			array[4,-2,3],
			array[0,6,3]
		] AS mat
)
, w_res AS (
	SELECT nfiesta_test.fn_inverse(mat) as mi FROM w
)
SELECT round(t.val::numeric, 10) as val, t.i 
	FROM unnest((SELECT mi FROM w_res)) WITH ORDINALITY AS t(val, i) 
	ORDER BY t.i;
