--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
/*
with w_conf as (
select 
	v_conf_overview.estimation_cell, 
	t_total_estimate_conf.estimate_date_begin, 
	t_total_estimate_conf.estimate_date_end, 
	'' as note,
	t_variable.id as target_variable
from nfiesta_test.v_conf_overview
inner join nfiesta_test.t_total_estimate_conf on (v_conf_overview.total_estimate_conf = t_total_estimate_conf.id)
inner join nfiesta_test.t_variable on (
	v_conf_overview.target_variable = t_variable.target_variable and
        case when (t_variable.sub_population_category is not null) then
	v_conf_overview.sub_population_category = t_variable.sub_population_category
        else v_conf_overview.sub_population_category is null end and
        case when (t_variable.area_domain_category is not null) then
        v_conf_overview.area_domain_category = t_variable.area_domain_category
        else v_conf_overview.area_domain_category is null end
	)
where estimate_type_str = '1p_total'
order by estimation_cell, t_variable.id, tac_id
)
select format('select nfiesta_test.fn_1p_est_configuration(%s::integer, %s::date, %s::date, %s::varchar, %s::integer);'
, estimation_cell, quote_literal(estimate_date_begin), quote_literal(estimate_date_end), quote_literal(note), target_variable)
from w_conf
;
*/
--:r ! psql -d contrib_regression --tuples-only --no-psqlrc -f addconfs.sql
 select nfiesta_test.fn_1p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(66::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(66::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(66::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(66::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(72::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(72::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(72::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(72::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(73::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(73::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(73::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(73::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(74::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(74::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(74::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(74::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(80::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(80::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(80::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(80::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(82::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(82::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(82::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(82::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(84::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(84::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(84::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(84::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(86::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(86::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(86::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(86::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(88::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(88::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(88::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(88::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(89::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(89::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(89::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(89::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(91::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(91::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(91::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(91::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(92::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(92::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(92::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(92::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(93::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(93::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(93::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(93::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(95::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(95::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(95::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(95::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(96::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(96::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(96::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(96::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(97::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(97::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(97::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(97::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(103::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(103::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(103::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(103::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(105::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(105::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(105::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(105::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(106::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(106::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(106::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(106::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(110::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(110::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(110::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(110::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(111::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(111::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(111::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(111::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(113::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(113::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(113::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(113::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(117::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(117::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(117::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(117::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(118::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(118::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(118::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(118::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(119::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(119::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(119::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(119::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(120::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(120::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(120::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(120::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);
 select nfiesta_test.fn_1p_est_configuration(122::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer);
 select nfiesta_test.fn_1p_est_configuration(122::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer);
 select nfiesta_test.fn_1p_est_configuration(122::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer);
 select nfiesta_test.fn_1p_est_configuration(122::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 4::integer);

with w_1pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_1pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_1pt as nom
	inner join w_1pt as denom on (nom.estimation_cell = denom.estimation_cell and nom.target_variable = 1 and denom.target_variable = 2)
)
insert into nfiesta_test.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_1pr
order by nom__tec_id, denom__tec_id;

