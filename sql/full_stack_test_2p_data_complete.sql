--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
refresh materialized view nfiesta_test.v_ldsity_conf;
with w_data as (
	select estimate_conf, (nfiesta_test.fn_2p_data(total_estimate_conf)).*
		from (select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p_total' and sigma = False 
			--and param_area <= 48
		) as confs
)
select	estimate_conf,  
	stratum, attribute, nb_sampling_units, sweight_strata_sum, round(lambda_d_plus::numeric, 10) as buffered_area_ha,
        sum(cluster) as cids_sum,
        sum(ldsity_d) as sum__ldsity_d, sum(ldsity_d_plus) as sum__ldsity_d_plus,
	round(sum(ldsity_res_d)::numeric, 5) as sum__ldsity_res_d, round(sum(ldsity_res_d_plus)::numeric, 5) as sum__ldsity_res_d_plus,
        sum(sweight) as sweights_sum,
        sum(sweight_strata_sum / (lambda_d_plus * sweight)) as pix_sum
from w_data group by estimate_conf, stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
order by estimate_conf, stratum, attribute
;
