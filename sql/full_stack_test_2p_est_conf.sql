--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
/*
with w_conf as (
select 
	v_conf_overview.estimation_cell, 
	t_total_estimate_conf.estimate_date_begin, 
	t_total_estimate_conf.estimate_date_end, 
	'' as note,
	t_variable.id as target_variable,
	v_conf_overview.tac_id as aux_conf,
	false as force_synthetic --v_conf_overview.force_synthetic
from nfiesta_test.v_conf_overview
inner join nfiesta_test.t_total_estimate_conf on (v_conf_overview.total_estimate_conf = t_total_estimate_conf.id)
inner join nfiesta_test.t_variable on (
	v_conf_overview.target_variable = t_variable.target_variable and
        case when (t_variable.sub_population_category is not null) then
	v_conf_overview.sub_population_category = t_variable.sub_population_category
        else v_conf_overview.sub_population_category is null end and
        case when (t_variable.area_domain_category is not null) then
        v_conf_overview.area_domain_category = t_variable.area_domain_category
        else v_conf_overview.area_domain_category is null end
	)
where estimate_type_str = '2p_total'
order by estimation_cell, t_variable.id, tac_id
)
select format('select nfiesta_test.fn_2p_est_configuration(%s::integer, %s::date, %s::date, %s::varchar, %s::integer, %s::integer, %s::boolean);'
, estimation_cell, quote_literal(estimate_date_begin), quote_literal(estimate_date_end), quote_literal(note), target_variable, aux_conf, force_synthetic::varchar)
from w_conf
;
*/
--:r ! psql -d contrib_regression --tuples-only --no-psqlrc -f addconfs.sql
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 2::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 2::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 2::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(2::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 3::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 3::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 3::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(3::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(4::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 7::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 7::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 7::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(7::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 9::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 9::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 9::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(9::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 10::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 10::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 10::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 47::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(10::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 11::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 11::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 11::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(11::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 12::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 12::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 12::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(12::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 13::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 13::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 13::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(13::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 14::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 14::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 14::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(14::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 15::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 15::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 15::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(15::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 16::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 16::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 16::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(16::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(26::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 27::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 27::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(27::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 27::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(30::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(31::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 32::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 32::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(32::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 32::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(33::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(34::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 35::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 35::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(35::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 35::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(36::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(37::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(38::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(39::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(40::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(41::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(42::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 43::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 43::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(43::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 43::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 26::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(79::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 30::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 31::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 33::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 34::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(90::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(94::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 36::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(98::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(99::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 37::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(100::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 4::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 45::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(101::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(102::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 38::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(104::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 39::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(107::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(108::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(109::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(112::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 40::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 41::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 48::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(114::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(115::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 49::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(116::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 42::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 43::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 43::integer, false::boolean);
 select nfiesta_test.fn_2p_est_configuration(121::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 43::integer, false::boolean);

 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 26::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 26::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(65::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 26::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 31::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 31::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(81::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 31::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 33::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 33::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(85::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 33::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 1::integer, 34::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 2::integer, 34::integer, true::boolean);
 select nfiesta_test.fn_2p_est_configuration(87::integer, '2011-01-01'::date, '2015-12-31'::date, ''::varchar, 3::integer, 34::integer, true::boolean);

------------------------------------------2p/2p
with w_2pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf 
)
, w_2pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_2pt as nom
	inner join w_2pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.param_area = denom.param_area
		and nom.target_variable = 1 and nom.area_domain_category in (1, 2) and denom.target_variable = 1 and denom.area_domain_category is null
	)
)
insert into nfiesta_test.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_2pr
order by nom__tec_id, denom__tec_id;
------------------------------------------2p/1p
with w_1pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_2pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf
)
, w_21pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_2pt as nom
	inner join w_1pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.target_variable = 1 and denom.target_variable = 2)
)
insert into nfiesta_test.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_21pr
order by nom__tec_id, denom__tec_id;

------------------------------------------1p/2p
with w_1pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '1p_total' order by estimate_conf
)
, w_2pt as (
	select * from nfiesta_test.v_conf_overview where estimate_type_str = '2p_total' order by estimate_conf
)
, w_12pr as (
	select 
		nom.total_estimate_conf as nom__tec_id,
		denom.total_estimate_conf as denom__tec_id
	from 
	w_1pt as nom
	inner join w_2pt as denom on (nom.estimation_cell = denom.estimation_cell 
		and nom.target_variable = 1 and nom.area_domain_category in (1, 2) and denom.target_variable = 1 and denom.area_domain_category is null)
)
insert into nfiesta_test.t_estimate_conf (estimate_type, total_estimate_conf, denominator) 
select 2, nom__tec_id, denom__tec_id
from w_12pr
order by nom__tec_id, denom__tec_id;
