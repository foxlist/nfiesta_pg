--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: @extschema@. Type: SCHEMA; Schema: -; Owner: -
--

--CREATE SCHEMA @extschema@.


--
-- Name: fn_1p_data(integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_1p_data(conf_id integer) RETURNS TABLE(gid bigint, cluster integer, attribute integer, stratum integer, plots_per_cluster integer, plcount bigint, cluster_is_in_cell boolean, ldsity_d double precision, ldsity_d_plus double precision, is_aux boolean, is_target boolean, geom public.geometry, ldsity_res_d double precision, ldsity_res_d_plus double precision, pix double precision, sweight double precision, delta_t__g_beta double precision, nb_sampling_units integer, sweight_strata_sum double precision, lambda_d_plus double precision)
    LANGUAGE plpgsql STABLE ROWS 100000
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	left join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	left join @extschema@.t_model ON t_model.id = t_aux_conf.model
	left join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select 
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2total_2ndph_est_data.reference_year_set,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.f_a_cell ON (f_a_cell.gid = cm_plot2cell_mapping.estimation_cell and w_configuration.cell = f_a_cell.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			t_target_data.sub_population_category = t_variable.sub_population_category
			)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_total_estimate_data on t_total_estimate_data.total_estimate_conf = ANY (w_configuration.t_total_estimate_conf__id)
	inner join @extschema@.t_panel2total_2ndph_est_data ON t_panel2total_2ndph_est_data.total_estimate_data = t_total_estimate_data.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_1p_data as (
	with w_ldsity_cluster as (select * from w_ldsity_cluster)
	select 
		/*w_ldsity_cluster.conf_id, */w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, 
		w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix, w_pix.sweight, NULL::double precision as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	--INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_1p_data(conf_id integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_1p_data(conf_id integer) IS 'Function preparing data for regression estimate.';


--
-- Name: fn_1p_ratio_var(integer, integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_1p_ratio_var(conf_id integer, conf_id__denom integer) RETURNS TABLE(attribute integer, point1p double precision, var1p double precision, point2p double precision, var2p double precision)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
from w_1p_ratio_var
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_1p_ratio_var(conf_id integer, conf_id__denom integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_1p_ratio_var(conf_id integer, conf_id__denom integer) IS 'Function computing ratio and variance using regression estimate.';


--
-- Name: fn_1p_total_var(integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_1p_total_var(conf_id integer) RETURNS TABLE(attribute integer, point1p double precision, var1p double precision, point2p double precision, var2p double precision)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
	, w_data_agg AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg
	)
	, w_est1p AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum group by attribute order by attribute
	)	
	select 
	w_est1p.attribute, 
	point1p,  var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
	from w_est1p;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_1p_total_var(conf_id integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(conf_id integer) IS 'Function computing total and variance using regression estimate.';


--
-- Name: fn_2p_data(integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_2p_data(conf_id integer) RETURNS TABLE(gid bigint, cluster integer, attribute integer, stratum integer, plots_per_cluster integer, plcount bigint, cluster_is_in_cell boolean, ldsity_d double precision, ldsity_d_plus double precision, is_aux boolean, is_target boolean, geom public.geometry, ldsity_res_d double precision, ldsity_res_d_plus double precision, pix double precision, sweight double precision, delta_t__g_beta double precision, nb_sampling_units integer, sweight_strata_sum double precision, lambda_d_plus double precision)
    LANGUAGE plpgsql STABLE ROWS 100000
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2aux_conf.reference_year_set,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_configuration.param_area = f_a_param_area.gid)
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_auxiliary_data.value as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot 
		inner join @extschema@.t_variable on t_variable.auxiliary_variable_category = t_auxiliary_data.auxiliary_variable_category
		inner join w_configuration on t_variable.id = ANY (w_configuration.aux_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			t_target_data.sub_population_category = t_variable.sub_population_category
			)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
, w_X AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_Y AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_I AS (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		(plots_per_cluster^2)::float / plcount::float as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS (
	select 
		w_cell_selection.conf_id,
		attribute as r,
		1 as c,
		aux_total as val 
	from
	(select estimation_cell as cell, v_ldsity_conf.id as attribute, aux_total 
		from @extschema@.t_aux_total 
		inner join @extschema@.v_ldsity_conf on (t_aux_total.auxiliary_variable_category = v_ldsity_conf.auxiliary_variable_category)
	) as t_aux_total
	inner join w_cell_selection on (w_cell_selection.cell_gid = t_aux_total.cell)
	inner join (select distinct conf_id, r from w_X) as foo on (foo.r = attribute and foo.conf_id = w_cell_selection.conf_id)
)
, w_DELTA_T AS (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_t_G_beta AS (
	select 
		w_configuration.id as conf_id, 
		t_g_beta.r, t_g_beta.c, t_g_beta.val 
	from @extschema@.t_g_beta 
	inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
)
, w_DELTA_G_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_Y_T AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_SIGMA_PI AS (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_SIGMA_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_SIGMA_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_Y_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_X_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
, w_residuals_plot AS ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS (
	with
	w_residuals_plot as (select * from w_residuals_plot),
	w_ldsity_plot as (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster as (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
, w_2p_data as (
	with w_ldsity_residuals_cluster as (select * from w_ldsity_residuals_cluster)
	select 
		/*w_ldsity_residuals_cluster.conf_id, */w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
select * from w_2p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_2p_data(conf_id integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_2p_data(conf_id integer) IS 'Function preparing data for regression estimate.';


--
-- Name: fn_2p_ratio_var(integer, integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_2p_ratio_var(conf_id integer, conf_id__denom integer) RETURNS TABLE(attribute integer, point1p double precision, var1p double precision, point2p double precision, var2p double precision)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			(plots_per_cluster^2)::float / plcount::float as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_SIGMA_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_SIGMA_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_SIGMA_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			(plots_per_cluster^2)::float / plcount::float as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_SIGMA_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_SIGMA_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_SIGMA_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.phi * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p, var2p 
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_2p_ratio_var(conf_id integer, conf_id__denom integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_2p_ratio_var(conf_id integer, conf_id__denom integer) IS 'Function computing ratio and variance using regression estimate.';


--
-- Name: fn_2p_total_var(integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_2p_total_var(conf_id integer) RETURNS TABLE(attribute integer, point1p double precision, var1p double precision, point2p double precision, var2p double precision)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_2p_data(' || conf_id || ') where is_target
	)
	, w_I AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data
                order by r, c
	)
	, w_SIGMA AS (
		SELECT distinct
			1 as r,
			cluster as c,
			(plots_per_cluster^2)::float / plcount::float as val
		from	w_data
		order by r, c
	)
	, w_PI AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data
                order by r, c
	)
	, w_SIGMA_PI AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA as A inner join w_PI as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data
                order by r, c
	)
	, w_total_1p AS (
                with w_I_PI AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I      AS A 
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI as A, w_Y_T as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data
	)
	, w_correction_2p AS (
                with w_DELTA_T__G_beta_SIGMA_PI AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta AS A
                        inner join w_SIGMA_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_SIGMA_PI as A, w_Y_T as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p 		AS A
                inner join  w_correction_2p    	AS B    on (A.c = B.c)
		order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESIDUALS----------------------------------
-------------------------------------------------------------------------------
	, w_e AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data
                order by r, c
	)
	, w_I_e AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_PHI AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e        		AS A
                inner join  w_DELTA_T__G_beta__e  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
	, w_zeroResidualsTotalTest AS (
                select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                        w_SIGMA_PI as A, (select c as r, r as c, val_d_plus from w_e) as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_phi AS (
	select w_data.*, w_PHI.val as phi from w_data inner join w_PHI on (cluster = c and attribute = r)
) 
, w_data_agg AS        (
        SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(phi order by cluster) as residuals,
		array_agg(sweight order by cluster) as sweights
        FROM
            w_data_phi
        group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est1p AS    (
	SELECT
	    attribute, sum(total) as point1p, sum(var) AS var1p 
	from w_est1p_stratum group by attribute order by attribute
)
, w_est2p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, residuals, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est2p AS    (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	from w_est2p_stratum group by attribute order by attribute
)
select 
	w_est1p.attribute, 
	point1p,  var1p,
	val as point2p, var2p 
from w_est1p
inner join w_est2p on (w_est1p.attribute = w_est2p.attribute) 
inner join w_total_2p on (w_est2p.attribute = w_total_2p.c)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_2p_total_var(conf_id integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_2p_total_var(conf_id integer) IS 'Function computing total and variance using regression estimate.';


--
-- Name: fn_create_buffered_geometry(public.geometry, integer, integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
	_geom_buff		geometry(MultiPolygon,3035);
	_axis_shift		double precision;
BEGIN

	CASE
	WHEN _tract_type = 100
	THEN
		 RETURN ST_Multi(ST_Buffer(_geom,_point_distance_m));

	WHEN _tract_type = 200
	THEN
		_axis_shift := ( (_point_distance_m^2) / 2.0 )^(1/2.0);


		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu, na opacnou stratnu, nez se vytvari trakt
					ST_Translate(_geom, -_axis_shift, -_axis_shift)
				) AS geom
			)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				x, y, geom,
				x-_point_distance_m AS x_shift,
				y-_point_distance_m AS y_shift,
				ST_SetSRID(ST_MakePoint(x-_axis_shift, y-_axis_shift),3035) AS geom_shift
			FROM
				w_coordinates
		)
		,w_make_line AS (
			SELECT
				ST_Union(ST_MakeLine(geom,geom_shift)) AS line
			FROM 
				w_shift_points
			)
		,w_union_all AS (
			SELECT
				ST_Union(t1.line, ST_Boundary(t2.geom)) AS all_lines
			FROM
				w_make_line AS t1,
				w_only_shifted AS t2
			)
		SELECT
			ST_Multi(ST_buildarea(all_lines))
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;

	WHEN _tract_type = 300
	THEN

		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu doleva
					ST_Translate(_geom, -(x_shift*_point_distance_m), -(y_shift*_point_distance_m))
				) AS geom_unioned
			FROM
				(SELECT unnest(ARRAY[1,1,0]) AS x_shift, unnest(ARRAY[0,1,1]) AS y_shift) AS t1
			)
		,w_union_only_shifted AS (
			SELECT ST_Union(ST_MakeValid(geom_unioned)) AS geom_unioned
			FROM w_only_shifted
		)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				geom,
				ST_SetSRID(ST_MakePoint(x-(x_shift[1]*_point_distance_m), y-(y_shift[1]*_point_distance_m)),3035) AS geom_shift_l,
				ST_SetSRID(ST_MakePoint(x-(x_shift[2]*_point_distance_m), y-(y_shift[2]*_point_distance_m)),3035) AS geom_shift_ld,
				ST_SetSRID(ST_MakePoint(x-(x_shift[3]*_point_distance_m), y-(y_shift[3]*_point_distance_m)),3035) AS geom_shift_d
			FROM
				w_coordinates AS t1,
				(SELECT ARRAY[1,1,0] AS x_shift, ARRAY[0,1,1] AS y_shift) AS t2
		)
		,w_make_line AS (
			-- make the square from 4 lines
			SELECT	ST_MakeLine(geom,geom_shift_l) AS line			FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_l,geom_shift_ld) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_ld,geom_shift_d) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_d,geom) AS line			FROM w_shift_points
			-- diagonal line to prevent st_buildarea to exclude squares
			UNION ALL
			SELECT	ST_MakeLine(geom,geom_shift_ld) AS line			FROM w_shift_points
		)
		,w_union_lines AS (
			SELECT ST_Collect(line) AS line FROM w_make_line
		)
		,w_area_lines AS (
			SELECT
					ST_BuildArea(line) as area_lines
			FROM
					w_union_lines
		)
		,w_area_geoms AS (
			SELECT
				ST_BuildArea(ST_MakeValid(geom_unioned)) AS area_geoms
			FROM
				w_union_only_shifted
		),
		w_union_all AS (
			SELECT
				ST_union(t1.area_lines,t2.area_geoms) AS geom
			FROM
				w_area_lines AS t1,
				w_area_geoms AS t2
		)
		SELECT
			ST_Multi(geom)
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;
	ELSE
		RAISE EXCEPTION 'Not known tract type!';
	END CASE;

END;
$$;


--
-- Name: FUNCTION fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer) IS 'Functions generates a buffered geometry of original geographical domain for specified tract_type.';


--
-- Name: fn_g_beta(integer); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_g_beta(conf_id integer) RETURNS TABLE(r integer, c integer, val double precision)
    LANGUAGE plpgsql STABLE ROWS 100000
    AS $$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description,
		array_agg(t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum, f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		t_auxiliary_data.value as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot
	inner join @extschema@.t_variable on t_auxiliary_data.auxiliary_variable_category = t_variable.auxiliary_variable_category
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	WHERE t_variable.id = ANY (w_configuration.aux_attributes)
)
, w_ldsity_cluster AS (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, cluster from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, buffered_area_m2/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON w_clusters.cluster = cm_cluster2panel_mapping.cluster
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		(plots_per_cluster^2)::float / plcount::float as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
	from w_ldsity_cluster 
	group by conf_id, cluster, plots_per_cluster, plcount) as m
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
	)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_G_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
        select r, c, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$$;


--
-- Name: FUNCTION fn_g_beta(conf_id integer); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_g_beta(conf_id integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by r(ow) and c(column) indices.';


--
-- Name: fn_get_rast_total_clip(integer, regclass); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass) RETURNS TABLE(ldsity_type integer, attribute integer, total double precision)
    LANGUAGE plpgsql PARALLEL SAFE
    AS $_$
BEGIN
RETURN QUERY EXECUTE
'
WITH
	-- cutting geometry on the fly by 50x50 inspire grid
	w_cell AS (
		SELECT
			t2.gid,
			ST_Intersection(t1.geom, t2.geom) AS geom
		FROM
			@extschema@.f_a_cell AS t1
		INNER JOIN
			@extschema@.f_a_cell AS t2
		ON ST_Intersects(t1.geom,t2.geom)
		WHERE
			t1.gid = $1 AND
			t2.cell_type = 300
	),
	-- clipping raster to the geometry area
	w_clip AS (
			SELECT
				t2.rid,
				ST_Clip(t2.rast, NULL::integer[], t1.geom, ARRAY[255,255,255,255], TRUE) AS rast
			FROM 
				w_cell AS t1,
				'|| table_name ||' AS t2
			WHERE
				-- make sure that previous intersection is polygon, not only point or linestring
				(ST_GeometryType(t1.geom) = ''ST_Polygon'' OR
				ST_GeometryType(t1.geom) = ''ST_MultiPolygon'') AND
				ST_Intersects(t1.geom, t2.rast)
		)
		-- reclassification of raster and dump bands to separate rasters
		, w_raster_reclass AS (
			SELECT
				rid,
				ldsity_type,
				attribute,
				ST_Reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, 255) rast
			FROM
				(SELECT
				   	t1.rid, t1.rast, t2.band_number
				FROM
				   w_clip AS t1
				CROSS JOIN
					-- only  4 bands exists
					unnest(ARRAY[1,2,3,4]) AS t2(band_number)
				)AS t1
			INNER JOIN
				-- table of desired ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
				(
				SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
					UNION ALL
				SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
					UNION ALL
				SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
					UNION ALL
				SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
					UNION ALL
				SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
				) AS t2
			ON
				t1.band_number = t2.band_number
		)
		, w_value_count AS (
			-- calculation of present values in raster
			SELECT
				rid,
				ldsity_type,
				attribute,
				(ST_ValueCount(rast, 1, true)) AS val_count,
				(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
			FROM
				w_raster_reclass
			ORDER BY rid, ldsity_type, attribute
		)
		-- summation of values
		SELECT
			ldsity_type,
			attribute,
			sum(pixarea * (val_count).value * (val_count).count)
		FROM
			  w_value_count
		GROUP BY
			ldsity_type,
			attribute
		ORDER BY
			ldsity_type,
			attribute'
			USING cell_gid;
END;
$_$;


--
-- Name: FUNCTION fn_get_rast_total_clip(cell_gid integer, table_name regclass); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_clip(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Clip (pixel belongs to the geometry if the geometry covers at least 50% of it).';


--
-- Name: fn_get_rast_total_intersection(integer, regclass); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass) RETURNS TABLE(ldsity_type integer, attribute integer, total double precision)
    LANGUAGE plpgsql PARALLEL SAFE
    AS $_$
BEGIN
RETURN QUERY EXECUTE '
WITH w_cell_raster_reclass AS (
		SELECT
			t1.rid,
			t1.gid,
			t2.ldsity_type,
			t2.attribute,
			t1.geom,
			covers,
			-- all rasters have only 1 band, the original one was expanded
			-- reclass all, case when noreclass was slowing down the calculation
			ST_reclass(ST_Band(t1.rast,t1.band_number), 1, t2.reclass, ''8BUI''::text, NULL) rast
		FROM
			-- rasters covering whole calculated cell
			(SELECT
				t1.rid,
				t2.gid,
				t2.geom,
				t3.band AS band_number,
				ST_Covers(t2.geom, ST_Convexhull(t1.rast)) AS covers,
				-- expand raster bands into separate rasters
				--ST_Band(t1.rast,t3.band) AS rast
				t1.rast
			FROM					
				'|| table_name ||' AS t1
			INNER JOIN
				(
				SELECT
					gid,
					geom
				FROM
					@extschema@.f_a_cell
					--order by gid
				WHERE
					gid = $1
				) AS t2
			ON
				ST_Intersects(t2.geom, t1.rast)
			CROSS JOIN
				-- only  4 bands exists
				unnest(ARRAY[1,2,3,4]) AS t3(band)
			) t1
		INNER JOIN
		-- table of disared ldsitys encoded from raster (first 3 from band 1 and the last from band 2)
			(
			SELECT 700 AS ldsity_type, 900 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:0, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous
				UNION ALL
			SELECT 700 AS ldsity_type, 1000 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:0, (2-255):0, [255-255]:255'' AS reclass -- deciduous
				UNION ALL
			SELECT 700 AS ldsity_type, 1100 AS attribute, 1 AS band_number, ''[0-1):0, [1-1]:1, [2-2]:1, (2-255):0, [255-255]:255'' AS reclass -- coniferous + deciduous
				UNION ALL
			SELECT 600 AS ldsity_type, 1200 AS attribute, 2 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1400 AS attribute, 3 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- coniferous * treeCoverDensity
				UNION ALL
			SELECT 800 AS ldsity_type, 1500 AS attribute, 4 AS band_number, ''[0-100]:0-100, (100-255):0, [255-255]:255'' AS reclass -- deciduous * treeCoverDensity
			) AS t2
		ON
			t1.band_number = t2.band_number
	-- rasters which are not fully within the cell
	),w_not_covers AS (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_Intersection(rast, geom)) AS intersection,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			NOT covers
		)
	-- rasters which are fully within the cell
	,w_covers AS (
		SELECT
			rid,
			gid,
			ldsity_type,
			attribute,
			geom,
			covers,
			(ST_ValueCount(rast, 1, true)) AS val_count,
			rast
		FROM
			w_cell_raster_reclass	
		WHERE
			covers
	)
	--INSERT INTO @extschema@.t_aux_total(cell, ldsity_type, attribute, aux_total)
	SELECT
		ldsity_type,
		attribute,
		coalesce(sum(rid_sum), 0)	-- 1 min 30 sec
	FROM 
		(
		-- sum of values for each raster covering the cell
		-- speed up solution for rasters completely within cell (no need for st_intersection)
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * count * val)
			FROM 
				(SELECT
					(val_count).value AS val,
					(val_count).count AS count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))/10000 AS pixarea
				) AS t1
			) AS rid_sum
		FROM
			w_covers
		UNION ALL
		SELECT 
			rid,
			gid,
			ldsity_type,
			attribute,
			(SELECT
				sum(pixarea * val)
			FROM
				(SELECT
					(intersection).val AS val,
					ST_Area((intersection).geom) / 10000 AS pixarea
				) AS t2
			) AS rid_sum
		FROM
			w_not_covers		
		) AS w_rid_sum
	GROUP BY
		ldsity_type,
		attribute
	ORDER BY
		ldsity_type,
		attribute' USING cell_gid;
END;
$_$;


--
-- Name: FUNCTION fn_get_rast_total_intersection(cell_gid integer, table_name regclass); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_get_rast_total_intersection(cell_gid integer, table_name regclass) IS 'Function for computing total of raster in geometry. Raster is clipped by geometry using ST_Intersection (pixel is cutted by geometry boundary and only its exact portion belongs to the geometry).';


--
-- Name: fn_inverse(double precision[]); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_inverse(input double precision[]) RETURNS double precision[]
    LANGUAGE plpython3u STRICT
    AS $$
	#import numpy as np
	from numpy import matrix

	#test to singularity
	# if the rank of matrix is the same value as the row dimension (no 0 rows or columns, rows linearly independent)
	# than it should be able to compute inversion

	#def is_invertible(a):
	#	return a.shape[0] == a.shape[1] and np.linalg.matrix_rank(a) == a.shape[0]	

	#if is_invertible(a):

	#test deprecated
	#matrix_rank uses svd decomposition but np.linalg.inv(a) uses LU factorization (LAPACK)

	a = matrix(input)
	try:
		inversed=a.I.tolist()
	except Exception:
		plpy.info("not able to compute inversion of matrix", input)
		return None
	return list(inversed)


	#else:
	#	raise ValueError('Input matrix is singular, inversion of it does not exist!')

$$;


--
-- Name: FUNCTION fn_inverse(input double precision[]); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_inverse(input double precision[]) IS 'Function for computing inverse of matrix using python3.';


--
-- Name: fn_raise_notice(text); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_raise_notice(message text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin
    raise notice '%	%', clock_timestamp(), message;
    return true;
end;
$$;


--
-- Name: fn_trg__t_estimate_conf__before(); Type: FUNCTION; Schema: @extschema@. Owner: -
--

CREATE FUNCTION @extschema@.fn_trg__t_estimate_conf__before() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
	_check_zero	integer;

BEGIN
	-- test na after trigger
	IF TG_WHEN <> 'BEFORE'
	THEN
		RAISE EXCEPTION '@extschema@.fn_trg__t_estimate_conf__before';
	END IF;

	IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE'
	THEN

		-- no plot in param area
		IF NOT EXISTS (SELECT plot_gid FROM @extschema@.t_map_plot_param_area WHERE param_area_gid = NEW.param_area)
		THEN
			RAISE EXCEPTION '@extschema@.fn_trg__t_estimate_conf__before: in the table t_map_plot_param_area '
			'does not exist any record, which would enable the calculation of the estimate for given parametrization area (param_area = %).', NEW.param_area;
		END IF;

		-- at least 100 clusters in param area
		IF NOT EXISTS (	SELECT
					param_area_gid
				FROM
					@extschema@.t_map_plot_param_area AS t1
				INNER JOIN
					@extschema@.f_p_plot AS t2
				ON
					t1.plot_gid = t2.gid
				WHERE
					param_area_gid = NEW.param_area
				GROUP BY param_area_gid
				HAVING
					count(DISTINCT cluster) >= 100
				)
		THEN
			RAISE EXCEPTION '@extschema@.fn_trg__t_estimate_conf__before: given parametrization area does not have at 
					least 100 sampling units (param_area = %).', NEW.param_area;
		END IF;

		-- cell not in param area
		IF NOT (SELECT
				ST_Within(t2.geom, t1.geom)
			FROM
				@extschema@.f_a_param_area AS t1,
				@extschema@.f_a_cell AS t2
			WHERE
				t1.gid = NEW.param_area AND
				t2.gid = NEW.cell
			)
		THEN
			RAISE EXCEPTION '@extschema@.fn_trg__t_estimate_conf__before: given cell is not completely within given parametrization area '
			'(cell = %, param_area = %).', NEW.cell, NEW.param_area;
		END IF;
	END IF;

    RETURN NEW;
END;
$$;


--
-- Name: FUNCTION fn_trg__t_estimate_conf__before(); Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON FUNCTION @extschema@.fn_trg__t_estimate_conf__before() IS 'Trigger function for estimate configuration checks before insert or update to the table t_estimate_conf.';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: c_area_domain; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_area_domain (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_area_domain; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_area_domain IS 'Table of area domains, agregates of attribute domain categories detectable on plot center.';


--
-- Name: COLUMN c_area_domain.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain.id IS 'Identifier of area domain, primary key.';


--
-- Name: COLUMN c_area_domain.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain.label IS 'Label of area domain.';


--
-- Name: COLUMN c_area_domain.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain.description IS 'Description of area domain.';


--
-- Name: c_area_domain_category; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_area_domain_category (
    id integer NOT NULL,
    domain integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_area_domain_category IS 'Table of area domain categories, attribute domains detectable on plot center.';


--
-- Name: COLUMN c_area_domain_category.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain_category.id IS 'Identifier of area domain category, primary key.';


--
-- Name: COLUMN c_area_domain_category.domain; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain_category.domain IS 'Identifier of area domain, foreign key to table c_area_domain.';


--
-- Name: COLUMN c_area_domain_category.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain_category.label IS 'Label of area domain.';


--
-- Name: COLUMN c_area_domain_category.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_area_domain_category.description IS 'Description of area domain.';


--
-- Name: c_area_domain_category_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_area_domain_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_area_domain_category_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_area_domain_category_id_seq OWNED BY @extschema@.c_area_domain_category.id;


--
-- Name: c_area_domain_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_area_domain_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_area_domain_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_area_domain_id_seq OWNED BY @extschema@.c_area_domain.id;


--
-- Name: c_aux_phase_type; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_aux_phase_type (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_aux_phase_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_aux_phase_type IS 'Table of auxiliary estimation phases types (wall2wall, dense grid).';


--
-- Name: COLUMN c_aux_phase_type.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_aux_phase_type.id IS 'Identifier of estimation phase type.';


--
-- Name: COLUMN c_aux_phase_type.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_aux_phase_type.label IS 'Label of estimation phase type.';


--
-- Name: COLUMN c_aux_phase_type.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_aux_phase_type.description IS 'Description of estimation phase type.';


--
-- Name: c_aux_phase_type_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_aux_phase_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_aux_phase_type_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_aux_phase_type_id_seq OWNED BY @extschema@.c_aux_phase_type.id;


--
-- Name: c_auxiliary_variable; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_auxiliary_variable (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_auxiliary_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_auxiliary_variable IS 'Table of auxiliary variables, aggregates of local densities coming from other sources, primarly from remote sensing.';


--
-- Name: COLUMN c_auxiliary_variable.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable.id IS 'Identifier of auxiliary variable, primary key.';


--
-- Name: COLUMN c_auxiliary_variable.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable.label IS 'Label of auxiliary variable.';


--
-- Name: COLUMN c_auxiliary_variable.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable.description IS 'Description of auxiliary variable.';


--
-- Name: c_auxiliary_variable_category; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_auxiliary_variable_category (
    id integer NOT NULL,
    auxiliary_variable integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_auxiliary_variable_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_auxiliary_variable_category IS 'Table of auxiliary variable categories, local densities coming from other sources, primarly from remote sensing.';


--
-- Name: COLUMN c_auxiliary_variable_category.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.id IS 'Identifier of auxiliary variable category, primary key.';


--
-- Name: COLUMN c_auxiliary_variable_category.auxiliary_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.auxiliary_variable IS 'Identifier of auxiliary variable, foreign key to table c_auxiliary_variable.';


--
-- Name: COLUMN c_auxiliary_variable_category.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.label IS 'Label of auxiliary variable category.';


--
-- Name: COLUMN c_auxiliary_variable_category.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_auxiliary_variable_category.description IS 'Description of auxiliary variable category.';


--
-- Name: c_auxiliary_variable_category_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_auxiliary_variable_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_auxiliary_variable_category_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_auxiliary_variable_category_id_seq OWNED BY @extschema@.c_auxiliary_variable_category.id;


--
-- Name: c_auxiliary_variable_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_auxiliary_variable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_auxiliary_variable_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_auxiliary_variable_id_seq OWNED BY @extschema@.c_auxiliary_variable.id;


--
-- Name: c_country; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_country (
    id integer NOT NULL,
    label character varying(100),
    description text
);


--
-- Name: TABLE c_country; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_country IS 'Looup table of country codes.';


--
-- Name: COLUMN c_country.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_country.id IS 'Primary key, identifier of the country.';


--
-- Name: COLUMN c_country.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_country.label IS 'Label (code) of the country.';


--
-- Name: COLUMN c_country.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_country.description IS 'Description (name) of the country.';


--
-- Name: c_country_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_country_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_country_id_seq OWNED BY @extschema@.c_country.id;


--
-- Name: c_estimate_type; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_estimate_type (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_estimate_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_estimate_type IS 'Table of estimate types (total, ratio).';


--
-- Name: COLUMN c_estimate_type.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimate_type.id IS 'Identifier of estimate types.';


--
-- Name: COLUMN c_estimate_type.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimate_type.label IS 'Label of estimate types.';


--
-- Name: COLUMN c_estimate_type.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimate_type.description IS 'Description of estimatie types.';


--
-- Name: c_estimate_type_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_estimate_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_estimate_type_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_estimate_type_id_seq OWNED BY @extschema@.c_estimate_type.id;


--
-- Name: c_estimation_cell; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_estimation_cell (
    id integer NOT NULL,
    estimation_cell_collection integer NOT NULL,
    label character varying(20) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_estimation_cell IS 'Table of estimation cells.';


--
-- Name: COLUMN c_estimation_cell.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell.id IS 'Identifier of estimation cell, primary key.';


--
-- Name: COLUMN c_estimation_cell.estimation_cell_collection; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell.estimation_cell_collection IS 'Identifier of estimation_cell_collection, foreign key to table c_estimation_cell_collection.';


--
-- Name: COLUMN c_estimation_cell.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell.label IS 'Label of estimation cell.';


--
-- Name: COLUMN c_estimation_cell.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell.description IS 'Description of estimation cell.';


--
-- Name: c_estimation_cell_collection; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_estimation_cell_collection (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_estimation_cell_collection; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_estimation_cell_collection IS 'Table of estimate cell collections.';


--
-- Name: COLUMN c_estimation_cell_collection.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.id IS 'Identifier of estimate cell collection.';


--
-- Name: COLUMN c_estimation_cell_collection.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.label IS 'Label of estimate cell collection.';


--
-- Name: COLUMN c_estimation_cell_collection.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_estimation_cell_collection.description IS 'Description of estimate cell collection.';


--
-- Name: c_estimation_cell_collection_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_estimation_cell_collection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_estimation_cell_collection_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_estimation_cell_collection_id_seq OWNED BY @extschema@.c_estimation_cell_collection.id;


--
-- Name: c_estimation_cell_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_estimation_cell_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_estimation_cell_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_estimation_cell_id_seq OWNED BY @extschema@.c_estimation_cell.id;


--
-- Name: c_param_area_type; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_param_area_type (
    id integer NOT NULL,
    label character varying(100),
    description text
);


--
-- Name: TABLE c_param_area_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_param_area_type IS 'Table of parametrization area types. Areas can be generated by the same logic or have the same size (e.g. 100x100 km).';


--
-- Name: COLUMN c_param_area_type.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_param_area_type.id IS 'Primary key, identifier of the param_area type.';


--
-- Name: COLUMN c_param_area_type.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_param_area_type.label IS 'Short description of the type (e.g. Inspire 100x100 with buffer).';


--
-- Name: COLUMN c_param_area_type.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_param_area_type.description IS 'Description of the type.';


--
-- Name: c_param_area_type_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_param_area_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_param_area_type_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_param_area_type_id_seq OWNED BY @extschema@.c_param_area_type.id;


--
-- Name: c_state_or_change; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_state_or_change (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_state_or_change; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_state_or_change IS 'Table of categories coding if target variable has state or change character.';


--
-- Name: COLUMN c_state_or_change.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_state_or_change.id IS 'Identifier of the state or change category, primary key.';


--
-- Name: COLUMN c_state_or_change.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_state_or_change.label IS 'Label of the state or change category.';


--
-- Name: COLUMN c_state_or_change.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_state_or_change.description IS 'Description of the state or change category.';


--
-- Name: c_state_or_change_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_state_or_change_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_state_or_change_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_state_or_change_id_seq OWNED BY @extschema@.c_state_or_change.id;


--
-- Name: c_sub_population; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_sub_population (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_sub_population; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_sub_population IS 'Table of sub-populations, agregates of attribute domain categories detectable on measured objects (several entities on 1 plot center).';


--
-- Name: COLUMN c_sub_population.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population.id IS 'Identifier of sub-population, primary key.';


--
-- Name: COLUMN c_sub_population.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population.label IS 'Label of sub-population.';


--
-- Name: COLUMN c_sub_population.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population.description IS 'Description of sub-population.';


--
-- Name: c_sub_population_category; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_sub_population_category (
    id integer NOT NULL,
    sub_population integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_sub_population_category IS 'Table of sub-population categories, attribute domains detectable on measured objects (several entities on 1 plot center).';


--
-- Name: COLUMN c_sub_population_category.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population_category.id IS 'Identifier of sub-population category, primary key.';


--
-- Name: COLUMN c_sub_population_category.sub_population; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population_category.sub_population IS 'Identifier of sub-population, foreign key to table c_sub_population.';


--
-- Name: COLUMN c_sub_population_category.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population_category.label IS 'Label of sub-population category.';


--
-- Name: COLUMN c_sub_population_category.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_sub_population_category.description IS 'Description of sub-population category.';


--
-- Name: c_sub_population_category_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_sub_population_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_sub_population_category_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_sub_population_category_id_seq OWNED BY @extschema@.c_sub_population_category.id;


--
-- Name: c_sub_population_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_sub_population_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_sub_population_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_sub_population_id_seq OWNED BY @extschema@.c_sub_population.id;


--
-- Name: c_target_variable; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_target_variable (
    id integer NOT NULL,
    state_or_change integer NOT NULL,
    variable_type integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_target_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_target_variable IS 'Table of target variables, local densities coming from field/photogrammetric survey - data sources primarly provided by countries.';


--
-- Name: COLUMN c_target_variable.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_target_variable.id IS 'Identifier of target variable, primary key.';


--
-- Name: COLUMN c_target_variable.state_or_change; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_target_variable.state_or_change IS 'Identifier of state or change category, foreign key to table c_state_or_change.';


--
-- Name: COLUMN c_target_variable.variable_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_target_variable.variable_type IS 'Identifier of variable type, foreign key to table c_variable_type.';


--
-- Name: COLUMN c_target_variable.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_target_variable.label IS 'Label of target variable.';


--
-- Name: COLUMN c_target_variable.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_target_variable.description IS 'Description of target variable.';


--
-- Name: c_target_variable_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_target_variable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_target_variable_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_target_variable_id_seq OWNED BY @extschema@.c_target_variable.id;


--
-- Name: c_variable_type; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.c_variable_type (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);


--
-- Name: TABLE c_variable_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.c_variable_type IS 'Table of target variable types.';


--
-- Name: COLUMN c_variable_type.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_variable_type.id IS 'Identifier of variable type, primary key.';


--
-- Name: COLUMN c_variable_type.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_variable_type.label IS 'Label of variable type.';


--
-- Name: COLUMN c_variable_type.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.c_variable_type.description IS 'Description of variable type.';


--
-- Name: c_variable_type_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.c_variable_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_variable_type_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.c_variable_type_id_seq OWNED BY @extschema@.c_variable_type.id;


--
-- Name: cm_cell2param_area_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_cell2param_area_mapping (
    id integer NOT NULL,
    estimation_cell integer NOT NULL,
    param_area integer NOT NULL
);


--
-- Name: TABLE cm_cell2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_cell2param_area_mapping IS 'Table with mapping between cells and parametrization areas.';


--
-- Name: COLUMN cm_cell2param_area_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cell2param_area_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_cell2param_area_mapping.estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cell2param_area_mapping.estimation_cell IS 'Identifier of estimation_cell, foreign key to table c_estimation_cell.';


--
-- Name: COLUMN cm_cell2param_area_mapping.param_area; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cell2param_area_mapping.param_area IS 'Identifier of parametrization area, foreign key to table f_a_param_area.';


--
-- Name: cm_cell2param_area_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_cell2param_area_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_cell2param_area_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_cell2param_area_mapping_id_seq OWNED BY @extschema@.cm_cell2param_area_mapping.id;


--
-- Name: cm_cluster2panel_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_cluster2panel_mapping (
    id integer NOT NULL,
    panel integer NOT NULL,
    cluster integer NOT NULL,
    sampling_weight double precision NOT NULL
);


--
-- Name: TABLE cm_cluster2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_cluster2panel_mapping IS 'Table with mapping between clusters and panels.';


--
-- Name: COLUMN cm_cluster2panel_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cluster2panel_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_cluster2panel_mapping.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cluster2panel_mapping.panel IS 'Identifier of panel, foreign key to table t_panel.';


--
-- Name: COLUMN cm_cluster2panel_mapping.cluster; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cluster2panel_mapping.cluster IS 'Identifier of cluster (sampling unit), foreign key to table t_cluster.';


--
-- Name: COLUMN cm_cluster2panel_mapping.sampling_weight; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_cluster2panel_mapping.sampling_weight IS 'Sampling weight - for each cluster can vary among panels.';


--
-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_cluster2panel_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_cluster2panel_mapping_id_seq OWNED BY @extschema@.cm_cluster2panel_mapping.id;


--
-- Name: cm_plot2cell_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_plot2cell_mapping (
    id integer NOT NULL,
    plot integer,
    estimation_cell integer
);


--
-- Name: TABLE cm_plot2cell_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_plot2cell_mapping IS 'Table caching spatial membership between plots and cells.';


--
-- Name: COLUMN cm_plot2cell_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cell_mapping.id IS 'Primary key, identifier of the record.';


--
-- Name: COLUMN cm_plot2cell_mapping.plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cell_mapping.plot IS 'Foreign key to plots.';


--
-- Name: COLUMN cm_plot2cell_mapping.estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cell_mapping.estimation_cell IS 'Foreign key to cells.';


--
-- Name: cm_plot2cell_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_plot2cell_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_plot2cell_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_plot2cell_mapping_id_seq OWNED BY @extschema@.cm_plot2cell_mapping.id;


--
-- Name: cm_plot2cluster_config_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_plot2cluster_config_mapping (
    id integer NOT NULL,
    cluster_configuration integer NOT NULL,
    plot integer NOT NULL
);


--
-- Name: TABLE cm_plot2cluster_config_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_plot2cluster_config_mapping IS 'Table with mapping between plots and cluster configurations.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cluster_config_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.cluster_configuration; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cluster_config_mapping.cluster_configuration IS 'Identifier of cluster_configuration, foreign key to table t_cluster_configuration.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2cluster_config_mapping.plot IS 'Identifier of plot, foreign key to table f_p_plot.';


--
-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_plot2cluster_config_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_plot2cluster_config_mapping_id_seq OWNED BY @extschema@.cm_plot2cluster_config_mapping.id;


--
-- Name: cm_plot2param_area_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_plot2param_area_mapping (
    id integer NOT NULL,
    plot integer,
    param_area integer
);


--
-- Name: TABLE cm_plot2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_plot2param_area_mapping IS 'Table caching spatial membership of plots into parametrization areas.';


--
-- Name: COLUMN cm_plot2param_area_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2param_area_mapping.id IS 'Primary key, identifier of the record.';


--
-- Name: COLUMN cm_plot2param_area_mapping.plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2param_area_mapping.plot IS 'Foreign key to plots.';


--
-- Name: COLUMN cm_plot2param_area_mapping.param_area; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_plot2param_area_mapping.param_area IS 'Foreign key to parametrization areas.';


--
-- Name: cm_plot2param_area_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_plot2param_area_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_plot2param_area_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_plot2param_area_mapping_id_seq OWNED BY @extschema@.cm_plot2param_area_mapping.id;


--
-- Name: cm_refyearset2panel_mapping; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.cm_refyearset2panel_mapping (
    id integer NOT NULL,
    panel integer NOT NULL,
    reference_year_set integer NOT NULL
);


--
-- Name: TABLE cm_refyearset2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.cm_refyearset2panel_mapping IS 'Table with mapping between panels and reference year sets. Representativnes is met both spatially (panel) and within the time scale (ref year set).';


--
-- Name: COLUMN cm_refyearset2panel_mapping.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_refyearset2panel_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_refyearset2panel_mapping.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_refyearset2panel_mapping.panel IS 'Identifier of inventory panel, foreign key to table t_panel.';


--
-- Name: COLUMN cm_refyearset2panel_mapping.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.cm_refyearset2panel_mapping.reference_year_set IS 'Identifier of reference year set, foreign key to t_reference_year_set.';


--
-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.cm_refyearset2panel_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.cm_refyearset2panel_mapping_id_seq OWNED BY @extschema@.cm_refyearset2panel_mapping.id;


--
-- Name: f_a_cell; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.f_a_cell (
    gid integer NOT NULL,
    cell_id integer,
    cell_inside boolean NOT NULL,
    cell_intersects boolean NOT NULL,
    geom public.geometry(MultiPolygon,3035) NOT NULL,
    estimation_cell integer NOT NULL
);


--
-- Name: TABLE f_a_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.f_a_cell IS 'Table of areas, where the estimation is done (cells), contains geometries.';


--
-- Name: COLUMN f_a_cell.gid; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.gid IS 'Primary key, identifier of the cell.';


--
-- Name: COLUMN f_a_cell.cell_id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.cell_id IS 'One cell can be stored in multiple parts if geometry is large and complicated.';


--
-- Name: COLUMN f_a_cell.cell_inside; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.cell_inside IS 'Cell is completely inside union of all strata.';


--
-- Name: COLUMN f_a_cell.cell_intersects; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.cell_intersects IS 'Cell intersects union of all strata.';


--
-- Name: COLUMN f_a_cell.geom; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.geom IS 'Polygon geometry of the cell.';


--
-- Name: COLUMN f_a_cell.estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_cell.estimation_cell IS 'Estimation cell, foreign key to table c_estimation_cell.';


--
-- Name: f_a_cell_gid_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.f_a_cell_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: f_a_cell_gid_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.f_a_cell_gid_seq OWNED BY @extschema@.f_a_cell.gid;


--
-- Name: f_a_param_area; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.f_a_param_area (
    gid integer NOT NULL,
    param_area_id integer,
    param_area_code character varying(100) NOT NULL,
    param_area_type integer NOT NULL,
    geom public.geometry(MultiPolygon,3035) NOT NULL
);


--
-- Name: TABLE f_a_param_area; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.f_a_param_area IS 'Table of areas, where the parametrization of model (set of explanatory variables) is done. Contains geometries.';


--
-- Name: COLUMN f_a_param_area.gid; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_param_area.gid IS 'Primary key, identifier of the param_area.';


--
-- Name: COLUMN f_a_param_area.param_area_id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_param_area.param_area_id IS 'One parametrization area can be stored in multiple parts if geometry is large and complicated.';


--
-- Name: COLUMN f_a_param_area.param_area_code; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_param_area.param_area_code IS 'Code of parametrization area e.g. nuts code.';


--
-- Name: COLUMN f_a_param_area.param_area_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_param_area.param_area_type IS 'Foreign key, identifier of the param_area_type.';


--
-- Name: COLUMN f_a_param_area.geom; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_a_param_area.geom IS 'Polygon geometry of the param_area.';


--
-- Name: f_a_param_area_gid_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.f_a_param_area_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: f_a_param_area_gid_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.f_a_param_area_gid_seq OWNED BY @extschema@.f_a_param_area.gid;


--
-- Name: f_p_plot; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.f_p_plot (
    gid integer NOT NULL,
    pid character varying(50),
    plot character varying(20) NOT NULL,
    geom public.geometry(Point,3035),
    cluster integer,
    coordinates_degraded boolean,
    comment text
);


--
-- Name: TABLE f_p_plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.f_p_plot IS 'Table of inventarization plots.';


--
-- Name: COLUMN f_p_plot.gid; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_p_plot.gid IS 'Primary key, identifier of the plot DB wide.';


--
-- Name: COLUMN f_p_plot.pid; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_p_plot.pid IS 'Identifier of the plot in stratum.';


--
-- Name: COLUMN f_p_plot.plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_p_plot.plot IS 'Identifier of the plot within cluster.';


--
-- Name: COLUMN f_p_plot.geom; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.f_p_plot.geom IS 'Geometry of the plot.';


--
-- Name: f_p_plot_gid_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.f_p_plot_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: f_p_plot_gid_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.f_p_plot_gid_seq OWNED BY @extschema@.f_p_plot.gid;


--
-- Name: t_aux_conf; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_aux_conf (
    id integer NOT NULL,
    param_area integer,
    model integer,
    description text
);


--
-- Name: TABLE t_aux_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_aux_conf IS 'Parametrization configuration - combination of parametrization area and model (explanatory variables).';


--
-- Name: COLUMN t_aux_conf.param_area; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_conf.param_area IS 'Foreign key to parametrization area.';


--
-- Name: COLUMN t_aux_conf.model; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_conf.model IS 'Model.';


--
-- Name: COLUMN t_aux_conf.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_conf.description IS 'Description of estimate.';


--
-- Name: t_aux_conf_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_aux_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_aux_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_aux_conf_id_seq OWNED BY @extschema@.t_aux_conf.id;


--
-- Name: t_aux_total; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_aux_total (
    id integer NOT NULL,
    estimation_cell integer NOT NULL,
    auxiliary_variable_category integer,
    aux_total double precision
);


--
-- Name: TABLE t_aux_total; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_aux_total IS 'Table with totals of auxiliaries (wall2wall) within the cell.';


--
-- Name: COLUMN t_aux_total.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_total.id IS 'Primary key, identifier of the record.';


--
-- Name: COLUMN t_aux_total.estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_total.estimation_cell IS 'Foreign key to cell (target estimate area).';


--
-- Name: COLUMN t_aux_total.auxiliary_variable_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_total.auxiliary_variable_category IS 'Foreign key to attribute of local density.';


--
-- Name: COLUMN t_aux_total.aux_total; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_aux_total.aux_total IS 'Total of auxilliary local density.';


--
-- Name: t_aux_total_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_aux_total_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_aux_total_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_aux_total_id_seq OWNED BY @extschema@.t_aux_total.id;


--
-- Name: t_auxiliary_data; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_auxiliary_data (
    id integer NOT NULL,
    plot integer NOT NULL,
    auxiliary_variable_category integer NOT NULL,
    value double precision NOT NULL
);


--
-- Name: TABLE t_auxiliary_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_auxiliary_data IS 'Table with the auxiliary plot data.';


--
-- Name: t_auxiliary_data_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_auxiliary_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_auxiliary_data_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_auxiliary_data_id_seq OWNED BY @extschema@.t_auxiliary_data.id;


--
-- Name: t_cluster; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_cluster (
    id integer NOT NULL,
    cluster character varying(20),
    comment text
);


--
-- Name: TABLE t_cluster; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_cluster IS 'Table of sampling clusters.';


--
-- Name: COLUMN t_cluster.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster.id IS 'Primary key, identifier of the cluster.';


--
-- Name: COLUMN t_cluster.cluster; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster.cluster IS 'Average of relative plot sampling weights per cluster. Is used to compute pix and pixy.';


--
-- Name: t_cluster_configuration; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_cluster_configuration (
    id integer NOT NULL,
    cluster_configuration character varying(20) NOT NULL,
    cluster_design boolean NOT NULL,
    cluster_rotation boolean,
    geom public.geometry(MultiPoint),
    plots_per_cluster integer NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_cluster_configuration; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_cluster_configuration IS 'Table of cluster configurations.';


--
-- Name: COLUMN t_cluster_configuration.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.id IS 'Identifier of cluster configuration, primary key.';


--
-- Name: COLUMN t_cluster_configuration.cluster_configuration; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.cluster_configuration IS 'Character identifier of cluster configuration.';


--
-- Name: COLUMN t_cluster_configuration.cluster_design; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.cluster_design IS 'Boolean value coding if clusters are used.';


--
-- Name: COLUMN t_cluster_configuration.cluster_rotation; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.cluster_rotation IS 'Boolean value coding if there is random rotation of plots.';


--
-- Name: COLUMN t_cluster_configuration.geom; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.geom IS 'The geometry of a standard cluster (local unprojected Cartesian coordinate system). 
Axis x (first coordinate) aims to the east, y axis (second coordinate) aims to the north. Units are meters. 
Plots of the cluster would be represented by point geometries positioned with reference to the 0,0 anchor point. 
One of the plots may correspond to the cluster anchor point itself.';


--
-- Name: COLUMN t_cluster_configuration.plots_per_cluster; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.plots_per_cluster IS 'Nominal (and fixed) number of sample plots in each cluster.';


--
-- Name: COLUMN t_cluster_configuration.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.label IS 'Label of the cluster configuration.';


--
-- Name: COLUMN t_cluster_configuration.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_cluster_configuration.comment IS 'Optional commentary.';


--
-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_cluster_configuration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_cluster_configuration_id_seq OWNED BY @extschema@.t_cluster_configuration.id;


--
-- Name: t_cluster_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_cluster_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_cluster_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_cluster_id_seq OWNED BY @extschema@.t_cluster.id;


--
-- Name: t_estimate_conf; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_estimate_conf (
    id integer NOT NULL,
    estimate_type integer NOT NULL,
    total_estimate_conf integer NOT NULL,
    denominator integer,
    CONSTRAINT check__t_estimate_conf__denominator CHECK (
CASE
    WHEN (estimate_type = 1) THEN (denominator IS NULL)
    ELSE (denominator IS NOT NULL)
END)
);


--
-- Name: TABLE t_estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_estimate_conf IS 'Table of estimate configurations (configuration of total or combination of two totals in case of ratio).';


--
-- Name: COLUMN t_estimate_conf.estimate_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_estimate_conf.estimate_type IS 'Estimate type (total or ratio), foreign key to table c_estimate_type.';


--
-- Name: COLUMN t_estimate_conf.total_estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_estimate_conf.total_estimate_conf IS 'Estimate of specific total, foreign key to table t_total_estimate_conf.';


--
-- Name: COLUMN t_estimate_conf.denominator; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_estimate_conf.denominator IS 'Optional field, in case of ratio the total used as a denominator, foreign key to table t_total_estimate_conf.';


--
-- Name: t_estimate_conf_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_estimate_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_estimate_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_estimate_conf_id_seq OWNED BY @extschema@.t_estimate_conf.id;


--
-- Name: t_g_beta; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_g_beta (
    id integer NOT NULL,
    aux_conf integer,
    r integer,
    c integer,
    val double precision
);


--
-- Name: TABLE t_g_beta; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_g_beta IS 'Table with the precalculated G_Beta for given auxiliary configuration.';


--
-- Name: COLUMN t_g_beta.aux_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_g_beta.aux_conf IS 'Parametrization configuration, foreign key to table t_param_conf.';


--
-- Name: COLUMN t_g_beta.r; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_g_beta.r IS 'attribute';


--
-- Name: COLUMN t_g_beta.c; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_g_beta.c IS 'cluster';


--
-- Name: t_g_beta_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_g_beta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_g_beta_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_g_beta_id_seq OWNED BY @extschema@.t_g_beta.id;


--
-- Name: t_inventory_campaign; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_inventory_campaign (
    id integer NOT NULL,
    inventory character varying(20) NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_inventory_campaign; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_inventory_campaign IS 'Table of inventory campaigns or cycles.';


--
-- Name: COLUMN t_inventory_campaign.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_inventory_campaign.id IS 'Identifier of inventory campaign, primary key.';


--
-- Name: COLUMN t_inventory_campaign.inventory; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_inventory_campaign.inventory IS 'Character identifier of inventory campaign.';


--
-- Name: COLUMN t_inventory_campaign.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_inventory_campaign.label IS 'Label of inventory campaign.';


--
-- Name: COLUMN t_inventory_campaign.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_inventory_campaign.comment IS 'Optional commentary.';


--
-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_inventory_campaign_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_inventory_campaign_id_seq OWNED BY @extschema@.t_inventory_campaign.id;


--
-- Name: t_model; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_model (
    id integer NOT NULL,
    description text
);


--
-- Name: TABLE t_model; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_model IS 'Table with specified models (defined sets of explanatory variables, just a container with unique id).';


--
-- Name: COLUMN t_model.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_model.id IS 'Primary key, identifier of the record.';


--
-- Name: COLUMN t_model.description; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_model.description IS 'Description of model.';


--
-- Name: t_model_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_model_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_model_id_seq OWNED BY @extschema@.t_model.id;


--
-- Name: t_model_variables; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_model_variables (
    id integer NOT NULL,
    variable integer,
    model integer
);


--
-- Name: TABLE t_model_variables; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_model_variables IS 'Table of explanatory variables used in a model.';


--
-- Name: COLUMN t_model_variables.variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_model_variables.variable IS 'Explanatory variable used in a model, foreign key to table t_variable.';


--
-- Name: COLUMN t_model_variables.model; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_model_variables.model IS 'Model.';


--
-- Name: t_model_variables_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_model_variables_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_model_variables_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_model_variables_id_seq OWNED BY @extschema@.t_model_variables.id;


--
-- Name: t_panel; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_panel (
    id integer NOT NULL,
    stratum integer NOT NULL,
    parent_panel integer,
    panel character varying(20) NOT NULL,
    cluster_configuration integer NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_panel IS 'Table of sampling panels (representative sets of sampling locations/units) within stratum.';


--
-- Name: COLUMN t_panel.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.id IS 'Identifier of panel, primary key.';


--
-- Name: COLUMN t_panel.stratum; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.stratum IS 'Identifier of sampling stratum, foreign key to table t_stratum.';


--
-- Name: COLUMN t_panel.parent_panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.parent_panel IS 'Identifier of a parent panel, foreign key to the same table.';


--
-- Name: COLUMN t_panel.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.panel IS 'Character identifier of panel.';


--
-- Name: COLUMN t_panel.cluster_configuration; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.cluster_configuration IS 'Identifier of cluster configuration, foreign key to table t_cluster_configuration.';


--
-- Name: COLUMN t_panel.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.label IS 'Label of the panel.';


--
-- Name: COLUMN t_panel.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel.comment IS 'Optional commentary.';


--
-- Name: t_panel2aux_conf; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_panel2aux_conf (
    id integer NOT NULL,
    aux_conf integer NOT NULL,
    panel integer NOT NULL,
    reference_year_set integer
);


--
-- Name: TABLE t_panel2aux_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_panel2aux_conf IS 'Table of mapping between panels and parametrization configuration.';


--
-- Name: COLUMN t_panel2aux_conf.aux_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2aux_conf.aux_conf IS 'Identifier of the configuration of parametrization (param_area and model), foreign key to table t_aux_conf.';


--
-- Name: COLUMN t_panel2aux_conf.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2aux_conf.panel IS 'Sampling panel, foreign key to table t_panel.';


--
-- Name: COLUMN t_panel2aux_conf.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2aux_conf.reference_year_set IS 'Reference year set, foreign key to table t_reference_year_set.';


--
-- Name: t_panel2aux_conf_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_panel2aux_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_panel2aux_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_panel2aux_conf_id_seq OWNED BY @extschema@.t_panel2aux_conf.id;


--
-- Name: t_panel2total_1stph_est_data; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_panel2total_1stph_est_data (
    id integer NOT NULL,
    total_estimate_data integer NOT NULL,
    panel integer NOT NULL,
    reference_year_set integer NOT NULL
);


--
-- Name: TABLE t_panel2total_1stph_est_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_panel2total_1stph_est_data IS 'Table of mapping between panels and estimate data configuration (zero and the first phase).';


--
-- Name: COLUMN t_panel2total_1stph_est_data.total_estimate_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_1stph_est_data.total_estimate_data IS 'Identifier of the configuration of the 1st (dense grid) or the 0 (the most dense grid, or wall2wall data) etc. phase of the estimate, 
										foreign key to table t_total_estimate_data.';


--
-- Name: COLUMN t_panel2total_1stph_est_data.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_1stph_est_data.panel IS 'Sampling panel, foreign key to table t_panel.';


--
-- Name: COLUMN t_panel2total_1stph_est_data.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_1stph_est_data.reference_year_set IS 'Reference year set, foreign key to table t_reference_year_set.';


--
-- Name: t_panel2total_1stph_est_data_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_panel2total_1stph_est_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_panel2total_1stph_est_data_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_panel2total_1stph_est_data_id_seq OWNED BY @extschema@.t_panel2total_1stph_est_data.id;


--
-- Name: t_panel2total_2ndph_est_data; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_panel2total_2ndph_est_data (
    id integer NOT NULL,
    total_estimate_data integer NOT NULL,
    panel integer NOT NULL,
    reference_year_set integer NOT NULL
);


--
-- Name: TABLE t_panel2total_2ndph_est_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_panel2total_2ndph_est_data IS 'Table of mapping between panels and estimate data configuration (the last phase, field survey data).';


--
-- Name: COLUMN t_panel2total_2ndph_est_data.total_estimate_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_2ndph_est_data.total_estimate_data IS 'Identifier of the 2nd phase of the estimate - the most detailed field survey where the target variable is measured
										(usuall therminology of more phase estimates), foreign key to table t_total_estimate_data.';


--
-- Name: COLUMN t_panel2total_2ndph_est_data.panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_2ndph_est_data.panel IS 'Sampling panel, foreign key to table t_panel.';


--
-- Name: COLUMN t_panel2total_2ndph_est_data.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_panel2total_2ndph_est_data.reference_year_set IS 'Reference year set, foreign key to table t_reference_year_set.';


--
-- Name: t_panel2total_2ndph_est_data_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_panel2total_2ndph_est_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_panel2total_2ndph_est_data_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_panel2total_2ndph_est_data_id_seq OWNED BY @extschema@.t_panel2total_2ndph_est_data.id;


--
-- Name: t_panel_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_panel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_panel_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_panel_id_seq OWNED BY @extschema@.t_panel.id;


--
-- Name: t_plot_measurement_dates; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_plot_measurement_dates (
    id integer NOT NULL,
    plot integer NOT NULL,
    reference_year_set integer NOT NULL,
    measurement_date date
);


--
-- Name: TABLE t_plot_measurement_dates; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_plot_measurement_dates IS 'Table of measurement dates of plots.';


--
-- Name: COLUMN t_plot_measurement_dates.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_plot_measurement_dates.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN t_plot_measurement_dates.plot; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_plot_measurement_dates.plot IS 'Identifier of inventory panel, foreign key to table t_panel.';


--
-- Name: COLUMN t_plot_measurement_dates.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_plot_measurement_dates.reference_year_set IS 'Identifier of reference year set, foreign key to t_reference_year_set.';


--
-- Name: COLUMN t_plot_measurement_dates.measurement_date; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_plot_measurement_dates.measurement_date IS 'Measurement date on which the plot was measured.';


--
-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_plot_measurement_dates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_plot_measurement_dates_id_seq OWNED BY @extschema@.t_plot_measurement_dates.id;


--
-- Name: t_reference_year_set; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_reference_year_set (
    id integer NOT NULL,
    inventory_campaign integer NOT NULL,
    reference_year_set character varying(20) NOT NULL,
    reference_date_begin date NOT NULL,
    reference_date_end date NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_reference_year_set IS 'Table of reference year or season sets (generally speaking representative periods).';


--
-- Name: COLUMN t_reference_year_set.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.id IS 'Identifier of reference period set, primary key.';


--
-- Name: COLUMN t_reference_year_set.inventory_campaign; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.inventory_campaign IS 'Identifier of inventory campaign, foreign key to table t_inventory_campaign.';


--
-- Name: COLUMN t_reference_year_set.reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.reference_year_set IS 'Character identifier of reference period set.';


--
-- Name: COLUMN t_reference_year_set.reference_date_begin; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.reference_date_begin IS 'Date from which the period set begins.';


--
-- Name: COLUMN t_reference_year_set.reference_date_end; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.reference_date_end IS 'Date to which the period set ends.';


--
-- Name: COLUMN t_reference_year_set.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.label IS 'Label of reference year set.';


--
-- Name: COLUMN t_reference_year_set.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_reference_year_set.comment IS 'Optional commentary.';


--
-- Name: t_reference_year_set_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_reference_year_set_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_reference_year_set_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_reference_year_set_id_seq OWNED BY @extschema@.t_reference_year_set.id;


--
-- Name: t_result; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_result (
    id integer NOT NULL,
    estimate_conf integer,
    point double precision,
    var double precision,
    time_of_calc timestamp with time zone,
    extension_version text NOT NULL
);


--
-- Name: TABLE t_result; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_result IS 'Table with the final estimates.';


--
-- Name: COLUMN t_result.estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_result.estimate_conf IS 'Identifier of the estimate configuration, foreign key to table t_estimate_conf.';


--
-- Name: COLUMN t_result.time_of_calc; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_result.time_of_calc IS 'Time in which the calculation started.';


--
-- Name: COLUMN t_result.extension_version; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_result.extension_version IS 'Version of the extension with which the results were calculated.';


--
-- Name: t_result_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_result_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_result_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_result_id_seq OWNED BY @extschema@.t_result.id;


--
-- Name: t_strata_set; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_strata_set (
    id integer NOT NULL,
    country integer NOT NULL,
    strata_set character varying(20) NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_strata_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_strata_set IS 'Table of agregations of stratas - valid for each country, in most cases the stratum set consists of 1 sampling stratum.';


--
-- Name: COLUMN t_strata_set.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_strata_set.id IS 'Identifier of strata set, primary key.';


--
-- Name: COLUMN t_strata_set.country; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_strata_set.country IS 'Identifier of country, foreign key to table c_country.';


--
-- Name: COLUMN t_strata_set.strata_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_strata_set.strata_set IS 'Character identifier of strata set.';


--
-- Name: COLUMN t_strata_set.label; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_strata_set.label IS 'Label of strata set.';


--
-- Name: COLUMN t_strata_set.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_strata_set.comment IS 'Optional commentary.';


--
-- Name: t_strata_set_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_strata_set_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_strata_set_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_strata_set_id_seq OWNED BY @extschema@.t_strata_set.id;


--
-- Name: t_stratum; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_stratum (
    id integer NOT NULL,
    stratum character varying(20),
    area_m2 double precision,
    buffered_area_m2 double precision,
    comment text,
    geom public.geometry(MultiPolygon,3035),
    geom_buffered public.geometry(MultiPolygon,3035),
    strata_set integer,
    label character varying(120)
);


--
-- Name: TABLE t_stratum; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_stratum IS 'Table of stratum geometries and its attributes.';


--
-- Name: COLUMN t_stratum.id; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.id IS 'Primary key, identifier of the stratum.';


--
-- Name: COLUMN t_stratum.stratum; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.stratum IS 'Id if the stratum within the country.';


--
-- Name: COLUMN t_stratum.area_m2; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.area_m2 IS 'Area of the stratum, [m2].';


--
-- Name: COLUMN t_stratum.buffered_area_m2; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.buffered_area_m2 IS 'Area of the buffered stratum (in case it is buffered), [m2].';


--
-- Name: COLUMN t_stratum.comment; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.comment IS 'How is the design implemented in particular country (e.g. systematic with grid 2x2, random origin).';


--
-- Name: COLUMN t_stratum.geom; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.geom IS 'Polygon geometry of the stratum. Can be NULL if geometry is not known.';


--
-- Name: COLUMN t_stratum.geom_buffered; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.geom_buffered IS 'Polygon geometry of the buffered stratum. Can be NULL if geometry is not known.';


--
-- Name: COLUMN t_stratum.strata_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_stratum.strata_set IS 'Identifier of stratum set, foreign key to table t_strata_set.';


--
-- Name: t_stratum_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_stratum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_stratum_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_stratum_id_seq OWNED BY @extschema@.t_stratum.id;


--
-- Name: t_target_data; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_target_data (
    id integer NOT NULL,
    plot integer NOT NULL,
    target_variable integer,
    sub_population_category integer,
    value double precision NOT NULL,
    area_domain_category integer,
    reference_year_set integer
);


--
-- Name: TABLE t_target_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_target_data IS 'Table with the measured plot data.';


--
-- Name: t_target_data_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_target_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_target_data_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_target_data_id_seq OWNED BY @extschema@.t_target_data.id;


--
-- Name: t_total_estimate_conf; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_total_estimate_conf (
    id integer NOT NULL,
    estimation_cell integer NOT NULL,
    estimate_date_begin date NOT NULL,
    estimate_date_end date NOT NULL,
    total_estimate_conf character varying(120) NOT NULL
);


--
-- Name: TABLE t_total_estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_total_estimate_conf IS 'Table of total estimate configurations (only totals with defined estimation cell).';


--
-- Name: COLUMN t_total_estimate_conf.estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.estimation_cell IS 'Estimation cell, foreign key to table c_estimation_cell.';


--
-- Name: COLUMN t_total_estimate_conf.estimate_date_begin; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.estimate_date_begin IS 'The beginning of the time to witch the estimate is calculated.';


--
-- Name: COLUMN t_total_estimate_conf.estimate_date_end; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.estimate_date_end IS 'The end of the time to witch the estimate is calculated.';


--
-- Name: COLUMN t_total_estimate_conf.total_estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.total_estimate_conf IS 'Short description of the estimate.';


--
-- Name: t_total_estimate_conf_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_total_estimate_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_total_estimate_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_total_estimate_conf_id_seq OWNED BY @extschema@.t_total_estimate_conf.id;


--
-- Name: t_total_estimate_data; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_total_estimate_data (
    id integer NOT NULL,
    total_estimate_conf integer NOT NULL,
    variable integer NOT NULL,
    phase integer NOT NULL,
    aux_phase_type integer,
    cor_phase integer,
    aux_conf integer,
    CONSTRAINT check__t_total_estimate_data__aux_conf CHECK (
CASE
    WHEN (phase = 2) THEN (aux_conf IS NULL)
    ELSE (aux_conf IS NOT NULL)
END),
    CONSTRAINT check__t_total_estimate_data__aux_phase_type CHECK (
CASE
    WHEN (phase = 2) THEN (aux_phase_type IS NULL)
    ELSE (aux_phase_type IS NOT NULL)
END),
    CONSTRAINT check__t_total_estimate_data__cor_phase CHECK (
CASE
    WHEN (phase <= 1) THEN (cor_phase IS NULL)
    ELSE (cor_phase > phase)
END),
    CONSTRAINT check__t_total_estimate_data__phase CHECK (((phase >= 0) AND (phase <= 2)))
);


--
-- Name: TABLE t_total_estimate_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_total_estimate_data IS 'Table where most of the data used for estimate and each of the phases is specified (with complementary to table t_panel2total_estimate_data).';


--
-- Name: COLUMN t_total_estimate_data.total_estimate_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.total_estimate_conf IS 'Identifier of the configuration of the estimate of total. Foreign key to table t_total_estimate_conf.';


--
-- Name: COLUMN t_total_estimate_data.variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.variable IS 'Target variable of estimate, or explained variable in 2p, 3p. In 2p it must fit the target variable for the 2nd phase. Foreign key to table t_variable.';


--
-- Name: COLUMN t_total_estimate_data.phase; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.phase IS 'Order of the estimation phases (from 0 to 2).';


--
-- Name: COLUMN t_total_estimate_data.aux_phase_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.aux_phase_type IS 'Identifier of the phase type - type of the source (exhaustive/wall2wall or dense grid), foreign key to table c_phase_type.';


--
-- Name: COLUMN t_total_estimate_data.cor_phase; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.cor_phase IS 'For 3p, identifier of the phase to which it should be correlated.';


--
-- Name: COLUMN t_total_estimate_data.aux_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON COLUMN @extschema@.t_total_estimate_data.aux_conf IS 'Parametrization configuration for more phases estimates (parametrization area and model-auxiliary variables), foreign key to t_param_conf.';


--
-- Name: t_total_estimate_data_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_total_estimate_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_total_estimate_data_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_total_estimate_data_id_seq OWNED BY @extschema@.t_total_estimate_data.id;


--
-- Name: t_variable; Type: TABLE; Schema: @extschema@. Owner: -
--

CREATE TABLE @extschema@.t_variable (
    id integer NOT NULL,
    target_variable integer,
    sub_population_category integer,
    area_domain_category integer,
    auxiliary_variable_category integer
);


--
-- Name: TABLE t_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON TABLE @extschema@.t_variable IS 'Table with available combinations of variables (target/aux) and sub-populations/area domains';


--
-- Name: t_variable_id_seq; Type: SEQUENCE; Schema: @extschema@. Owner: -
--

CREATE SEQUENCE @extschema@.t_variable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_variable_id_seq; Type: SEQUENCE OWNED BY; Schema: @extschema@. Owner: -
--

ALTER SEQUENCE @extschema@.t_variable_id_seq OWNED BY @extschema@.t_variable.id;


--
-- Name: v_ldsity_conf; Type: MATERIALIZED VIEW; Schema: @extschema@. Owner: -
--

CREATE MATERIALIZED VIEW @extschema@.v_ldsity_conf AS
 WITH w_target AS (
         SELECT NULL::integer AS auxiliary_variable_category,
            t1.target_variable,
            t1.area_domain_category,
            t1.sub_population_category,
            NULL::character varying AS auxiliary_variable_category_label,
            t2.label AS target_variable_label,
            t3.label AS sub_population_category_label,
            t4.label AS area_domain_category_label,
            count(*) AS total
           FROM (((@extschema@.t_target_data t1
             JOIN @extschema@.c_target_variable t2 ON ((t1.target_variable = t2.id)))
             LEFT JOIN @extschema@.c_sub_population_category t3 ON ((t1.sub_population_category = t3.id)))
             LEFT JOIN @extschema@.c_area_domain_category t4 ON ((t1.area_domain_category = t4.id)))
          GROUP BY t1.target_variable, t1.sub_population_category, t1.area_domain_category, t2.label, t3.label, t4.label
          ORDER BY t1.target_variable, t1.sub_population_category, t1.area_domain_category
        ), w_aux AS (
         SELECT t1.auxiliary_variable_category,
            NULL::integer AS target_variable,
            NULL::integer AS area_domain_category,
            NULL::integer AS sub_population_category,
            t2.label AS auxiliary_variable_category_label,
            NULL::character varying AS target_variable_label,
            NULL::character varying AS sub_population_category_label,
            NULL::character varying AS area_domain_category_label,
            count(*) AS total
           FROM (@extschema@.t_auxiliary_data t1
             JOIN @extschema@.c_auxiliary_variable_category t2 ON ((t1.auxiliary_variable_category = t2.id)))
          GROUP BY t1.auxiliary_variable_category, t2.label
          ORDER BY t1.auxiliary_variable_category
        ), w_all AS (
         SELECT w_target.auxiliary_variable_category,
            w_target.target_variable,
            w_target.area_domain_category,
            w_target.sub_population_category,
            w_target.auxiliary_variable_category_label,
            w_target.target_variable_label,
            w_target.sub_population_category_label,
            w_target.area_domain_category_label,
            w_target.total
           FROM w_target
        UNION ALL
         SELECT w_aux.auxiliary_variable_category,
            w_aux.target_variable,
            w_aux.area_domain_category,
            w_aux.sub_population_category,
            w_aux.auxiliary_variable_category_label,
            w_aux.target_variable_label,
            w_aux.sub_population_category_label,
            w_aux.area_domain_category_label,
            w_aux.total
           FROM w_aux
        )
 SELECT row_number() OVER () AS id,
    w_all.auxiliary_variable_category,
    w_all.target_variable,
    w_all.area_domain_category,
    w_all.sub_population_category,
    w_all.auxiliary_variable_category_label,
    w_all.target_variable_label,
    w_all.sub_population_category_label,
    w_all.area_domain_category_label,
    w_all.total
   FROM w_all
  WITH NO DATA;


--
-- Name: MATERIALIZED VIEW v_ldsity_conf; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON MATERIALIZED VIEW @extschema@.v_ldsity_conf IS 'Available combinations of ldsity and attribute. View on table t_variable with human readable labels.';


--
-- Name: c_area_domain id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_area_domain ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_area_domain_id_seq'::regclass);


--
-- Name: c_area_domain_category id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_area_domain_category ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_area_domain_category_id_seq'::regclass);


--
-- Name: c_auxiliary_variable id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_auxiliary_variable ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_auxiliary_variable_id_seq'::regclass);


--
-- Name: c_auxiliary_variable_category id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_auxiliary_variable_category ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_auxiliary_variable_category_id_seq'::regclass);


--
-- Name: c_country id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_country ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_country_id_seq'::regclass);


--
-- Name: c_estimation_cell id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimation_cell ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_estimation_cell_id_seq'::regclass);


--
-- Name: c_estimation_cell_collection id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimation_cell_collection ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_estimation_cell_collection_id_seq'::regclass);


--
-- Name: c_param_area_type id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_param_area_type ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_param_area_type_id_seq'::regclass);


--
-- Name: c_state_or_change id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_state_or_change ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_state_or_change_id_seq'::regclass);


--
-- Name: c_sub_population id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_sub_population ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_sub_population_id_seq'::regclass);


--
-- Name: c_sub_population_category id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_sub_population_category ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_sub_population_category_id_seq'::regclass);


--
-- Name: c_target_variable id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_target_variable ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_target_variable_id_seq'::regclass);


--
-- Name: c_variable_type id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_variable_type ALTER COLUMN id SET DEFAULT nextval('@extschema@.c_variable_type_id_seq'::regclass);


--
-- Name: cm_cell2param_area_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cell2param_area_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_cell2param_area_mapping_id_seq'::regclass);


--
-- Name: cm_cluster2panel_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cluster2panel_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_cluster2panel_mapping_id_seq'::regclass);


--
-- Name: cm_plot2cell_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cell_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_plot2cell_mapping_id_seq'::regclass);


--
-- Name: cm_plot2cluster_config_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cluster_config_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_plot2cluster_config_mapping_id_seq'::regclass);


--
-- Name: cm_plot2param_area_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2param_area_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_plot2param_area_mapping_id_seq'::regclass);


--
-- Name: cm_refyearset2panel_mapping id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_refyearset2panel_mapping ALTER COLUMN id SET DEFAULT nextval('@extschema@.cm_refyearset2panel_mapping_id_seq'::regclass);


--
-- Name: f_a_cell gid; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_cell ALTER COLUMN gid SET DEFAULT nextval('@extschema@.f_a_cell_gid_seq'::regclass);


--
-- Name: f_a_param_area gid; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_param_area ALTER COLUMN gid SET DEFAULT nextval('@extschema@.f_a_param_area_gid_seq'::regclass);


--
-- Name: f_p_plot gid; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_p_plot ALTER COLUMN gid SET DEFAULT nextval('@extschema@.f_p_plot_gid_seq'::regclass);


--
-- Name: t_aux_conf id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_conf ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_aux_conf_id_seq'::regclass);


--
-- Name: t_aux_total id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_total ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_aux_total_id_seq'::regclass);


--
-- Name: t_auxiliary_data id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_auxiliary_data ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_auxiliary_data_id_seq'::regclass);


--
-- Name: t_cluster id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_cluster ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_cluster_id_seq'::regclass);


--
-- Name: t_cluster_configuration id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_cluster_configuration ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_cluster_configuration_id_seq'::regclass);


--
-- Name: t_estimate_conf id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_estimate_conf ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_estimate_conf_id_seq'::regclass);


--
-- Name: t_g_beta id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_g_beta ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_g_beta_id_seq'::regclass);


--
-- Name: t_inventory_campaign id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_inventory_campaign ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_inventory_campaign_id_seq'::regclass);


--
-- Name: t_model id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_model_id_seq'::regclass);


--
-- Name: t_model_variables id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model_variables ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_model_variables_id_seq'::regclass);


--
-- Name: t_panel id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_panel_id_seq'::regclass);


--
-- Name: t_panel2aux_conf id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2aux_conf ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_panel2aux_conf_id_seq'::regclass);


--
-- Name: t_panel2total_1stph_est_data id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_1stph_est_data ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_panel2total_1stph_est_data_id_seq'::regclass);


--
-- Name: t_panel2total_2ndph_est_data id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_2ndph_est_data ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_panel2total_2ndph_est_data_id_seq'::regclass);


--
-- Name: t_plot_measurement_dates id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_plot_measurement_dates ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_plot_measurement_dates_id_seq'::regclass);


--
-- Name: t_reference_year_set id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_reference_year_set ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_reference_year_set_id_seq'::regclass);


--
-- Name: t_result id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_result ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_result_id_seq'::regclass);


--
-- Name: t_strata_set id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_strata_set ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_strata_set_id_seq'::regclass);


--
-- Name: t_stratum id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_stratum ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_stratum_id_seq'::regclass);


--
-- Name: t_target_data id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_target_data_id_seq'::regclass);


--
-- Name: t_total_estimate_conf id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_conf ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_total_estimate_conf_id_seq'::regclass);


--
-- Name: t_total_estimate_data id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_total_estimate_data_id_seq'::regclass);


--
-- Name: t_variable id; Type: DEFAULT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable ALTER COLUMN id SET DEFAULT nextval('@extschema@.t_variable_id_seq'::regclass);


--
-- Name: f_p_plot f_p_plot__pid_cluster_unique; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_p_plot
    ADD CONSTRAINT f_p_plot__pid_cluster_unique UNIQUE (pid, plot, cluster);


--
-- Name: c_area_domain pkey__c_area_domain; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_area_domain
    ADD CONSTRAINT pkey__c_area_domain PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_area_domain ON c_area_domain; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_area_domain ON @extschema@.c_area_domain IS 'Primary key.';


--
-- Name: c_area_domain_category pkey__c_area_domain_category; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_area_domain_category
    ADD CONSTRAINT pkey__c_area_domain_category PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_area_domain_category ON c_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_area_domain_category ON @extschema@.c_area_domain_category IS 'Primary key.';


--
-- Name: c_aux_phase_type pkey__c_aux_phase_type; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_aux_phase_type
    ADD CONSTRAINT pkey__c_aux_phase_type PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_aux_phase_type ON c_aux_phase_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_aux_phase_type ON @extschema@.c_aux_phase_type IS 'Primary key.';


--
-- Name: c_auxiliary_variable pkey__c_auxiliary_variable; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_auxiliary_variable
    ADD CONSTRAINT pkey__c_auxiliary_variable PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_auxiliary_variable ON c_auxiliary_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_auxiliary_variable ON @extschema@.c_auxiliary_variable IS 'Primary key.';


--
-- Name: c_auxiliary_variable_category pkey__c_auxiliary_variable_category; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_auxiliary_variable_category
    ADD CONSTRAINT pkey__c_auxiliary_variable_category PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_auxiliary_variable_category ON c_auxiliary_variable_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_auxiliary_variable_category ON @extschema@.c_auxiliary_variable_category IS 'Primary key.';


--
-- Name: c_country pkey__c_country; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_country
    ADD CONSTRAINT pkey__c_country PRIMARY KEY (id);


--
-- Name: c_estimate_type pkey__c_estimate_type; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimate_type
    ADD CONSTRAINT pkey__c_estimate_type PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_estimate_type ON c_estimate_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_estimate_type ON @extschema@.c_estimate_type IS 'Primary key.';


--
-- Name: c_estimation_cell pkey__c_estimation_cell; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimation_cell
    ADD CONSTRAINT pkey__c_estimation_cell PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_estimation_cell ON c_estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_estimation_cell ON @extschema@.c_estimation_cell IS 'Primary key.';


--
-- Name: c_estimation_cell_collection pkey__c_estimation_cell_collection; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimation_cell_collection
    ADD CONSTRAINT pkey__c_estimation_cell_collection PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_estimation_cell_collection ON c_estimation_cell_collection; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_estimation_cell_collection ON @extschema@.c_estimation_cell_collection IS 'Primary key.';


--
-- Name: c_param_area_type pkey__c_param_area_type; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_param_area_type
    ADD CONSTRAINT pkey__c_param_area_type PRIMARY KEY (id);


--
-- Name: c_state_or_change pkey__c_state_or_change; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_state_or_change
    ADD CONSTRAINT pkey__c_state_or_change PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_state_or_change ON c_state_or_change; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_state_or_change ON @extschema@.c_state_or_change IS 'Primary key.';


--
-- Name: c_sub_population pkey__c_sub_population; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_sub_population
    ADD CONSTRAINT pkey__c_sub_population PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_sub_population ON c_sub_population; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_sub_population ON @extschema@.c_sub_population IS 'Primary key.';


--
-- Name: c_sub_population_category pkey__c_sub_population_category; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_sub_population_category
    ADD CONSTRAINT pkey__c_sub_population_category PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_sub_population_category ON c_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_sub_population_category ON @extschema@.c_sub_population_category IS 'Primary key.';


--
-- Name: c_target_variable pkey__c_target_variable; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_target_variable
    ADD CONSTRAINT pkey__c_target_variable PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_target_variable ON c_target_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_target_variable ON @extschema@.c_target_variable IS 'Primary key.';


--
-- Name: c_variable_type pkey__c_variable_type; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_variable_type
    ADD CONSTRAINT pkey__c_variable_type PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__c_variable_type ON c_variable_type; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__c_variable_type ON @extschema@.c_variable_type IS 'Primary key.';


--
-- Name: cm_cell2param_area_mapping pkey__cm_cell2param_area_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cell2param_area_mapping
    ADD CONSTRAINT pkey__cm_cell2param_area_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_cell2param_area_mapping ON cm_cell2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_cell2param_area_mapping ON @extschema@.cm_cell2param_area_mapping IS 'Primary key.';


--
-- Name: cm_cluster2panel_mapping pkey__cm_cluster2panel_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cluster2panel_mapping
    ADD CONSTRAINT pkey__cm_cluster2panel_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_cluster2panel_mapping ON cm_cluster2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_cluster2panel_mapping ON @extschema@.cm_cluster2panel_mapping IS 'Primary key.';


--
-- Name: cm_plot2cell_mapping pkey__cm_plot2cell_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cell_mapping
    ADD CONSTRAINT pkey__cm_plot2cell_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_plot2cell_mapping ON cm_plot2cell_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_plot2cell_mapping ON @extschema@.cm_plot2cell_mapping IS 'Primary key.';


--
-- Name: cm_plot2cluster_config_mapping pkey__cm_plot2cluster_config_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cluster_config_mapping
    ADD CONSTRAINT pkey__cm_plot2cluster_config_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_plot2cluster_config_mapping ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_plot2cluster_config_mapping ON @extschema@.cm_plot2cluster_config_mapping IS 'Primary key.';


--
-- Name: cm_plot2param_area_mapping pkey__cm_plot2param_area_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2param_area_mapping
    ADD CONSTRAINT pkey__cm_plot2param_area_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_plot2param_area_mapping ON cm_plot2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_plot2param_area_mapping ON @extschema@.cm_plot2param_area_mapping IS 'Primary key.';


--
-- Name: cm_refyearset2panel_mapping pkey__cm_refyearset2panel_mapping; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_refyearset2panel_mapping
    ADD CONSTRAINT pkey__cm_refyearset2panel_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_refyearset2panel_mapping ON @extschema@.cm_refyearset2panel_mapping IS 'Primary key.';


--
-- Name: t_total_estimate_data pkey__cm_target; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data
    ADD CONSTRAINT pkey__cm_target PRIMARY KEY (id);


--
-- Name: f_a_cell pkey__f_a_cell; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_cell
    ADD CONSTRAINT pkey__f_a_cell PRIMARY KEY (gid);


--
-- Name: CONSTRAINT pkey__f_a_cell ON f_a_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__f_a_cell ON @extschema@.f_a_cell IS 'Primary key.';


--
-- Name: f_a_param_area pkey__f_a_param_area; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_param_area
    ADD CONSTRAINT pkey__f_a_param_area PRIMARY KEY (gid);


--
-- Name: f_p_plot pkey__f_p_plots; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_p_plot
    ADD CONSTRAINT pkey__f_p_plots PRIMARY KEY (gid);


--
-- Name: t_aux_conf pkey__t_aux_conf; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_conf
    ADD CONSTRAINT pkey__t_aux_conf PRIMARY KEY (id);


--
-- Name: t_aux_total pkey__t_aux_total; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_total
    ADD CONSTRAINT pkey__t_aux_total PRIMARY KEY (id);


--
-- Name: t_auxiliary_data pkey__t_auxiliary_data; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_auxiliary_data
    ADD CONSTRAINT pkey__t_auxiliary_data PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_auxiliary_data ON t_auxiliary_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_auxiliary_data ON @extschema@.t_auxiliary_data IS 'Primary key.';


--
-- Name: t_cluster pkey__t_cluster; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_cluster
    ADD CONSTRAINT pkey__t_cluster PRIMARY KEY (id);


--
-- Name: t_cluster_configuration pkey__t_cluster_configuration; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_cluster_configuration
    ADD CONSTRAINT pkey__t_cluster_configuration PRIMARY KEY (id);


--
-- Name: t_estimate_conf pkey__t_estimate_conf; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_estimate_conf
    ADD CONSTRAINT pkey__t_estimate_conf PRIMARY KEY (id);


--
-- Name: t_g_beta pkey__t_g_beta; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_g_beta
    ADD CONSTRAINT pkey__t_g_beta PRIMARY KEY (id);


--
-- Name: t_inventory_campaign pkey__t_inventory_campaign; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_inventory_campaign
    ADD CONSTRAINT pkey__t_inventory_campaign PRIMARY KEY (id);


--
-- Name: t_model pkey__t_model; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model
    ADD CONSTRAINT pkey__t_model PRIMARY KEY (id);


--
-- Name: t_model_variables pkey__t_model_variables; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model_variables
    ADD CONSTRAINT pkey__t_model_variables PRIMARY KEY (id);


--
-- Name: t_panel pkey__t_panel; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel
    ADD CONSTRAINT pkey__t_panel PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_panel ON t_panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_panel ON @extschema@.t_panel IS 'Primary key.';


--
-- Name: t_panel2aux_conf pkey__t_panel2aux_conf; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2aux_conf
    ADD CONSTRAINT pkey__t_panel2aux_conf PRIMARY KEY (id);


--
-- Name: t_panel2total_1stph_est_data pkey__t_panel2total_1stph_est_data; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_1stph_est_data
    ADD CONSTRAINT pkey__t_panel2total_1stph_est_data PRIMARY KEY (id);


--
-- Name: t_panel2total_2ndph_est_data pkey__t_panel2total_2ndph_est_data; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_2ndph_est_data
    ADD CONSTRAINT pkey__t_panel2total_2ndph_est_data PRIMARY KEY (id);


--
-- Name: t_plot_measurement_dates pkey__t_plot_measurement_dates; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_plot_measurement_dates
    ADD CONSTRAINT pkey__t_plot_measurement_dates PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_plot_measurement_dates ON t_plot_measurement_dates; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_plot_measurement_dates ON @extschema@.t_plot_measurement_dates IS 'Primary key.';


--
-- Name: t_reference_year_set pkey__t_reference_year_set; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_reference_year_set
    ADD CONSTRAINT pkey__t_reference_year_set PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_reference_year_set ON t_reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_reference_year_set ON @extschema@.t_reference_year_set IS 'Primary key.';


--
-- Name: t_result pkey__t_result; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_result
    ADD CONSTRAINT pkey__t_result PRIMARY KEY (id);


--
-- Name: t_strata_set pkey__t_strata_set; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_strata_set
    ADD CONSTRAINT pkey__t_strata_set PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_strata_set ON t_strata_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_strata_set ON @extschema@.t_strata_set IS 'Primary key.';


--
-- Name: t_stratum pkey__t_stratum; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_stratum
    ADD CONSTRAINT pkey__t_stratum PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_stratum ON t_stratum; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_stratum ON @extschema@.t_stratum IS 'Primary key.';


--
-- Name: t_target_data pkey__t_target_data; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT pkey__t_target_data PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_target_data ON t_target_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_target_data ON @extschema@.t_target_data IS 'Primary key.';


--
-- Name: t_total_estimate_conf pkey__t_total_estimate_conf; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_conf
    ADD CONSTRAINT pkey__t_total_estimate_conf PRIMARY KEY (id);


--
-- Name: t_variable pkey__t_variable; Type: CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable
    ADD CONSTRAINT pkey__t_variable PRIMARY KEY (id);


--
-- Name: f_a_cell_geom_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX f_a_cell_geom_idx ON @extschema@.f_a_cell USING gist (geom);


--
-- Name: f_a_param_area_geom_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX f_a_param_area_geom_idx ON @extschema@.f_a_param_area USING gist (geom);


--
-- Name: f_a_sampling_stratum_geom_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX f_a_sampling_stratum_geom_idx ON @extschema@.t_stratum USING gist (geom);


--
-- Name: f_p_plot_geom_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX f_p_plot_geom_idx ON @extschema@.f_p_plot USING gist (geom);


--
-- Name: idx__f_p_plot__gid; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX idx__f_p_plot__gid ON @extschema@.f_p_plot USING btree (gid);


--
-- Name: idx__f_p_plot__gid__cluster; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX idx__f_p_plot__gid__cluster ON @extschema@.f_p_plot USING btree (gid, cluster);


--
-- Name: idx__t_cluster__id; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX idx__t_cluster__id ON @extschema@.t_cluster USING btree (id);


--
-- Name: idx__t_g_beta__aux_conf; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX idx__t_g_beta__aux_conf ON @extschema@.t_g_beta USING btree (aux_conf);


--
-- Name: t_auxiliary_data_plot_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_auxiliary_data_plot_idx ON @extschema@.t_auxiliary_data USING btree (plot);


--
-- Name: t_cluster__cid__idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_cluster__cid__idx ON @extschema@.t_cluster USING btree (cluster);


--
-- Name: t_map_plot_cell__cell_gid__idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_map_plot_cell__cell_gid__idx ON @extschema@.cm_plot2cell_mapping USING btree (estimation_cell);


--
-- Name: t_map_plot_cell__plot_gid__idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_map_plot_cell__plot_gid__idx ON @extschema@.cm_plot2cell_mapping USING btree (plot);


--
-- Name: t_map_plot_param_area__param_area_gid__idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_map_plot_param_area__param_area_gid__idx ON @extschema@.cm_plot2param_area_mapping USING btree (param_area);


--
-- Name: t_map_plot_param_area__plot_gid__idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_map_plot_param_area__plot_gid__idx ON @extschema@.cm_plot2param_area_mapping USING btree (plot);


--
-- Name: t_target_data_plot_idx; Type: INDEX; Schema: @extschema@. Owner: -
--

CREATE INDEX t_target_data_plot_idx ON @extschema@.t_target_data USING btree (plot);


--
-- Name: c_area_domain_category fkey__c_area_domain_category__c_area_domain; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_area_domain_category
    ADD CONSTRAINT fkey__c_area_domain_category__c_area_domain FOREIGN KEY (domain) REFERENCES @extschema@.c_area_domain(id);


--
-- Name: CONSTRAINT fkey__c_area_domain_category__c_area_domain ON c_area_domain_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_area_domain_category__c_area_domain ON @extschema@.c_area_domain_category IS 'Foreign key to table @extschema@.c_area_domain.';


--
-- Name: c_auxiliary_variable_category fkey__c_auxiliary_variable_category__c_auxiliary_variable; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_auxiliary_variable_category
    ADD CONSTRAINT fkey__c_auxiliary_variable_category__c_auxiliary_variable FOREIGN KEY (auxiliary_variable) REFERENCES @extschema@.c_auxiliary_variable(id);


--
-- Name: CONSTRAINT fkey__c_auxiliary_variable_category__c_auxiliary_variable ON c_auxiliary_variable_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_auxiliary_variable_category__c_auxiliary_variable ON @extschema@.c_auxiliary_variable_category IS 'Foreign key to table @extschema@.c_auxiliary_variable.';


--
-- Name: c_estimation_cell fkey__c_estimation_cell__c_estimation_cell_collection; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_estimation_cell
    ADD CONSTRAINT fkey__c_estimation_cell__c_estimation_cell_collection FOREIGN KEY (estimation_cell_collection) REFERENCES @extschema@.c_estimation_cell_collection(id);


--
-- Name: CONSTRAINT fkey__c_estimation_cell__c_estimation_cell_collection ON c_estimation_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_estimation_cell__c_estimation_cell_collection ON @extschema@.c_estimation_cell IS 'Foreign key to table @extschema@.c_estimation_cell_collection.';


--
-- Name: c_sub_population_category fkey__c_sub_population_category__c_sub_population; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_sub_population_category
    ADD CONSTRAINT fkey__c_sub_population_category__c_sub_population FOREIGN KEY (sub_population) REFERENCES @extschema@.c_sub_population(id);


--
-- Name: CONSTRAINT fkey__c_sub_population_category__c_sub_population ON c_sub_population_category; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_sub_population_category__c_sub_population ON @extschema@.c_sub_population_category IS 'Foreign key to table @extschema@.c_sub_population.';


--
-- Name: c_target_variable fkey__c_target_variable__c_state_or_change; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_target_variable
    ADD CONSTRAINT fkey__c_target_variable__c_state_or_change FOREIGN KEY (state_or_change) REFERENCES @extschema@.c_state_or_change(id);


--
-- Name: CONSTRAINT fkey__c_target_variable__c_state_or_change ON c_target_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_target_variable__c_state_or_change ON @extschema@.c_target_variable IS 'Foreign key to table @extschema@.c_state_or_change.';


--
-- Name: c_target_variable fkey__c_target_variable__c_variable_type; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.c_target_variable
    ADD CONSTRAINT fkey__c_target_variable__c_variable_type FOREIGN KEY (variable_type) REFERENCES @extschema@.c_variable_type(id);


--
-- Name: CONSTRAINT fkey__c_target_variable__c_variable_type ON c_target_variable; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__c_target_variable__c_variable_type ON @extschema@.c_target_variable IS 'Foreign key to table @extschema@.c_variable_type.';


--
-- Name: t_model_variables fkey__cm_auxiliary__t_model; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model_variables
    ADD CONSTRAINT fkey__cm_auxiliary__t_model FOREIGN KEY (model) REFERENCES @extschema@.t_model(id);


--
-- Name: cm_cell2param_area_mapping fkey__cm_cell2param_area_mapping__c_estimation_cell; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cell2param_area_mapping
    ADD CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id);


--
-- Name: CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell ON cm_cell2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cell2param_area_mapping__c_estimation_cell ON @extschema@.cm_cell2param_area_mapping IS 'Foreign key to table @extschema@.c_estimation_cell.';


--
-- Name: cm_cell2param_area_mapping fkey__cm_cell2param_area_mapping__f_a_param_area; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cell2param_area_mapping
    ADD CONSTRAINT fkey__cm_cell2param_area_mapping__f_a_param_area FOREIGN KEY (param_area) REFERENCES @extschema@.f_a_param_area(gid);


--
-- Name: CONSTRAINT fkey__cm_cell2param_area_mapping__f_a_param_area ON cm_cell2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cell2param_area_mapping__f_a_param_area ON @extschema@.cm_cell2param_area_mapping IS 'Foreign key to table @extschema@.f_a_param_area.';


--
-- Name: cm_cluster2panel_mapping fkey__cm_cluster2panel_mapping__t_cluster; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cluster2panel_mapping
    ADD CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster FOREIGN KEY (cluster) REFERENCES @extschema@.t_cluster(id);


--
-- Name: CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster ON cm_cluster2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster ON @extschema@.cm_cluster2panel_mapping IS 'Foreign key to table @extschema@.t_cluster.';


--
-- Name: cm_cluster2panel_mapping fkey__cm_cluster2panel_mapping__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_cluster2panel_mapping
    ADD CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel FOREIGN KEY (panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel ON cm_cluster2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel ON @extschema@.cm_cluster2panel_mapping IS 'Foreign key to table @extschema@.t_panel.';


--
-- Name: cm_plot2cell_mapping fkey__cm_plot2cell_mapping__c_estimation_cell; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cell_mapping
    ADD CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id);


--
-- Name: CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell ON cm_plot2cell_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cell_mapping__c_estimation_cell ON @extschema@.cm_plot2cell_mapping IS 'Foreign key to table @extschema@.c_estimation_cell.';


--
-- Name: cm_plot2cell_mapping fkey__cm_plot2cell_mapping__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cell_mapping
    ADD CONSTRAINT fkey__cm_plot2cell_mapping__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__cm_plot2cell_mapping__f_p_plot ON cm_plot2cell_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cell_mapping__f_p_plot ON @extschema@.cm_plot2cell_mapping IS 'Foreign key to table @extschema@.f_p_plot.';


--
-- Name: cm_plot2cluster_config_mapping fkey__cm_plot2cluster_config_mapping__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cluster_config_mapping
    ADD CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot ON @extschema@.cm_plot2cluster_config_mapping IS 'Foreign key to table @extschema@.f_p_plot.';


--
-- Name: cm_plot2cluster_config_mapping fkey__cm_plot2cluster_config_mapping__t_cluster_configuration; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2cluster_config_mapping
    ADD CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration FOREIGN KEY (cluster_configuration) REFERENCES @extschema@.t_cluster_configuration(id);


--
-- Name: CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration ON @extschema@.cm_plot2cluster_config_mapping IS 'Foreign key to table @extschema@.t_cluster_configuration.';


--
-- Name: cm_plot2param_area_mapping fkey__cm_plot2param_area_mapping__f_a_param_area; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2param_area_mapping
    ADD CONSTRAINT fkey__cm_plot2param_area_mapping__f_a_param_area FOREIGN KEY (param_area) REFERENCES @extschema@.f_a_param_area(gid);


--
-- Name: CONSTRAINT fkey__cm_plot2param_area_mapping__f_a_param_area ON cm_plot2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2param_area_mapping__f_a_param_area ON @extschema@.cm_plot2param_area_mapping IS 'Foreign key to table @extschema@.f_a_param_area.';


--
-- Name: cm_plot2param_area_mapping fkey__cm_plot2param_area_mapping__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_plot2param_area_mapping
    ADD CONSTRAINT fkey__cm_plot2param_area_mapping__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__cm_plot2param_area_mapping__f_p_plot ON cm_plot2param_area_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2param_area_mapping__f_p_plot ON @extschema@.cm_plot2param_area_mapping IS 'Foreign key to table @extschema@.f_p_plot.';


--
-- Name: cm_refyearset2panel_mapping fkey__cm_refyearset2panel_mapping__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_refyearset2panel_mapping
    ADD CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel FOREIGN KEY (panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel ON @extschema@.cm_refyearset2panel_mapping IS 'Foreign key to table @extschema@.t_panel.';


--
-- Name: cm_refyearset2panel_mapping fkey__cm_refyearset2panel_mapping__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.cm_refyearset2panel_mapping
    ADD CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set ON @extschema@.cm_refyearset2panel_mapping IS 'Foreign key to table @extschema@.t_reference_year_set.';


--
-- Name: f_a_cell fkey__f_a_cell__c_estimation_cell; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_cell
    ADD CONSTRAINT fkey__f_a_cell__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id) ON UPDATE CASCADE;


--
-- Name: CONSTRAINT fkey__f_a_cell__c_estimation_cell ON f_a_cell; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__f_a_cell__c_estimation_cell ON @extschema@.f_a_cell IS 'Foreign key to table @extschema@.c_estimation_cell.';


--
-- Name: f_a_param_area fkey__f_a_param_area__c_param_area_type; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_a_param_area
    ADD CONSTRAINT fkey__f_a_param_area__c_param_area_type FOREIGN KEY (param_area_type) REFERENCES @extschema@.c_param_area_type(id) ON UPDATE CASCADE;


--
-- Name: f_p_plot fkey__f_p_plot__t_cluster; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.f_p_plot
    ADD CONSTRAINT fkey__f_p_plot__t_cluster FOREIGN KEY (cluster) REFERENCES @extschema@.t_cluster(id);


--
-- Name: t_aux_total fkey__t_aux_total__c_auxiliary_variable_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_total
    ADD CONSTRAINT fkey__t_aux_total__c_auxiliary_variable_category FOREIGN KEY (auxiliary_variable_category) REFERENCES @extschema@.c_auxiliary_variable_category(id);


--
-- Name: t_aux_total fkey__t_aux_total__c_estimation_cell; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_total
    ADD CONSTRAINT fkey__t_aux_total__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id);


--
-- Name: t_auxiliary_data fkey__t_auxiliary_data__c_auxiliary_variable_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_auxiliary_data
    ADD CONSTRAINT fkey__t_auxiliary_data__c_auxiliary_variable_category FOREIGN KEY (auxiliary_variable_category) REFERENCES @extschema@.c_auxiliary_variable_category(id);


--
-- Name: CONSTRAINT fkey__t_auxiliary_data__c_auxiliary_variable_category ON t_auxiliary_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_auxiliary_data__c_auxiliary_variable_category ON @extschema@.t_auxiliary_data IS 'Foreign key to table @extschema@.c_auxiliary_variable_category.';


--
-- Name: t_auxiliary_data fkey__t_auxiliary_data__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_auxiliary_data
    ADD CONSTRAINT fkey__t_auxiliary_data__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__t_auxiliary_data__f_p_plot ON t_auxiliary_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_auxiliary_data__f_p_plot ON @extschema@.t_auxiliary_data IS 'Foreign key to table @extschema@.f_p_plot.';


--
-- Name: t_estimate_conf fkey__t_estimate_conf__c_estimate_type; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_estimate_conf
    ADD CONSTRAINT fkey__t_estimate_conf__c_estimate_type FOREIGN KEY (estimate_type) REFERENCES @extschema@.c_estimate_type(id);


--
-- Name: t_aux_conf fkey__t_estimate_conf__f_a_param_area; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_conf
    ADD CONSTRAINT fkey__t_estimate_conf__f_a_param_area FOREIGN KEY (param_area) REFERENCES @extschema@.f_a_param_area(gid);


--
-- Name: t_aux_conf fkey__t_estimate_conf__t_model; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_aux_conf
    ADD CONSTRAINT fkey__t_estimate_conf__t_model FOREIGN KEY (model) REFERENCES @extschema@.t_model(id);


--
-- Name: t_estimate_conf fkey__t_estimate_conf__t_total_estimate_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_estimate_conf
    ADD CONSTRAINT fkey__t_estimate_conf__t_total_estimate_conf FOREIGN KEY (total_estimate_conf) REFERENCES @extschema@.t_total_estimate_conf(id);


--
-- Name: t_estimate_conf fkey__t_estimate_conf__t_total_estimate_conf2; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_estimate_conf
    ADD CONSTRAINT fkey__t_estimate_conf__t_total_estimate_conf2 FOREIGN KEY (denominator) REFERENCES @extschema@.t_total_estimate_conf(id);


--
-- Name: t_g_beta fkey__t_g_beta__t_aux_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_g_beta
    ADD CONSTRAINT fkey__t_g_beta__t_aux_conf FOREIGN KEY (aux_conf) REFERENCES @extschema@.t_aux_conf(id);


--
-- Name: t_model_variables fkey__t_model_variables__t_variable; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_model_variables
    ADD CONSTRAINT fkey__t_model_variables__t_variable FOREIGN KEY (variable) REFERENCES @extschema@.t_variable(id);


--
-- Name: t_panel2aux_conf fkey__t_panel2aux_conf__t_aux_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2aux_conf
    ADD CONSTRAINT fkey__t_panel2aux_conf__t_aux_conf FOREIGN KEY (aux_conf) REFERENCES @extschema@.t_aux_conf(id);


--
-- Name: t_panel2aux_conf fkey__t_panel2aux_conf__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2aux_conf
    ADD CONSTRAINT fkey__t_panel2aux_conf__t_panel FOREIGN KEY (panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: t_panel2aux_conf fkey__t_panel2aux_conf__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2aux_conf
    ADD CONSTRAINT fkey__t_panel2aux_conf__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: t_panel2total_1stph_est_data fkey__t_panel2total_1stph_est_data__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_1stph_est_data
    ADD CONSTRAINT fkey__t_panel2total_1stph_est_data__t_panel FOREIGN KEY (panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: t_panel2total_1stph_est_data fkey__t_panel2total_1stph_est_data__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_1stph_est_data
    ADD CONSTRAINT fkey__t_panel2total_1stph_est_data__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: t_panel2total_1stph_est_data fkey__t_panel2total_1stph_est_data__t_total_estimate_data; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_1stph_est_data
    ADD CONSTRAINT fkey__t_panel2total_1stph_est_data__t_total_estimate_data FOREIGN KEY (total_estimate_data) REFERENCES @extschema@.t_total_estimate_data(id);


--
-- Name: t_panel2total_2ndph_est_data fkey__t_panel2total_2ndph_est_data__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_2ndph_est_data
    ADD CONSTRAINT fkey__t_panel2total_2ndph_est_data__t_panel FOREIGN KEY (panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: t_panel2total_2ndph_est_data fkey__t_panel2total_2ndph_est_data__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_2ndph_est_data
    ADD CONSTRAINT fkey__t_panel2total_2ndph_est_data__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: t_panel2total_2ndph_est_data fkey__t_panel2total_2ndph_est_data__t_total_estimate_data; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel2total_2ndph_est_data
    ADD CONSTRAINT fkey__t_panel2total_2ndph_est_data__t_total_estimate_data FOREIGN KEY (total_estimate_data) REFERENCES @extschema@.t_total_estimate_data(id);


--
-- Name: t_panel fkey__t_panel__t_cluster_configuration; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel
    ADD CONSTRAINT fkey__t_panel__t_cluster_configuration FOREIGN KEY (cluster_configuration) REFERENCES @extschema@.t_cluster_configuration(id);


--
-- Name: CONSTRAINT fkey__t_panel__t_cluster_configuration ON t_panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_panel__t_cluster_configuration ON @extschema@.t_panel IS 'Foreign key to table @extschema@.t_cluster_configuration.';


--
-- Name: t_panel fkey__t_panel__t_panel; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel
    ADD CONSTRAINT fkey__t_panel__t_panel FOREIGN KEY (parent_panel) REFERENCES @extschema@.t_panel(id);


--
-- Name: t_panel fkey__t_panel__t_stratum; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_panel
    ADD CONSTRAINT fkey__t_panel__t_stratum FOREIGN KEY (stratum) REFERENCES @extschema@.t_stratum(id);


--
-- Name: CONSTRAINT fkey__t_panel__t_stratum ON t_panel; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_panel__t_stratum ON @extschema@.t_panel IS 'Foreign key to table @extschema@.t_panel.';


--
-- Name: t_plot_measurement_dates fkey__t_plot_measurement_dates__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_plot_measurement_dates
    ADD CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot ON t_plot_measurement_dates; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot ON @extschema@.t_plot_measurement_dates IS 'Foreign key to table @extschema@.f_p_plot.';


--
-- Name: t_plot_measurement_dates fkey__t_plot_measurement_dates__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_plot_measurement_dates
    ADD CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set ON t_plot_measurement_dates; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set ON @extschema@.t_plot_measurement_dates IS 'Foreign key to table @extschema@.t_reference_year_set.';


--
-- Name: t_reference_year_set fkey__t_reference_year_set__t_inventory_campaign; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_reference_year_set
    ADD CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign FOREIGN KEY (inventory_campaign) REFERENCES @extschema@.t_inventory_campaign(id);


--
-- Name: CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign ON t_reference_year_set; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign ON @extschema@.t_reference_year_set IS 'Foreign key to table @extschema@.t_inventory_campaign.';


--
-- Name: t_result fkey__t_result__t_estimate_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_result
    ADD CONSTRAINT fkey__t_result__t_estimate_conf FOREIGN KEY (estimate_conf) REFERENCES @extschema@.t_estimate_conf(id);


--
-- Name: t_strata_set fkey__t_strata_set__c_country; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_strata_set
    ADD CONSTRAINT fkey__t_strata_set__c_country FOREIGN KEY (country) REFERENCES @extschema@.c_country(id);


--
-- Name: t_stratum fkey__t_stratum__t_strata_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_stratum
    ADD CONSTRAINT fkey__t_stratum__t_strata_set FOREIGN KEY (strata_set) REFERENCES @extschema@.t_strata_set(id);


--
-- Name: CONSTRAINT fkey__t_stratum__t_strata_set ON t_stratum; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_stratum__t_strata_set ON @extschema@.t_stratum IS 'Foreign key to table @extschema@.t_strata_set.';


--
-- Name: t_target_data fkey__t_target_data__c_area_domain_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT fkey__t_target_data__c_area_domain_category FOREIGN KEY (area_domain_category) REFERENCES @extschema@.c_area_domain_category(id);


--
-- Name: CONSTRAINT fkey__t_target_data__c_area_domain_category ON t_target_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_target_data__c_area_domain_category ON @extschema@.t_target_data IS 'Foreign key to table @extschema@.c_area_domain_category.';


--
-- Name: t_target_data fkey__t_target_data__c_sub_population_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT fkey__t_target_data__c_sub_population_category FOREIGN KEY (sub_population_category) REFERENCES @extschema@.c_sub_population_category(id);


--
-- Name: CONSTRAINT fkey__t_target_data__c_sub_population_category ON t_target_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_target_data__c_sub_population_category ON @extschema@.t_target_data IS 'Foreign key to table @extschema@.c_sub_population_category.';


--
-- Name: t_target_data fkey__t_target_data__c_target_variable; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT fkey__t_target_data__c_target_variable FOREIGN KEY (target_variable) REFERENCES @extschema@.c_target_variable(id);


--
-- Name: CONSTRAINT fkey__t_target_data__c_target_variable ON t_target_data; Type: COMMENT; Schema: @extschema@. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_target_data__c_target_variable ON @extschema@.t_target_data IS 'Foreign key to table @extschema@.c_target_variable.';


--
-- Name: t_target_data fkey__t_target_data__f_p_plot; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT fkey__t_target_data__f_p_plot FOREIGN KEY (plot) REFERENCES @extschema@.f_p_plot(gid) ON UPDATE CASCADE;


--
-- Name: t_target_data fkey__t_target_data__t_reference_year_set; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_target_data
    ADD CONSTRAINT fkey__t_target_data__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES @extschema@.t_reference_year_set(id);


--
-- Name: t_total_estimate_conf fkey__t_total_estimate_conf__c_estimation_cell; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_conf
    ADD CONSTRAINT fkey__t_total_estimate_conf__c_estimation_cell FOREIGN KEY (estimation_cell) REFERENCES @extschema@.c_estimation_cell(id);


--
-- Name: t_total_estimate_data fkey__t_total_estimate_data__c_aux_phase_type; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data
    ADD CONSTRAINT fkey__t_total_estimate_data__c_aux_phase_type FOREIGN KEY (aux_phase_type) REFERENCES @extschema@.c_aux_phase_type(id);


--
-- Name: t_total_estimate_data fkey__t_total_estimate_data__t_aux_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data
    ADD CONSTRAINT fkey__t_total_estimate_data__t_aux_conf FOREIGN KEY (aux_conf) REFERENCES @extschema@.t_aux_conf(id);


--
-- Name: t_total_estimate_data fkey__t_total_estimate_data__t_total_estimate_conf; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data
    ADD CONSTRAINT fkey__t_total_estimate_data__t_total_estimate_conf FOREIGN KEY (total_estimate_conf) REFERENCES @extschema@.t_total_estimate_conf(id);


--
-- Name: t_total_estimate_data fkey__t_total_estimate_data__t_variable; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_total_estimate_data
    ADD CONSTRAINT fkey__t_total_estimate_data__t_variable FOREIGN KEY (variable) REFERENCES @extschema@.t_variable(id);


--
-- Name: t_variable fkey__t_variable__c_area_domain_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable
    ADD CONSTRAINT fkey__t_variable__c_area_domain_category FOREIGN KEY (area_domain_category) REFERENCES @extschema@.c_area_domain_category(id);


--
-- Name: t_variable fkey__t_variable__c_auxiliary_variable_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable
    ADD CONSTRAINT fkey__t_variable__c_auxiliary_variable_category FOREIGN KEY (auxiliary_variable_category) REFERENCES @extschema@.c_auxiliary_variable_category(id);


--
-- Name: t_variable fkey__t_variable__c_sub_population_category; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable
    ADD CONSTRAINT fkey__t_variable__c_sub_population_category FOREIGN KEY (sub_population_category) REFERENCES @extschema@.c_sub_population_category(id);


--
-- Name: t_variable fkey__t_variable__c_target_variable; Type: FK CONSTRAINT; Schema: @extschema@. Owner: -
--

ALTER TABLE ONLY @extschema@.t_variable
    ADD CONSTRAINT fkey__t_variable__c_target_variable FOREIGN KEY (target_variable) REFERENCES @extschema@.c_target_variable(id);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: c_aux_phase_type; Type: TABLE DATA; Schema: @extschema@. Owner: -
--

INSERT INTO @extschema@.c_aux_phase_type (id, label, description) VALUES (1, 'wall2wall', 'Wall2wall, exhaustive data covering continuously the area.');
INSERT INTO @extschema@.c_aux_phase_type (id, label, description) VALUES (2, 'dense grid', 'More dense grid which is in a hierarchical relationship with the next phase sparse grid (sparse grid is a subset of the dense one).');


--
-- Data for Name: c_estimate_type; Type: TABLE DATA; Schema: @extschema@. Owner: -
--

INSERT INTO @extschema@.c_estimate_type (id, label, description) VALUES (1, 'total', 'Estimate of total.');
INSERT INTO @extschema@.c_estimate_type (id, label, description) VALUES (2, 'ratio', 'Estimate of ratio (basically composed from two totals).');


--
-- Name: c_aux_phase_type_id_seq; Type: SEQUENCE SET; Schema: @extschema@. Owner: -
--

SELECT pg_catalog.setval('@extschema@.c_aux_phase_type_id_seq', 1, false);


--
-- Name: c_estimate_type_id_seq; Type: SEQUENCE SET; Schema: @extschema@. Owner: -
--

SELECT pg_catalog.setval('@extschema@.c_estimate_type_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--


SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_area_domain_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_auxiliary_variable_category_id_seq', '');
--SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_aux_phase_type', '');
--SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_aux_phase_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_country', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_country_id_seq', '');
--SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimate_type', '');
--SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimate_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_collection', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_estimation_cell_collection_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cell2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cell2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cluster2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_cluster2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cell_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cell_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cluster_config_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2cluster_config_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2param_area_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_plot2param_area_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_refyearset2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.cm_refyearset2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_param_area_type', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_param_area_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_state_or_change', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_state_or_change_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_sub_population_category_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_target_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_target_variable_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_variable_type', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.c_variable_type_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_cell_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_param_area', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_a_param_area_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_p_plot', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.f_p_plot_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_auxiliary_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_total', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_aux_total_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_configuration', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_cluster_configuration_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_g_beta', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_g_beta_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_inventory_campaign', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_inventory_campaign_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_variables', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_model_variables_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2aux_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2aux_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_1stph_est_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_1stph_est_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_2ndph_est_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_panel2total_2ndph_est_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_plot_measurement_dates', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_plot_measurement_dates_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_reference_year_set', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_reference_year_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_result', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_result_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_strata_set', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_strata_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_stratum', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_stratum_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_target_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_target_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_conf', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_conf_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_data', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_total_estimate_data_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable', '');
SELECT pg_catalog.pg_extension_config_dump('@extschema@.t_variable_id_seq', '');

