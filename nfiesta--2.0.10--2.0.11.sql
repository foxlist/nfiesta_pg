--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-------------------------------------------views
drop view if exists @extschema@.v_conf_overview;
-- <view_name="v_conf_overvie" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
	select
		t_estimate_conf.id as conf_id,
		t_total_estimate_conf.id as tec_id,
		ted_2ndph.id as ted_2ndph_id,
		ted_0thph.id as ted_0thph_id,
		t_aux_conf.id as tac_id,
		t_total_estimate_conf_denom.id as tec_d_id,
		ted_2ndph_denom.id as ted_2ndph_d_id,
		ted_0thph_denom.id as ted_0thph_d_id,
		t_aux_conf_denom.id as tac_d_id,
		case 	when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null 
			then '1p_total'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null 
			then '1p1p_ratio'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = false
			then '2p_total_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p_total_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null and 
				t_aux_conf.sigma = false
			then '2p1p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p1p_ratio_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and 
				t_aux_conf_denom.sigma = false
			then '1p2p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and
				t_aux_conf_denom.sigma = true
			then '1p2p_ratio_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and 
				t_aux_conf.sigma = false
			then '2p2p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and
				t_aux_conf.sigma = true
			then '2p2p_ratio_with_sigma'
			else 'unknown'
		end as estimate_type_str,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label/*,
		t_panel2total_2ndph_est_data.panel as t_panel2total_2ndph_est_data_panel,
		array_agg(t_reference_year_set.label) as est_data_2ndph_ref_year_set_label*/
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
        inner join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_2ndph ON ted_2ndph.total_estimate_conf = t_total_estimate_conf.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_0thph ON ted_0thph.total_estimate_conf = t_total_estimate_conf.id
	left join @extschema@.t_aux_conf ON t_aux_conf.id = ted_0thph.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
        left join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_2ndph_denom ON ted_2ndph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_0thph_denom ON ted_0thph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = ted_0thph_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.f_a_cell on (t_total_estimate_conf.estimation_cell = f_a_cell.gid)
	inner join @extschema@.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (ted_2ndph.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
	/*inner join @extschema@.t_panel2total_2ndph_est_data on (t_panel2total_2ndph_est_data.total_estimate_data = ted_2ndph.id)
	inner join @extschema@.t_reference_year_set on (t_panel2total_2ndph_est_data.reference_year_set = t_reference_year_set.id)*/
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
);
--select * from @extschema@.v_conf_overview;

-- </view>

-------------------------------------------functions
-- <function_name="fn_1p_data" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint, 3035),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	left join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	left join @extschema@.t_model ON t_model.id = t_aux_conf.model
	left join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2total_2ndph_est_data.reference_year_set,
		t_panel2total_2ndph_est_data.panel,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.f_a_cell ON (f_a_cell.gid = cm_plot2cell_mapping.estimation_cell and w_configuration.cell = f_a_cell.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
                        w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			case when (t_variable.sub_population_category is not null) then 
				t_target_data.sub_population_category = t_variable.sub_population_category
			else t_target_data.sub_population_category is null end and
                        case when (t_variable.area_domain_category is not null) then 
				t_target_data.area_domain_category = t_variable.area_domain_category
			else t_target_data.area_domain_category is null end
                	)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_total_estimate_data on t_total_estimate_data.total_estimate_conf = ANY (w_configuration.t_total_estimate_conf__id)
	inner join @extschema@.t_panel2total_2ndph_est_data ON t_panel2total_2ndph_est_data.total_estimate_data = t_total_estimate_data.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
        INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
							and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_1p_data as (
	with w_ldsity_cluster as (select * from w_ldsity_cluster)
	select 
		/*w_ldsity_cluster.conf_id, */w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, 
		w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix, w_pix.sweight, NULL::double precision as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	--INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function_name="fn_g_beta" function_schema="extschema" src="functions/extschema/fn_g_beta.sql">
-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	r integer,
	c integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel2aux_conf.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		t_auxiliary_data.value as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot
	inner join @extschema@.t_variable on t_auxiliary_data.auxiliary_variable_category = t_variable.auxiliary_variable_category
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	WHERE t_variable.id = ANY (w_configuration.aux_attributes)
)
, w_ldsity_cluster AS (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r, c, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by r(ow) and c(column) indices.';

-- </function>

-- <function_name="fn_2p_data" function_schema="extschema" src="functions/extschema/fn_2p_data.sql">
-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint, 3035),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
        pix double precision,
        sweight double precision,
        DELTA_T__G_beta double precision,
        nb_sampling_units integer, 
        sweight_strata_sum double precision, 
        lambda_d_plus double precision,
        sigma boolean
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.gid)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel2total_2ndph_est_data.reference_year_set as reference_year_set, t_panel2total_2ndph_est_data.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
        inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON (t_panel.id = t_panel2total_2ndph_est_data.panel)
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_auxiliary_data.value as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot 
		inner join @extschema@.t_variable on t_variable.auxiliary_variable_category = t_auxiliary_data.auxiliary_variable_category
		inner join w_configuration on t_variable.id = ANY (w_configuration.aux_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
                inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
                        case when (t_variable.sub_population_category is not null) then
                                t_target_data.sub_population_category = t_variable.sub_population_category
                        else t_target_data.sub_population_category is null end and
                        case when (t_variable.area_domain_category is not null) then
                                t_target_data.area_domain_category = t_variable.area_domain_category
                        else t_target_data.area_domain_category is null end
	                )
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint, 3035) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
, w_X AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_Y AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_I AS (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS (
	select 
		w_cell_selection.conf_id,
		attribute as r,
		1 as c,
		aux_total as val 
	from
	(select estimation_cell as cell, v_ldsity_conf.id as attribute, aux_total 
		from @extschema@.t_aux_total 
		inner join @extschema@.v_ldsity_conf on (t_aux_total.auxiliary_variable_category = v_ldsity_conf.auxiliary_variable_category)
	) as t_aux_total
	inner join w_cell_selection on (w_cell_selection.cell_gid = t_aux_total.cell)
	inner join (select distinct conf_id, r from w_X) as foo on (foo.r = attribute and foo.conf_id = w_cell_selection.conf_id)
)
, w_DELTA_T AS (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_t_G_beta AS (
        select
                w_configuration.id as conf_id,
                (@extschema@.fn_g_beta(w_configuration.t_aux_conf__id[1])).*
        from w_configuration
/*        select
                w_configuration.id as conf_id,
                t_g_beta.r, t_g_beta.c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])*/
)
, w_DELTA_G_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_Y_T AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_Y_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_X_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
, w_residuals_plot AS ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS (
	with
	w_residuals_plot as (select * from w_residuals_plot),
	w_ldsity_plot as (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster as (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
, w_2p_data as (
	with w_ldsity_residuals_cluster as (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma as (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell, w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function_name="fn_1p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p1p_ratio_var.sql">
-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
from w_1p_ratio_var
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_1p2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p2p_ratio_var.sql">
-- Function: @extschema@.fn_1p2p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d_plus - denom.phi * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom order by cluster) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p, var2p 
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_2p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p1p_ratio_var.sql">
-- Function: @extschema@.fn_2p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster) as cids,
			array_agg(ldsity_d) as ldsitys,
			array_agg(sweight) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.ldsity_d_plus * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus from w_data__denom) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p, var2p 
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_2p2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p2p_ratio_var.sql">
-- Function: @extschema@.fn_2p2p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_2p2p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.phi * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p, var2p 
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>
