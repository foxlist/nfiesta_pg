--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_delete_total_estimate_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_total_estimate_conf.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_total_estimate_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_total_estimate_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_total_estimate_conf(_total_estimate_conf integer)
RETURNS void
AS
$function$
DECLARE
_estimate_conf integer[];
BEGIN

-- 3rd order tables
-- t_result
DELETE FROM @extschema@.t_result WHERE estimate_conf IN (
	SELECT 	id
	FROM 	@extschema@.t_estimate_conf
	WHERE	total_estimate_conf = $1 OR
		denominator = $1)
	;

-- 2nd order tables
-- t_panel2total_1stph_estimate_conf, 2ndph
DELETE FROM @extschema@.t_panel2total_2ndph_estimate_conf WHERE total_estimate_conf = $1;
DELETE FROM @extschema@.t_panel2total_1stph_estimate_conf WHERE total_estimate_conf = $1;
RAISE NOTICE 'Deleting panel to total_estimate_conf mapping...';
-- t_estimate_conf
WITH w AS (DELETE FROM @extschema@.t_estimate_conf WHERE total_estimate_conf = $1 OR denominator = $1 RETURNING id)
SELECT array_agg(id) FROM w INTO _estimate_conf;
RAISE NOTICE 'Deleting estimate configurations (%).', _estimate_conf;

-- 1st order table
-- t_total_estimate_conf
DELETE FROM @extschema@.t_total_estimate_conf WHERE id = $1;
RAISE NOTICE 'Deleting total estimate configuration = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_total_estimate_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function name="fn_delete_aux_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_aux_conf.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_aux_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_aux_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_aux_conf(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_g_beta
DELETE FROM @extschema@.t_g_beta WHERE aux_conf = $1;
RAISE NOTICE 'Deleting t_g_betas...';
-- t_panel2aux_conf
DELETE FROM @extschema@.t_panel2aux_conf WHERE aux_conf = $1;
RAISE NOTICE 'Deleting panel to auxiliary configuration mapping...';
-- t_total_estimate_conf
PERFORM	@extschema@.fn_delete_total_estimate_conf(id)
FROM	@extschema@.t_total_estimate_conf
WHERE	aux_conf = $1;

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.t_aux_conf WHERE id = $1;
RAISE NOTICE 'Deleting auxiliary configuration = %.', $1;

RETURN;

END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_aux_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function name="fn_delete_param_area" schema="extschema" src="functions/extschema/configuration/fn_delete_param_area.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_param_area(integer)
--DROP FUNCTION @extschema@.fn_delete_param_area(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_param_area(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_aux_conf
PERFORM	@extschema@.fn_delete_aux_conf(id)
FROM	@extschema@.t_aux_conf
WHERE	param_area = $1;

-- cm_cell2param_area_mapping
DELETE FROM @extschema@.cm_cell2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting cell to parametrization area mapping...';
-- cm_plot2param_area_mapping
DELETE FROM @extschema@.cm_plot2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting plot to parametrization area mapping...';

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.f_a_param_area WHERE gid = $1;
RAISE NOTICE 'Deleting parametrization area = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_param_area(integer) IS 'Function for deleting the parametrization area and all records in referenced tables.';


-- </function>

