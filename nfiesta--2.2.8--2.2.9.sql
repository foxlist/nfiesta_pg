--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--CREATE INDEX idx__f_p_plot__plot ON @extschema@.f_p_plot USING btree ((plot::text));

--COMMENT ON INDEX @extschema@.idx__f_p_plot__plot IS 'Btree index on column plot from table f_p_plot with retype to text.';

-- <function name="fn_make_estimate_table" schema="extschema" src="functions/extschema/fn_make_estimate_table.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_make_estimate_table(integer)

-- DROP FUNCTION @extschema@.fn_make_estimate_table(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_make_estimate_table(
    _estimate_conf integer
)
  RETURNS TABLE(
	estimate_conf 	integer,
	point		double precision,
	var		double precision,
	version		text,
	calc_started	timestamp with time zone,
	calc_duration	interval,
	est_info	json
	) AS
$BODY$
DECLARE
	_calc_started		timestamp with time zone;
	_estimate_type 		integer;
	_total_estimate_conf	integer;
	_denominator		integer;
	_total_est_type		integer;
	_denom_type		integer;
	_columns		text;
	_columns1		text;
	_columns2		text;
	_function_call		text;
	_q 			text;
BEGIN

	_calc_started := clock_timestamp();

	SELECT
		t1.estimate_type,
		t1.total_estimate_conf,
		t1.denominator,
		t2.phase_estimate_type,
		t3.phase_estimate_type
	FROM
		@extschema@.t_estimate_conf AS t1
	INNER JOIN
		@extschema@.t_total_estimate_conf AS t2
	ON	t1.total_estimate_conf = t2.id
	LEFT JOIN
		@extschema@.t_total_estimate_conf AS t3
	ON	t1.denominator = t3.id
	WHERE
		t1.id = $1
	INTO
		_estimate_type, _total_estimate_conf, _denominator, _total_est_type, _denom_type;

	_columns1 := 'point1p AS point, var1p AS var, est_info';
	_columns2 := 'point2p AS point, var2p AS var, est_info';

	CASE
	WHEN
		_estimate_type = 1	-- total
	THEN
		CASE
		WHEN _total_est_type = 1	-- 1p
		THEN
			_columns := _columns1;
			_function_call := concat('@extschema@.fn_1p_total_var(',_total_estimate_conf,')');

		WHEN _total_est_type = 2	-- 2p
		THEN
			_columns := _columns2;
			_function_call := concat('@extschema@.fn_2p_total_var(',_total_estimate_conf,')');
		ELSE
			RAISE EXCEPTION 'Defined total estimate type not implemented (%).', _total_est_type;
		END CASE;
	WHEN
		_estimate_type = 2	-- ratio
	THEN
		CASE
		WHEN _total_est_type = 1 AND _denom_type = 1
		THEN
			_columns := _columns1;
			_function_call := concat('@extschema@.fn_1p1p_ratio_var(',_total_estimate_conf,', ', _denominator,')');

		WHEN _total_est_type = 1 AND _denom_type = 2
		THEN
			_columns := _columns2;
			_function_call := concat('@extschema@.fn_1p2p_ratio_var(',_total_estimate_conf,', ', _denominator,')');

		WHEN _total_est_type = 2 AND _denom_type = 1
		THEN
			_columns := _columns2;
			_function_call := concat('@extschema@.fn_2p1p_ratio_var(',_total_estimate_conf,', ', _denominator,')');

		WHEN _total_est_type = 2 AND _denom_type = 2
		THEN
			_columns := _columns2;
			_function_call := concat('@extschema@.fn_2p2p_ratio_var(',_total_estimate_conf,', ', _denominator,')');
		ELSE
			RAISE EXCEPTION 'Defined combination of estimate types used in ratio not implemented (%, %).', _total_est_type, _denom_type;
		END CASE;
	ELSE
		RAISE EXCEPTION 'Unexpected estimate_type (%).', _estimate_type;
	END CASE;


--RETURN QUERY EXECUTE
_q :=	'WITH w AS (
		SELECT 
			$1 AS estimate_conf,
			'||quote_literal(_calc_started)||'::timestamp with time zone AS calc_started,
			'|| _columns ||',
			(SELECT version FROM pg_available_extension_versions WHERE name = ''nfiesta'' AND installed) AS version
		FROM
			'|| _function_call || '
	)
	SELECT
		estimate_conf,
		point,
		var,
		version,
		calc_started,
		clock_timestamp() - calc_started AS calc_duration,
		est_info
	FROM
		w
	'
	;	

RETURN QUERY EXECUTE _q
	USING _estimate_conf;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
;

COMMENT ON FUNCTION @extschema@.fn_make_estimate_table(integer) IS 'Wrapper function for calculation of estimate irrespective of the estimate type.';

-- </function>


