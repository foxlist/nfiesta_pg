-- https://gitlab.com/nfiesta/nfiesta_pg/issues/34

drop view if exists @extschema@.v_conf_overview;
-- <view_name="v_conf_overview" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs AS MATERIALIZED (
	select
		t_estimate_conf.id as estimate_conf,
		t_total_estimate_conf.id as total_estimate_conf,
		t_aux_conf.id as aux_conf,
		t_total_estimate_conf_denom.id as total_estimate_conf__denom,
		t_aux_conf_denom.id as aux_conf__denom,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label,
		t_total_estimate_conf.estimate_date_begin, t_total_estimate_conf.estimate_date_end
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.target_variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels AS MATERIALIZED (
	select w_confs.*,
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by t_panel2total_2ndph_estimate_conf.panel) as t_panel2total_2ndph_estimate_conf_panels,
		array_agg(t_panel2total_2ndph_estimate_conf.reference_year_set order by t_panel2total_2ndph_estimate_conf.panel) as est_data_2ndph_ref_year_sets
	from w_confs
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on (t_panel2total_2ndph_estimate_conf.total_estimate_conf = w_confs.total_estimate_conf)
	group by w_confs.estimate_conf, w_confs.total_estimate_conf, w_confs.aux_conf, w_confs.total_estimate_conf__denom, w_confs.aux_conf__denom,
			w_confs.estimate_type_str, w_confs.sigma, w_confs.force_synthetic,
			w_confs.param_area, w_confs.param_area_code,
			w_confs.model, w_confs.model_description,
			w_confs.estimation_cell, w_confs.estimation_cell_label, w_confs.estimation_cell_collection, w_confs.target_variable, w_confs.target_variable_label,
			w_confs.sub_population_category, w_confs.sub_population_category_label, w_confs.area_domain_category, w_confs.area_domain_category_label,
			w_confs.estimate_date_begin, w_confs.estimate_date_end
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel2aux_conf.panel order by t_panel2aux_conf.panel) as t_panel2aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel2aux_conf on (t_panel2aux_conf.aux_conf = w_confs_2nd_panels.aux_conf)
	group by w_confs_2nd_panels.estimate_conf, w_confs_2nd_panels.total_estimate_conf, w_confs_2nd_panels.aux_conf, w_confs_2nd_panels.total_estimate_conf__denom, w_confs_2nd_panels.aux_conf__denom,
			w_confs_2nd_panels.estimate_type_str, w_confs_2nd_panels.sigma, w_confs_2nd_panels.force_synthetic,
			w_confs_2nd_panels.param_area, w_confs_2nd_panels.param_area_code,
			w_confs_2nd_panels.model, w_confs_2nd_panels.model_description,
			w_confs_2nd_panels.estimation_cell, w_confs_2nd_panels.estimation_cell_label, w_confs_2nd_panels.estimation_cell_collection, w_confs_2nd_panels.target_variable, w_confs_2nd_panels.target_variable_label,
			w_confs_2nd_panels.sub_population_category, w_confs_2nd_panels.sub_population_category_label, w_confs_2nd_panels.area_domain_category, w_confs_2nd_panels.area_domain_category_label,
			w_confs_2nd_panels.estimate_date_begin, w_confs_2nd_panels.estimate_date_end,
			w_confs_2nd_panels.t_panel2total_2ndph_estimate_conf_panels, w_confs_2nd_panels.est_data_2ndph_ref_year_sets

);
--select * from @extschema@.v_conf_overview;

-- </view>
