--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view_name="v_conf_overview" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
	select
		t_estimate_conf.id as conf_id,
		t_total_estimate_conf.id as tec_id,
		ted_2ndph.id as ted_2ndph_id,
		ted_0thph.id as ted_0thph_id,
		t_aux_conf.id as tac_id,
		t_total_estimate_conf_denom.id as tec_d_id,
		ted_2ndph_denom.id as ted_2ndph_d_id,
		ted_0thph_denom.id as ted_0thph_d_id,
		t_aux_conf_denom.id as tac_d_id,
		case 	when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null 
			then '1p_total'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null 
			then '1p1p_ratio'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = false
			then '2p_total_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p_total_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null and 
				t_aux_conf.sigma = false
			then '2p1p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p1p_ratio_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and 
				t_aux_conf_denom.sigma = false
			then '1p2p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and
				t_aux_conf_denom.sigma = true
			then '1p2p_ratio_with_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and 
				t_aux_conf.sigma = false
			then '2p2p_ratio_no_sigma'
			when 
				ted_2ndph.id is not null and 
				ted_0thph.id is not null and 
				ted_2ndph_denom.id is not null and 
				ted_0thph_denom.id is not null and
				t_aux_conf.sigma = true
			then '2p2p_ratio_with_sigma'
			else 'unknown'
		end as estimate_type_str,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label/*,
		t_panel2total_2ndph_est_data.panel as t_panel2total_2ndph_est_data_panel,
		array_agg(t_reference_year_set.label) as est_data_2ndph_ref_year_set_label*/
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
        inner join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_2ndph ON ted_2ndph.total_estimate_conf = t_total_estimate_conf.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_0thph ON ted_0thph.total_estimate_conf = t_total_estimate_conf.id
	left join @extschema@.t_aux_conf ON t_aux_conf.id = ted_0thph.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
        left join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_2ndph_denom ON ted_2ndph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_0thph_denom ON ted_0thph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = ted_0thph_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (ted_2ndph.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
	/*inner join @extschema@.t_panel2total_2ndph_est_data on (t_panel2total_2ndph_est_data.total_estimate_data = ted_2ndph.id)
	inner join @extschema@.t_reference_year_set on (t_panel2total_2ndph_est_data.reference_year_set = t_reference_year_set.id)*/
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
);
--select * from @extschema@.v_conf_overview;

-- </view>
