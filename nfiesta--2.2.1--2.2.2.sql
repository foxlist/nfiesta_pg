--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_create_param_area" schema="extschema" src="functions/extschema/configuration/fn_create_param_area.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_create_param_area(integer, regclass)
--DROP FUNCTION @extschema@.fn_create_param_area(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_create_param_area(_param_area_type integer, _estimation_cells integer[], _param_area_code varchar DEFAULT NULL)
RETURNS integer
AS
$function$
DECLARE
	_param_area integer;
	_not_known integer[];
BEGIN
	-- find out, if there are estimation cells given in parameter
	_not_known := (
		SELECT array_agg(estimation_cell ORDER BY estimation_cell)
		FROM
			(SELECT unnest(_estimation_cells) AS estimation_cell
			EXCEPT
			SELECT id FROM @extschema@.c_estimation_cell) AS t1
	);

	-- if there are not -> exception
	IF _not_known IS NOT NULL
	THEN
		RAISE EXCEPTION 'Estimation cell does not exist in lookup c_estimation_cell (%)!', _not_known;
	END IF;

	-- insert new param area with unioned geometry
	INSERT INTO @extschema@.f_a_param_area (param_area_type, param_area_code, geom)
	SELECT
		_param_area_type,
		CASE
		WHEN _param_area_code IS NOT NULL THEN _param_area_code
		ELSE string_agg(label,'+')
		END AS param_area_code,
		ST_Multi(ST_union(geom))
	FROM
		@extschema@.f_a_cell AS t1
	INNER JOIN @extschema@.c_estimation_cell AS t2
	ON	t1.estimation_cell = t2.id
	WHERE
		_estimation_cells @> array[estimation_cell]
	RETURNING gid
	INTO _param_area;

	-- insert mapping between cells and param_area
	INSERT INTO @extschema@.cm_cell2param_area_mapping(estimation_cell, param_area)
	SELECT unnest(_estimation_cells), _param_area;

	-- insert mapping between plots and param_area
	INSERT INTO @extschema@.cm_plot2param_area_mapping(plot, param_area)
	SELECT
		plot, _param_area
	FROM
		@extschema@.cm_plot2cell_mapping
	WHERE
		array[estimation_cell] <@ _estimation_cells;

RETURN _param_area;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_create_param_area(_param_area_type integer, _estimation_cells integer[], _param_area_code varchar) IS 'Function for creation of new parametrization area from given estimation cells.';

-- </function>

-- <function name="fn_delete_total_estimate_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_total_estimate_conf.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_total_estimate_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_total_estimate_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_total_estimate_conf(_total_estimate_conf integer)
RETURNS void
AS
$function$
DECLARE
_estimate_conf integer[];
BEGIN

-- 3rd order tables
-- t_result
DELETE FROM @extschema@.t_result WHERE estimate_conf IN (
	SELECT 	id
	FROM 	@extschema@.t_estimate_conf
	WHERE	total_estimate_conf = $1 OR
		denominator = $1)
	;

-- 2nd order tables
-- t_panel2total_1stph_estimate_conf, 2ndph
DELETE FROM @extschema@.t_panel2total_2ndph_estimate_conf WHERE total_estimate_conf = $1;
DELETE FROM @extschema@.t_panel2total_1stph_estimate_conf WHERE total_estimate_conf = $1;
RAISE NOTICE 'Deleting panel to total_estimate_conf mapping...';
-- t_estimate_conf
WITH w AS (DELETE FROM @extschema@.t_estimate_conf WHERE total_estimate_conf = $1 OR denominator = $1 RETURNING id)
SELECT array_agg(id) FROM w INTO _estimate_conf;
RAISE NOTICE 'Deleting estimate configurations (%).', _estimate_conf;

-- 1st order table
-- t_total_estimate_conf
DELETE FROM @extschema@.t_total_estimate_conf WHERE id = $1;
RAISE NOTICE 'Deleting total estimate configuration = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_total_estimate_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function name="fn_delete_aux_conf" schema="extschema" src="functions/extschema/configuration/fn_delete_aux_conf.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_aux_conf(integer)
--DROP FUNCTION @extschema@.fn_delete_aux_conf(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_aux_conf(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_g_beta
DELETE FROM @extschema@.t_g_beta WHERE aux_conf = $1;
RAISE NOTICE 'Deleting t_g_betas...';
-- t_panel2aux_conf
DELETE FROM @extschema@.t_panel2aux_conf WHERE aux_conf = $1;
RAISE NOTICE 'Deleting panel to auxiliary configuration mapping...';
-- t_total_estimate_conf
PERFORM	@extschema@.fn_delete_total_estimate_conf(id)
FROM	@extschema@.t_total_estimate_conf
WHERE	aux_conf = $1;

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.t_aux_conf WHERE id = $1;
RAISE NOTICE 'Deleting auxiliary configuration = %.', $1;

RETURN;

END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_aux_conf(integer) IS 'Function for auxiliary configuration deletion with all records in referenced tables.';

-- </function>

-- <function name="fn_delete_param_area" schema="extschema" src="functions/extschema/configuration/fn_delete_param_area.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_delete_param_area(integer)
--DROP FUNCTION @extschema@.fn_delete_param_area(integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_delete_param_area(_aux_conf integer)
RETURNS void
AS
$function$
DECLARE
BEGIN

-- 2nd order tables
-- t_aux_conf
PERFORM	@extschema@.fn_delete_aux_conf(id)
FROM	@extschema@.t_aux_conf
WHERE	param_area = $1;

-- cm_cell2param_area_mapping
DELETE FROM @extschema@.cm_cell2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting cell to parametrization area mapping...';
-- cm_plot2param_area_mapping
DELETE FROM @extschema@.cm_plot2param_area_mapping WHERE param_area = $1;
RAISE NOTICE 'Deleting plot to parametrization area mapping...';

-- 1st order table
-- t_aux_conf
DELETE FROM @extschema@.f_a_param_area WHERE gid = $1;
RAISE NOTICE 'Deleting parametrization area = %.', $1;

RETURN;
END;
$function$
LANGUAGE plpgsql
PARALLEL UNSAFE;

COMMENT ON FUNCTION @extschema@.fn_delete_param_area(integer) IS 'Function for deleting the parametrization area and all records in referenced tables.';


-- </function>

-- <view name="v_conf_overview" schema="extschema" src="views/extschema/v_conf_overview.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
with w_confs as (
	select
		t_estimate_conf.id as conf_id,
		t_total_estimate_conf.id as tec_id,
		t_aux_conf.id as tac_id,
		t_total_estimate_conf_denom.id as tec_d_id,
		t_aux_conf_denom.id as tac_d_id,
		case 	when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '1p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '1p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is null and 
				t_aux_conf_denom.id is null 
			then '2p_total'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is null 
			then '2p1p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '1p2p_ratio'
			when 
				t_total_estimate_conf.id is not null and 
				t_aux_conf.id is not null and 
				t_total_estimate_conf_denom.id is not null and 
				t_aux_conf_denom.id is not null 
			then '2p2p_ratio'
			else 'unknown'
		end as estimate_type_str,
		coalesce(t_aux_conf.sigma, t_aux_conf_denom.sigma) as sigma,
		coalesce(t_total_estimate_conf.force_synthetic, t_total_estimate_conf_denom.force_synthetic) as force_synthetic,
------------------------additional info begin-----------------------------------
		coalesce(t_aux_conf.param_area, t_aux_conf_denom.param_area) as param_area, 
		coalesce(f_a_param_area.param_area_code, f_a_param_area_denom.param_area_code) as param_area_code,
		coalesce(t_aux_conf.model, t_aux_conf_denom.model) as model, 
		coalesce(t_model.description, t_model_denom.description) as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, c_estimation_cell.estimation_cell_collection, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label,
		t_variable.area_domain_category, c_area_domain_category.label as area_domain_category_label
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
	left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_conf.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = t_total_estimate_conf_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	left join @extschema@.f_a_param_area as f_a_param_area_denom on (t_aux_conf_denom.param_area = f_a_param_area_denom.gid)
	left join @extschema@.t_model as t_model_denom on (t_aux_conf_denom.model = t_model_denom.id)
	inner join @extschema@.c_estimation_cell on (t_total_estimate_conf.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (t_total_estimate_conf.target_variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
	left join @extschema@.c_area_domain_category on (t_variable.area_domain_category = c_area_domain_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
)
, w_confs_2nd_panels as (
	select w_confs.*,
		array_agg(t_panel2total_2ndph_estimate_conf.panel order by t_panel2total_2ndph_estimate_conf.panel) as t_panel2total_2ndph_estimate_conf_panels,
		array_agg(t_panel2total_2ndph_estimate_conf.reference_year_set order by t_panel2total_2ndph_estimate_conf.panel) as est_data_2ndph_ref_year_sets
	from w_confs
	inner join @extschema@.t_panel2total_2ndph_estimate_conf on (t_panel2total_2ndph_estimate_conf.total_estimate_conf = w_confs.tec_id)
	group by conf_id, tec_id, tac_id, tec_d_id, tac_d_id,
			estimate_type_str, sigma, force_synthetic,
			param_area, param_area_code, 
			model, model_description, 
			estimation_cell, estimation_cell_label, estimation_cell_collection, target_variable, target_variable_label, 
			sub_population_category, sub_population_category_label, area_domain_category, area_domain_category_label
)
	select w_confs_2nd_panels.*,
		array_agg(t_panel2aux_conf.panel order by t_panel2aux_conf.panel) as t_panel2aux_conf_panels
	from w_confs_2nd_panels
	left join @extschema@.t_panel2aux_conf on (t_panel2aux_conf.aux_conf = w_confs_2nd_panels.tac_id)
	group by conf_id, tec_id, tac_id, tec_d_id, tac_d_id,
			estimate_type_str, sigma, force_synthetic,
			param_area, param_area_code, 
			model, model_description, 
			estimation_cell, estimation_cell_label, estimation_cell_collection, target_variable, target_variable_label, 
			sub_population_category, sub_population_category_label, area_domain_category, area_domain_category_label,
			t_panel2total_2ndph_estimate_conf_panels, est_data_2ndph_ref_year_sets

);
--select * from @extschema@.v_conf_overview;

-- </view>

