--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--
-- Name: v_ldsity_conf; Type: MATERIALIZED VIEW; Schema: @extschema@. Owner: -
--
--drop materialized view if exists @extschema@.v_ldsity_conf;

CREATE MATERIALIZED VIEW @extschema@.v_ldsity_conf AS
 WITH w_target AS MATERIALIZED (
         SELECT NULL::integer AS auxiliary_variable_category,
            t1.target_variable,
            t1.area_domain_category,
            t1.sub_population_category,
            NULL::character varying AS auxiliary_variable_category_label,
            t2.label AS target_variable_label,
            t3.label AS sub_population_category_label,
            t4.label AS area_domain_category_label,
            count(*) AS total
           FROM (((@extschema@.t_target_data t1
             JOIN @extschema@.c_target_variable t2 ON ((t1.target_variable = t2.id)))
             LEFT JOIN @extschema@.c_sub_population_category t3 ON ((t1.sub_population_category = t3.id)))
             LEFT JOIN @extschema@.c_area_domain_category t4 ON ((t1.area_domain_category = t4.id)))
          GROUP BY t1.target_variable, t1.sub_population_category, t1.area_domain_category, t2.label, t3.label, t4.label
          ORDER BY t1.target_variable, t1.sub_population_category, t1.area_domain_category
        ), w_aux AS MATERIALIZED (
         SELECT t1.auxiliary_variable_category,
            NULL::integer AS target_variable,
            NULL::integer AS area_domain_category,
            NULL::integer AS sub_population_category,
            t2.label AS auxiliary_variable_category_label,
            NULL::character varying AS target_variable_label,
            NULL::character varying AS sub_population_category_label,
            NULL::character varying AS area_domain_category_label,
            count(*) AS total
           FROM (@extschema@.t_auxiliary_data t1
             JOIN @extschema@.c_auxiliary_variable_category t2 ON ((t1.auxiliary_variable_category = t2.id)))
          GROUP BY t1.auxiliary_variable_category, t2.label
          ORDER BY t1.auxiliary_variable_category
        ), w_all AS MATERIALIZED (
         SELECT w_target.auxiliary_variable_category,
            w_target.target_variable,
            w_target.area_domain_category,
            w_target.sub_population_category,
            w_target.auxiliary_variable_category_label,
            w_target.target_variable_label,
            w_target.sub_population_category_label,
            w_target.area_domain_category_label,
            w_target.total
           FROM w_target
        UNION ALL
         SELECT w_aux.auxiliary_variable_category,
            w_aux.target_variable,
            w_aux.area_domain_category,
            w_aux.sub_population_category,
            w_aux.auxiliary_variable_category_label,
            w_aux.target_variable_label,
            w_aux.sub_population_category_label,
            w_aux.area_domain_category_label,
            w_aux.total
           FROM w_aux
        )
 SELECT
    w_all.auxiliary_variable_category,
    w_all.target_variable,
    w_all.area_domain_category,
    w_all.sub_population_category,
    w_all.auxiliary_variable_category_label,
    w_all.target_variable_label,
    w_all.sub_population_category_label,
    w_all.area_domain_category_label,
    w_all.total
   FROM w_all
  WITH NO DATA;

