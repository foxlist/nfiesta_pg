--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

/*
 	small improvements
*/

ALTER TABLE @extschema@.f_a_cell DROP COLUMN cell_inside;
ALTER TABLE @extschema@.f_a_cell DROP COLUMN cell_intersects;

-- lookup of estimate type according to number of phases
CREATE TABLE @extschema@.c_phase_estimate_type (
    id integer NOT NULL,
    label character varying(120) NOT NULL,
    description text NOT NULL
);

COMMENT ON TABLE @extschema@.c_phase_estimate_type IS 'Table of estimate types (total, ratio).';
COMMENT ON COLUMN @extschema@.c_phase_estimate_type.id IS 'Identifier of estimate types.';
COMMENT ON COLUMN @extschema@.c_phase_estimate_type.label IS 'Label of estimate types.';
COMMENT ON COLUMN @extschema@.c_phase_estimate_type.description IS 'Description of estimate types.';

INSERT INTO @extschema@.c_phase_estimate_type (id, label, description) VALUES
(1, '1p', 'One phase estimate.'),
(2, '2p-reg', 'Regression estimate with wall2wall auxiliaries.'),
(3, '2p', 'Two phase estimate.');


ALTER TABLE @extschema@.t_total_estimate_conf ADD COLUMN target_variable integer;
ALTER TABLE @extschema@.t_total_estimate_conf ADD COLUMN phase_estimate_type integer;

ALTER TABLE @extschema@.c_phase_estimate_type
	ADD CONSTRAINT pkey__c_phase_estimate_type__id PRIMARY KEY (id);

ALTER TABLE @extschema@.t_total_estimate_conf
	ADD CONSTRAINT fkey__t_total_estimate_conf__t_variable
	FOREIGN KEY (target_variable) REFERENCES @extschema@.t_variable(id);

ALTER TABLE @extschema@.t_total_estimate_conf
	ADD CONSTRAINT fkey__t_total_estimate_conf__c_phase_estimate_type
	FOREIGN KEY (phase_estimate_type) REFERENCES @extschema@.c_phase_estimate_type(id);

COMMENT ON COLUMN @extschema@.t_total_estimate_conf.target_variable IS 'Target variable of estimate.';
COMMENT ON COLUMN @extschema@.t_total_estimate_conf.phase_estimate_type IS 'Type of estimate considering the character of methodological principals of estimate phases combination.';

UPDATE @extschema@.t_total_estimate_conf SET target_variable = t1.variable
FROM
	@extschema@.t_total_estimate_data AS t1
WHERE
	t_total_estimate_conf.id = t1.total_estimate_conf AND
	t1.phase = 2;

UPDATE @extschema@.t_total_estimate_conf SET phase_estimate_type = t1.phase_estimate_type
FROM
	(
	SELECT
		DISTINCT
		id AS total_estimate_conf,
		CASE
		WHEN total = 1 THEN 1
		WHEN total = 2 AND max_aux_phase_type = 1 THEN 2 
		WHEN total = 2 AND max_aux_phase_type = 2 THEN 3
		ELSE NULL
		END AS phase_estimate_type  -- currently there is no implementation of 2p estimate (only regression)
	FROM
		(SELECT
			t1.id,
			max(aux_phase_type) OVER (PARTITION BY t1.id) AS max_aux_phase_type,
			count(t2.id) OVER(PARTITION BY t1.id) AS total
		FROM
			@extschema@.t_total_estimate_conf AS t1
		INNER JOIN
			@extschema@.t_total_estimate_data AS t2
		ON
			t1.id = t2.total_estimate_conf
		) AS t1

	) AS t1
WHERE
	t_total_estimate_conf.id = t1.total_estimate_conf;


