--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

alter table @extschema@.f_a_cell alter COLUMN geom type geometry(MultiPolygon);
alter table @extschema@.f_a_param_area alter COLUMN geom type geometry(MultiPolygon);
alter table @extschema@.f_p_plot alter COLUMN geom type geometry(Point);
alter table @extschema@.t_stratum alter COLUMN geom type geometry(MultiPolygon);
alter table @extschema@.t_stratum alter COLUMN geom_buffered type geometry(MultiPolygon);

-- <function_name="fn_create_buffered_geometry" function_schema="extschema" src="functions/extschema/fn_create_buffered_geometry.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_create_buffered_geometry
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon), integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION
@extschema@.fn_create_buffered_geometry
(
	_geom			geometry(MultiPolygon),
	_tract_type		integer,
	_point_distance_m	integer
)

RETURNS geometry(MultiPolygon)
AS
$$
DECLARE
	_geom_buff		geometry(MultiPolygon);
	_axis_shift		double precision;
BEGIN

	CASE
	WHEN _tract_type = 100
	THEN
		 RETURN ST_Multi(ST_Buffer(_geom,_point_distance_m));

	WHEN _tract_type = 200
	THEN
		_axis_shift := ( (_point_distance_m^2) / 2.0 )^(1/2.0);


		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu, na opacnou stratnu, nez se vytvari trakt
					ST_Translate(_geom, -_axis_shift, -_axis_shift)
				) AS geom
			)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				x, y, geom,
				x-_point_distance_m AS x_shift,
				y-_point_distance_m AS y_shift,
				ST_SetSRID(ST_MakePoint(x-_axis_shift, y-_axis_shift), st_srid(geom)) AS geom_shift
			FROM
				w_coordinates
		)
		,w_make_line AS (
			SELECT
				ST_Union(ST_MakeLine(geom,geom_shift)) AS line
			FROM 
				w_shift_points
			)
		,w_union_all AS (
			SELECT
				ST_Union(t1.line, ST_Boundary(t2.geom)) AS all_lines
			FROM
				w_make_line AS t1,
				w_only_shifted AS t2
			)
		SELECT
			ST_Multi(ST_buildarea(all_lines))
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;

	WHEN _tract_type = 300
	THEN

		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu doleva
					ST_Translate(_geom, -(x_shift*_point_distance_m), -(y_shift*_point_distance_m))
				) AS geom_unioned
			FROM
				(SELECT unnest(ARRAY[1,1,0]) AS x_shift, unnest(ARRAY[0,1,1]) AS y_shift) AS t1
			)
		,w_union_only_shifted AS (
			SELECT ST_Union(ST_MakeValid(geom_unioned)) AS geom_unioned
			FROM w_only_shifted
		)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				geom,
				ST_SetSRID(ST_MakePoint(x-(x_shift[1]*_point_distance_m), y-(y_shift[1]*_point_distance_m)),st_srid(geom)) AS geom_shift_l,
				ST_SetSRID(ST_MakePoint(x-(x_shift[2]*_point_distance_m), y-(y_shift[2]*_point_distance_m)),st_srid(geom)) AS geom_shift_ld,
				ST_SetSRID(ST_MakePoint(x-(x_shift[3]*_point_distance_m), y-(y_shift[3]*_point_distance_m)),st_srid(geom)) AS geom_shift_d
			FROM
				w_coordinates AS t1,
				(SELECT ARRAY[1,1,0] AS x_shift, ARRAY[0,1,1] AS y_shift) AS t2
		)
		,w_make_line AS (
			-- make the square from 4 lines
			SELECT	ST_MakeLine(geom,geom_shift_l) AS line			FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_l,geom_shift_ld) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_ld,geom_shift_d) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_d,geom) AS line			FROM w_shift_points
			-- diagonal line to prevent st_buildarea to exclude squares
			UNION ALL
			SELECT	ST_MakeLine(geom,geom_shift_ld) AS line			FROM w_shift_points
		)
		,w_union_lines AS (
			SELECT ST_Collect(line) AS line FROM w_make_line
		)
		,w_area_lines AS (
			SELECT
					ST_BuildArea(line) as area_lines
			FROM
					w_union_lines
		)
		,w_area_geoms AS (
			SELECT
				ST_BuildArea(ST_MakeValid(geom_unioned)) AS area_geoms
			FROM
				w_union_only_shifted
		),
		w_union_all AS (
			SELECT
				ST_union(t1.area_lines,t2.area_geoms) AS geom
			FROM
				w_area_lines AS t1,
				w_area_geoms AS t2
		)
		SELECT
			ST_Multi(geom)
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;
	ELSE
		RAISE EXCEPTION 'Not known tract type!';
	END CASE;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION @extschema@.fn_create_buffered_geometry(geometry(MultiPolygon), integer, integer) IS
'Functions generates a buffered geometry of original geographical domain for specified tract_type.
100 -- 2p, 1p rotated -- Two points, the second is rotated around the first.
200 -- 2p, no rotation -- N-E, Two points, no rotation, the second located North-East.
300 -- 4p in square, N-E -- Four points in square shape, no rotation. North, North-East, Eeast locations.
';

-- </function>

-- <function_name="fn_g_beta" function_schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	r integer,
	c integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel2aux_conf.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		t_auxiliary_data.value as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot
	inner join @extschema@.t_variable on t_auxiliary_data.auxiliary_variable_category = t_variable.auxiliary_variable_category
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	WHERE t_variable.id = ANY (w_configuration.aux_attributes)
)
, w_ldsity_cluster AS (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r, c, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by r(ow) and c(column) indices.';

-- </function>

-- <function_name="fn_1p_data" function_schema="extschema" src="functions/extschema/fn_1p_data.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_data(integer)

-- DROP FUNCTION @extschema@.fn_1p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
	pix double precision,
	sweight double precision,
	DELTA_T__G_beta double precision,
	nb_sampling_units integer, 
	sweight_strata_sum double precision, 
	lambda_d_plus double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		left join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	left join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	left join @extschema@.t_model ON t_model.id = t_aux_conf.model
	left join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as cell_gid,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, t_panel2total_2ndph_est_data.reference_year_set,
		t_panel2total_2ndph_est_data.panel,
		f_p_plot.geom
	from w_configuration
	inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
	inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.f_a_cell ON (f_a_cell.estimation_cell = cm_plot2cell_mapping.estimation_cell and w_configuration.cell = f_a_cell.estimation_cell)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
                        w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
		inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
			case when (t_variable.sub_population_category is not null) then 
				t_target_data.sub_population_category = t_variable.sub_population_category
			else t_target_data.sub_population_category is null end and
                        case when (t_variable.area_domain_category is not null) then 
				t_target_data.area_domain_category = t_variable.area_domain_category
			else t_target_data.area_domain_category is null end
                	)
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
		t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster, 
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_total_estimate_data on t_total_estimate_data.total_estimate_conf = ANY (w_configuration.t_total_estimate_conf__id)
	inner join @extschema@.t_panel2total_2ndph_est_data ON t_panel2total_2ndph_est_data.total_estimate_data = t_total_estimate_data.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2total_2ndph_est_data.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
        INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
							and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_1p_data as (
	with w_ldsity_cluster as (select * from w_ldsity_cluster)
	select 
		/*w_ldsity_cluster.conf_id, */w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, 
		w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		NULL::double precision as ldsity_res_D, NULL::double precision as ldsity_res_D_plus,
		NULL::double precision as pix, w_pix.sweight, NULL::double precision as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	--INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
select * from w_1p_data;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_1p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

-- <function_name="fn_2p_data" function_schema="extschema" src="functions/extschema/fn_2p_data.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_data(integer)

-- DROP FUNCTION @extschema@.fn_2p_data(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_data(
    IN conf_id integer
)
  RETURNS TABLE(
	gid bigint,
	cluster integer,
	attribute integer,
	stratum integer,
	plots_per_cluster integer,
	plcount bigint,
	cluster_is_in_cell boolean,
	ldsity_d double precision,
	ldsity_d_plus double precision,
	is_aux boolean,
	is_target boolean,
	geom geometry(MultiPoint),
	ldsity_res_d double precision,
	ldsity_res_d_plus double precision,
        pix double precision,
        sweight double precision,
        DELTA_T__G_beta double precision,
        nb_sampling_units integer, 
        sweight_strata_sum double precision, 
        lambda_d_plus double precision,
        sigma boolean
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
---------------------------------------------------------
-- Configuration block
---------------------------------------------------------
w_configuration AS (
	with w_a as (
		select
			t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma, t_aux_conf.param_area,
			array_agg(t_total_estimate_conf.id order by t_total_estimate_data.variable) as t_total_estimate_conf__id,
			array_agg(t_aux_conf.id order by t_total_estimate_data.variable) as t_aux_conf__id,
			array_agg(t_total_estimate_data.variable order by t_total_estimate_data.variable) as target_attributes,
			t_total_estimate_data.aux_conf
		from @extschema@.t_total_estimate_conf
		inner join @extschema@.t_total_estimate_data ON t_total_estimate_data.total_estimate_conf = t_total_estimate_conf.id
		inner join @extschema@.t_aux_conf ON t_aux_conf.id = t_total_estimate_data.aux_conf
		where t_total_estimate_conf.id = ' || conf_id || '
		group by t_total_estimate_conf.estimation_cell, t_aux_conf.model, t_aux_conf.sigma,
			t_aux_conf.param_area, t_total_estimate_data.aux_conf
	)
	select
		t_total_estimate_conf__id[1] as id,
		w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id,
		w_a.param_area, w_a.estimation_cell as cell, w_a.model, w_a.sigma,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes, w_a.target_attributes
	from w_a
	inner join @extschema@.t_aux_conf ON t_aux_conf.id = w_a.aux_conf
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
	group by w_a.estimation_cell, w_a.model, w_a.sigma, w_a.param_area, w_a.target_attributes, w_a.t_total_estimate_conf__id, w_a.t_aux_conf__id
	order by id limit 1
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_cell_selection AS (
	SELECT
		w_configuration.id as conf_id,
		estimation_cell as estimation_cell,
		geom
	FROM @extschema@.f_a_cell
	INNER JOIN w_configuration ON (w_configuration.cell = f_a_cell.estimation_cell)
)
---------------------------------------------------------
-- Data block
---------------------------------------------------------
, w_plot AS (
	select distinct
		f_p_plot.gid, w_configuration.id as conf_id, t_cluster.id as cluster, t_panel.stratum,
		cm_plot2cell_mapping.id IS NOT NULL AS plot_is_in_cell, 
		-- t_panel2aux_conf.reference_year_set as reference_year_set, t_panel2aux_conf.panel as panel,
		t_panel2total_2ndph_est_data.reference_year_set as reference_year_set, t_panel2total_2ndph_est_data.panel as panel,
		f_p_plot.geom
	from w_configuration
        inner join @extschema@.t_total_estimate_data on w_configuration.t_total_estimate_conf__id[1] = t_total_estimate_data.total_estimate_conf
        inner join @extschema@.t_panel2total_2ndph_est_data on t_total_estimate_data.id = t_panel2total_2ndph_est_data.total_estimate_data
	--inner join @extschema@.t_panel2aux_conf on w_configuration.t_aux_conf__id[1] = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON (t_panel.id = t_panel2total_2ndph_est_data.panel)
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	left join @extschema@.cm_plot2cell_mapping ON (cm_plot2cell_mapping.estimation_cell = w_configuration.cell and cm_plot2cell_mapping.plot = f_p_plot.gid)
	inner join @extschema@.cm_plot2param_area_mapping ON (cm_plot2param_area_mapping.param_area = w_configuration.param_area and cm_plot2param_area_mapping.plot = f_p_plot.gid)
)
, w_ldsity_plot AS (
	with w_plot as (select * from w_plot)
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_auxiliary_data.value as ldsity,
			true as is_aux, false as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot 
		inner join @extschema@.t_variable on t_variable.auxiliary_variable_category = t_auxiliary_data.auxiliary_variable_category
		inner join w_configuration on t_variable.id = ANY (w_configuration.aux_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	union all
		select
			w_plot.gid,
			w_plot.conf_id,
			w_plot.stratum,
			w_plot.panel,
			w_plot.cluster,
			t_variable.id as attribute,
			t_cluster_configuration.plots_per_cluster,
			w_plot.plot_is_in_cell,
			t_target_data.value as ldsity,
			false as is_aux, true as is_target,
			w_plot.geom
		from w_plot as w_plot
		inner join @extschema@.t_target_data on (w_plot.gid = t_target_data.plot and w_plot.reference_year_set = t_target_data.reference_year_set)
                inner join @extschema@.t_variable on (
			t_target_data.target_variable = t_variable.target_variable and
                        case when (t_variable.sub_population_category is not null) then
                                t_target_data.sub_population_category = t_variable.sub_population_category
                        else t_target_data.sub_population_category is null end and
                        case when (t_variable.area_domain_category is not null) then
                                t_target_data.area_domain_category = t_variable.area_domain_category
                        else t_target_data.area_domain_category is null end
	                )
		inner join w_configuration on t_variable.id = ANY (w_configuration.target_attributes)
		inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
		inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
)
, w_ldsity_cluster AS (
	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid,
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_ldsity_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_D,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus,
		w_ldsity_plot.is_aux, w_ldsity_plot.is_target,
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux, is_target
	ORDER BY stratum, cluster, attribute
)
---------------------------------------------------------
-- Matrix block
---------------------------------------------------------
, w_X AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_Y AS (
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D as val_D,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_target
)
, w_clusters AS (-------------------------LIST OF CLUSTERS
	select distinct conf_id, stratum, panel, cluster, cluster_is_in_cell from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = ANY (w_configuration.t_aux_conf__id)
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		w_strata_sum.sweight_strata_sum / (w_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix,
		cm_cluster2panel_mapping.sampling_weight as sweight
	FROM w_clusters
	INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster 
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as w_strata_sum ON w_clusters.stratum = w_strata_sum.f_a_sampling_stratum_gid
)
, w_I AS (
	SELECT 
		conf_id,
		1 AS r,
		cluster AS c,
		cluster_is_in_cell::int AS val
	FROM	w_clusters
	order by r, c
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A 
	inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_t_hat AS (  -- eq. 18
	select 
		A.conf_id,
		A.r,
		1 AS c,
		sum(A.val_D * B.val * C.val) as val
	from w_X 		AS A 
	inner join w_PI     	AS B 	on (A.c = B.c and A.conf_id = B.conf_id)
	inner join w_I		AS C 	on (A.c = C.c and A.conf_id = C.conf_id)
	group by A.r, A.conf_id
	order by A.r
)
, w_t AS (
	select 
		w_cell_selection.conf_id,
		attribute as r,
		1 as c,
		aux_total as val 
	from
	(select estimation_cell as cell, t_variable.id as attribute, aux_total 
		from @extschema@.t_aux_total 
		inner join @extschema@.t_variable on (t_aux_total.auxiliary_variable_category = t_variable.auxiliary_variable_category)
	) as t_aux_total
	inner join w_cell_selection on (w_cell_selection.estimation_cell = t_aux_total.cell)
	inner join (select distinct conf_id, r from w_X) as foo on (foo.r = attribute and foo.conf_id = w_cell_selection.conf_id)
)
, w_DELTA_T AS (
	select
		conf_id,
		c as r, 
		r as c, 
		w_t.val - w_t_hat.val as val 
	from w_t 
	inner join w_t_hat using (r, c, conf_id)
)
, w_t_G_beta AS (
/*        select
                w_configuration.id as conf_id,
                (@extschema@.fn_g_beta(w_configuration.t_aux_conf__id[1])).*
        from w_configuration*/
        select
                w_configuration.id as conf_id,
                t_g_beta.r, t_g_beta.c, t_g_beta.val
        from @extschema@.t_g_beta
        inner join w_configuration on (t_g_beta.aux_conf = w_configuration.t_aux_conf__id[1])
)
, w_DELTA_G_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM 	w_DELTA_T as A
	inner join w_t_G_beta as B ON (A.c = B.r and A.conf_id = B.conf_id)
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_Y_T AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val,
		val_D -- used for 1p estimete
	from w_Y
	order by r, c
)
, w_G_PI AS (
	select 
		A.conf_id,
		A.r, 
		A.c AS c,
		A.val * B.val as val
	from w_t_G_beta AS A, w_PI AS B 
	where A.c = B.c and A.conf_id = B.conf_id
	order by r, c
)
, w_beta_hat AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_G_PI as A, w_Y_T as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c
)
, w_X_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_aux
)
, w_Y_plots_T AS (
	SELECT conf_id, gid AS r, cluster, attribute AS c, plots_per_cluster, ldsity AS val 
	FROM w_ldsity_plot WHERE is_target
)
, w_X_beta AS (
	select
		A.conf_id,
		A.r,
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_plots_T as A, w_beta_hat as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	order by r, c 
)
, w_residuals_plot AS ( -------------------------RESIDUAL LOCAL DENSITY ON TRACT LEVEL
	select
		w_Y_plots_T.conf_id,
		w_Y_plots_T.r AS plot, 
		w_Y_plots_T.c AS attribute, 
		(w_Y_plots_T.val - w_X_beta.val) AS ldsity 
	from w_Y_plots_T 
	INNER JOIN w_X_beta USING (r, c, conf_id)
)
, w_residuals_cluster AS (
	with
	w_residuals_plot as (select * from w_residuals_plot),
	w_ldsity_plot as (select * from w_ldsity_plot)
	SELECT
		w_ldsity_plot.conf_id,
		w_ldsity_plot.cluster,
		w_residuals_plot.attribute,
		w_ldsity_plot.stratum,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(plot_is_in_cell::int) > 0 as cluster_is_in_cell,
		sum(w_residuals_plot.ldsity * plot_is_in_cell::int)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D, --eq 38
		sum(w_residuals_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_res_D_plus -- eq 39
	FROM w_ldsity_plot AS w_ldsity_plot
	INNER JOIN w_residuals_plot AS w_residuals_plot ON w_ldsity_plot.gid = w_residuals_plot.plot AND w_ldsity_plot.attribute = w_residuals_plot.attribute and w_ldsity_plot.conf_id = w_residuals_plot.conf_id
	GROUP BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_ldsity_plot.plots_per_cluster, w_residuals_plot.attribute
	ORDER BY stratum, w_ldsity_plot.conf_id, w_ldsity_plot.cluster, w_residuals_plot.attribute
)
, w_ldsity_residuals_cluster as (
	select 
		w_ldsity_cluster.conf_id, w_ldsity_cluster.gid, w_ldsity_cluster.cluster, 
		w_ldsity_cluster.attribute, w_ldsity_cluster.stratum, w_ldsity_cluster.plots_per_cluster, 
		w_ldsity_cluster.plcount, w_ldsity_cluster.cluster_is_in_cell, w_ldsity_cluster.ldsity_d, w_ldsity_cluster.ldsity_d_plus, 
		w_ldsity_cluster.is_aux, w_ldsity_cluster.is_target, w_ldsity_cluster.geom,
		w_residuals_cluster.ldsity_res_D, w_residuals_cluster.ldsity_res_D_plus
	from w_ldsity_cluster
	LEFT JOIN w_residuals_cluster USING (conf_id, cluster, attribute, stratum)
)
, w_2p_data as (
	with w_ldsity_residuals_cluster as (select * from w_ldsity_residuals_cluster)
	select 
		w_ldsity_residuals_cluster.conf_id, w_ldsity_residuals_cluster.gid, w_ldsity_residuals_cluster.cluster, 
		w_ldsity_residuals_cluster.attribute, w_ldsity_residuals_cluster.stratum, w_ldsity_residuals_cluster.plots_per_cluster, 
		w_ldsity_residuals_cluster.plcount, w_ldsity_residuals_cluster.cluster_is_in_cell, w_ldsity_residuals_cluster.ldsity_d, 
		w_ldsity_residuals_cluster.ldsity_d_plus, 
		w_ldsity_residuals_cluster.is_aux, w_ldsity_residuals_cluster.is_target, w_ldsity_residuals_cluster.geom,
		w_ldsity_residuals_cluster.ldsity_res_D, w_ldsity_residuals_cluster.ldsity_res_D_plus,
		w_pix.pix, w_pix.sweight, w_DELTA_G_beta.val as DELTA_T__G_beta, 
		w_strata_sum.nb_sampling_units::integer, w_strata_sum.sweight_strata_sum, w_strata_sum.lambda_d_plus
	from w_ldsity_residuals_cluster
	INNER JOIN w_pix USING (conf_id, cluster, stratum)
	INNER JOIN w_DELTA_G_beta ON (w_pix.conf_id = w_DELTA_G_beta.conf_id and cluster = w_DELTA_G_beta.c)
	inner join w_strata_sum on (stratum = f_a_sampling_stratum_gid)
)
, w_2p_data_sigma as (
	select 
		w_2p_data.gid, w_2p_data.cluster, w_2p_data.attribute, w_2p_data.stratum, w_2p_data.plots_per_cluster, w_2p_data.plcount,
		w_2p_data.cluster_is_in_cell, w_2p_data.ldsity_d, w_2p_data.ldsity_d_plus, w_2p_data.is_aux, w_2p_data.is_target,
		w_2p_data.geom, w_2p_data.ldsity_res_d, w_2p_data.ldsity_res_d_plus, w_2p_data.pix, w_2p_data.sweight, w_2p_data.DELTA_T__G_beta,
		w_2p_data.nb_sampling_units, w_2p_data.sweight_strata_sum, w_2p_data.lambda_d_plus, w_configuration.sigma
	from w_2p_data 
	inner join w_configuration on (w_2p_data.conf_id = w_configuration.id)
)
select * from w_2p_data_sigma;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

COMMENT ON FUNCTION @extschema@.fn_2p_data(integer) IS 'Function preparing data for regression estimate.';

-- </function>

