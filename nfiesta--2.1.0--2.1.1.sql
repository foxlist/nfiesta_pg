--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

COMMENT ON COLUMN @extschema@.t_result.point IS 'Point estimate.';
COMMENT ON COLUMN @extschema@.t_result.var IS 'Estimate of variability.';
COMMENT ON COLUMN @extschema@.t_result.calc_duration IS 'Time necessary for calculation.';
COMMENT ON COLUMN @extschema@.t_result.calc_started IS 'Time when calculation started.';

ALTER TYPE @extschema@.estimate_result ADD ATTRIBUTE est_info json;

alter table @extschema@.t_result add column sampling_units json;
COMMENT ON COLUMN @extschema@.t_result.sampling_units IS 'Number of sampling units used for estimate computation.';

DROP FUNCTION @extschema@.fn_1p_total_var(integer);
-- <function_name="fn_1p_total_var" function_schema="extschema" src="functions/extschema/fn_1p_total_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
	, w_data_agg AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).*
		from w_data_agg
	)
	, w_est1p AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p
		from w_est1p_stratum group by attribute order by attribute
	)
	, w_units_json as (
		select array_to_json(array_agg(row_to_json(t))) as s_units from
		(
			select
				stratum, attribute,
				count(*) as s_units_param_area, 
				sum(cluster_is_in_cell::int) as s_units_cell,
				sum((ldsity_d != 0)::int) as s_units_cell_nonzero
			from w_data
			group by stratum, attribute
			order by stratum, attribute
		) as t 
	)	
	select 
	w_est1p.attribute, 
	point1p,  var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	s_units
	from w_est1p
	, w_units_json
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS 'Function computing total and variance using regression estimate.';

-- </function>
DROP FUNCTION @extschema@.fn_2p_total_var(integer);
-- <function_name="fn_2p_total_var" function_schema="extschema" src="functions/extschema/fn_2p_total_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_2p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_2p_data(' || conf_id || ') where is_target
	)
	, w_I AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data
                order by r, c
	)
	, w_SIGMA AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data
		order by r, c
	)
	, w_PI AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data
                order by r, c
	)
	, w_SIGMA_PI AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA as A inner join w_PI as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data
                order by r, c
	)
	, w_total_1p AS (
                with w_I_PI AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I      AS A 
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI as A, w_Y_T as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data
	)
	, w_correction_2p AS (
                with w_DELTA_T__G_beta_PI AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta AS A
                        inner join w_PI     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI as A, w_Y_T as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p 		AS A
                inner join  w_correction_2p    	AS B    on (A.c = B.c)
		order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESIDUALS----------------------------------
-------------------------------------------------------------------------------
	, w_e AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data
                order by r, c
	)
	, w_I_e AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta as A, w_e as B
                WHERE
                        A.c = B.c
	)
	, w_PHI AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e        		AS A
                inner join  w_DELTA_T__G_beta__e  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
	, w_zeroResidualsTotalTest AS (
                select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                        w_SIGMA_PI as A, (select c as r, r as c, val_d_plus from w_e) as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
, w_data_phi AS (
	select w_data.*, w_PHI.val as phi from w_data inner join w_PHI on (cluster = c and attribute = r)
) 
, w_data_agg AS        (
        SELECT
		stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
		array_agg(cluster order by cluster) as cids,
		array_agg(ldsity_d order by cluster) as ldsitys,
		array_agg(phi order by cluster) as residuals,
		array_agg(sweight order by cluster) as sweights
        FROM
            w_data_phi
        group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est1p AS    (
	SELECT
	    attribute, sum(total) as point1p, sum(var) AS var1p 
	from w_est1p_stratum group by attribute order by attribute
)
, w_est2p_stratum AS    (
        SELECT 
		stratum, attribute, (htc_compute(cids, residuals, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
        from w_data_agg
)
, w_est2p AS    (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	from w_est2p_stratum group by attribute order by attribute
)	
, w_units_json as (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		select
			stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data
		group by stratum, attribute
		order by stratum, attribute
	) as t 
)	
select 
	w_est1p.attribute, 
	point1p,  var1p,
	val as point2p, var2p,
	s_units
from w_est1p
inner join w_est2p on (w_est1p.attribute = w_est2p.attribute) 
inner join w_total_2p on (w_est2p.attribute = w_total_2p.c),
w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p_total_var(integer) IS 'Function computing total and variance using regression estimate.';

-- </function>
DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);
-- <function_name="fn_1p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p1p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_units_json as (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		(select
			''nominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		union all
		(select
			''denominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
	) as t
)	
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p,
	s_units
from w_1p_ratio_var
, w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>
DROP FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer);
-- <function_name="fn_1p2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p2p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p2p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_1p2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_1p2pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.phi * w_ratio_1p2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p2p on (nom.attribute = w_ratio_1p2p.c)
	group by stratum, w_ratio_1p2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1p2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1p2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est1p2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p2p 
	    from w_est1p2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_1p2p_ratio_var AS (
	select 
            w_est1p2pr.attribute, 
	    w_ratio_1p2p.val as point1p2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1p2pr.var1p2p end as var1p2p
	    from     	w_est1p2pr 
	    inner join 	w_ratio_1p2p on (w_est1p2pr.attribute = w_ratio_1p2p.c)
)
, w_units_json as (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		(select
			''nominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		union all
		(select
			''denominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
	) as t
)	
select 
	attribute, 
	point1p, var1p,
	point1p2p, var1p2p, 
	s_units
from w_1p2p_ratio_var inner join w_1p_ratio_var using (attribute)
, w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>
DROP FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer);
-- <function_name="fn_2p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p1p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_ratio_2p1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2p1pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.ldsity_d * w_ratio_2p1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom) as denom using (cluster)
            inner join w_ratio_2p1p on (nom.attribute = w_ratio_2p1p.c)
	group by stratum, w_ratio_2p1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2p1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2p1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2p1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p1p 
	    from w_est2p1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p1p_ratio_var AS (
	select 
            w_est2p1pr.attribute, 
	    w_ratio_2p1p.val as point2p1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2p1pr.var2p1p end as var2p1p
	    from     	w_est2p1pr 
	    inner join 	w_ratio_2p1p on (w_est2p1pr.attribute = w_ratio_2p1p.c)
)
, w_units_json as (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		(select
			''nominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		union all
		(select
			''denominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
	) as t
)	
select 
	attribute, 
	point1p, var1p,
	point2p1p, var2p1p, 
	s_units
from w_2p1p_ratio_var inner join w_1p_ratio_var using (attribute)
, w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>
DROP FUNCTION @extschema@.fn_2p2p_ratio_var(integer, integer);
-- <function_name="fn_2p2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p2p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p2p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_2p2p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision,
	est_info json
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
        (select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.phi * w_ratio_2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_2p on (nom.attribute = w_ratio_2p.c)
	group by stratum, w_ratio_2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p 
	    from w_est2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p_ratio_var AS (
	select 
            w_est2pr.attribute, 
	    w_ratio_2p.val as point2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2pr.var2p end as var2p
	    from     	w_est2pr 
	    inner join 	w_ratio_2p on (w_est2pr.attribute = w_ratio_2p.c)
)
, w_units_json as (
	select array_to_json(array_agg(row_to_json(t))) as s_units from
	(
		(select
			''nominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__nom
		group by stratum, attribute
		order by stratum, attribute)
		union all
		(select
			''denominator'' as nomdenom, stratum, attribute,
			count(*) as s_units_param_area, 
			sum(cluster_is_in_cell::int) as s_units_cell,
			sum((ldsity_d != 0)::int) as s_units_cell_nonzero
		from w_data__denom
		group by stratum, attribute
		order by stratum, attribute)
	) as t
)	
select 
	attribute, 
	point1p, var1p,
	point2p, var2p, 
	s_units
from w_2p_ratio_var inner join w_1p_ratio_var using (attribute)
, w_units_json
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>
