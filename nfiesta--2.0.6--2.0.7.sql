--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE TYPE @extschema@.estimate_result AS (
	point	double precision,
	var	double precision
);

alter table @extschema@.t_result rename COLUMN time_of_calc TO calc_started;
alter table @extschema@.t_result add COLUMN calc_duration interval;

-------------------------------------------views
-- <view_name="v_conf_overview" view_schema="extschema" src="views/extschema/v_conf_overview.sql">
--drop view if exists @extschema@.v_conf_overview;
create or replace view @extschema@.v_conf_overview as (
	select
		t_estimate_conf.id as conf_id,
		t_total_estimate_conf.id as tec_id,
		ted_second_ph.id as ted_secph_id,
		ted_zero_ph.id as ted_zerph_id,
		t_aux_conf.id as tac_id,
		t_total_estimate_conf_denom.id as tec_d_id,
		ted_second_ph_denom.id as ted_secph_d_id,
		ted_zero_ph_denom.id as ted_zerph_d_id,
		t_aux_conf_denom.id as tac_d_id,
		case 	when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null 
			then '1p_total'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is null 
			then '1p_ratio'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null and
				t_aux_conf.sigma = false
			then '2p_total_no_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is null and 
				ted_zero_ph_denom.id is null and
				t_aux_conf.sigma = true
			then '2p_total_with_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is not null and 
				t_aux_conf.sigma = false
			then '2p_ratio_no_sigma'
			when 
				ted_second_ph.id is not null and 
				ted_zero_ph.id is not null and 
				ted_second_ph_denom.id is not null and 
				ted_zero_ph_denom.id is not null and
				t_aux_conf.sigma = true
			then '2p_ratio_with_sigma'
			else 'unknown'
		end as estimate_type_str,
------------------------additional info begin-----------------------------------
		t_aux_conf.param_area, f_a_param_area.param_area_code,
		t_aux_conf.model, t_model.description as model_description,
		t_total_estimate_conf.estimation_cell, c_estimation_cell.label as estimation_cell_label, --f_a_cell.geom,
		t_variable.target_variable, c_target_variable.label as target_variable_label, 
		t_variable.sub_population_category, c_sub_population_category.label as sub_population_category_label
------------------------additional info end-------------------------------------
	from @extschema@.t_estimate_conf
        inner join @extschema@.t_total_estimate_conf ON t_total_estimate_conf.id = t_estimate_conf.total_estimate_conf
        inner join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_second_ph ON ted_second_ph.total_estimate_conf = t_total_estimate_conf.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_zero_ph ON ted_zero_ph.total_estimate_conf = t_total_estimate_conf.id
	left join @extschema@.t_aux_conf ON t_aux_conf.id = ted_zero_ph.aux_conf
        left join @extschema@.t_total_estimate_conf as t_total_estimate_conf_denom ON t_total_estimate_conf_denom.id = t_estimate_conf.denominator
        left join (select * from @extschema@.t_total_estimate_data where phase = 2) as ted_second_ph_denom ON ted_second_ph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
        left join (select * from @extschema@.t_total_estimate_data where phase = 0) as ted_zero_ph_denom ON ted_zero_ph_denom.total_estimate_conf = t_total_estimate_conf_denom.id
	left join @extschema@.t_aux_conf as t_aux_conf_denom ON t_aux_conf_denom.id = ted_zero_ph_denom.aux_conf
------------------------additional info begin-----------------------------------
	left join @extschema@.f_a_param_area on (t_aux_conf.param_area = f_a_param_area.gid)
	left join @extschema@.t_model on (t_aux_conf.model = t_model.id)
	inner join @extschema@.f_a_cell on (t_total_estimate_conf.estimation_cell = f_a_cell.gid)
	inner join @extschema@.c_estimation_cell on (f_a_cell.estimation_cell = c_estimation_cell.id)
	inner join @extschema@.t_variable on (ted_second_ph.variable = t_variable.id)
	inner join @extschema@.c_target_variable on (t_variable.target_variable = c_target_variable.id)
	left join @extschema@.c_sub_population_category on (t_variable.sub_population_category = c_sub_population_category.id)
------------------------additional info end-------------------------------------
	order by t_estimate_conf.id
);
--select * from @extschema@.v_conf_overview;

-- </view>
