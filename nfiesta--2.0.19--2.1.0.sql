--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function_name="fn_1p_total_var" function_schema="extschema" src="functions/extschema/fn_1p_total_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_total_var(integer)

-- DROP FUNCTION @extschema@.fn_1p_total_var(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p_total_var(
    IN conf_id integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p TOTAL-----------------------------------
-------------------------------------------------------------------------------

	with w_data as (
		select * from @extschema@.fn_1p_data(' || conf_id || ') where is_target
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------

------------------------------------2p
------------------------------------USING point SQL


------------------------------------2p var using HTC implemented in C function
--DROP EXTENSION htc; CREATE EXTENSION htc;
	, w_data_agg AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg
	)
	, w_est1p AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum group by attribute order by attribute
	)	
	select 
	w_est1p.attribute, 
	point1p,  var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
	from w_est1p;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p_total_var(integer) IS 'Function computing total and variance using regression estimate.';

-- </function>

-- <function_name="fn_1p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p1p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)	
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
        (select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
select 
	attribute, 
	point1p, var1p,
	NULL::double precision as point2p, NULL::double precision as var2p 
from w_1p_ratio_var
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_1p2p_ratio_var" function_schema="extschema" src="functions/extschema/fn_1p2p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p2p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_1p2p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_1p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__nom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__nom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__nom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__nom 
	)
	, w_est1p__nom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__nom  group by attribute order by attribute
	)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_2p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__denom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__denom
                order by r, c
	)
	, w_SIGMA__denom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__denom
		order by r, c
	)
	, w_PI__denom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__denom
                order by r, c
	)
	, w_SIGMA_PI__denom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__denom as A inner join w_PI__denom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__denom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__denom
                order by r, c
	)
	, w_total_1p__denom AS (
                with w_I_PI__denom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__denom      AS A 
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__denom as A, w_Y_T__denom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__denom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__denom
	)
	, w_correction_2p__denom AS (
                with w_DELTA_T__G_beta_PI__denom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__denom AS A
                        inner join w_PI__denom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__denom as A, w_Y_T__denom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__denom 		AS A
                inner join  w_correction_2p__denom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__denom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__denom
                order by r, c
	)
	, w_I_e__denom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__denom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__denom as A, w_e__denom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__denom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__denom        		AS A
                inner join  w_DELTA_T__G_beta__e__denom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__denom AS (
	select w_data__denom.*, w_PHI__denom.val as phi from w_data__denom inner join w_PHI__denom on (cluster = c and attribute = r)
)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_1p__denom) as denom
)
, w_ratio_1p2p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select attribute, point1p::double precision as total from w_est1p__nom) as nom,
	(select c as attribute, val::double precision as total from w_total_2p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d, phi from w_data_phi__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_1p2pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p2p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.phi * w_ratio_1p2p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data__nom order by cluster) as nom
            inner join (select cluster, ldsity_d_plus, phi from w_data_phi__denom order by cluster) as denom using (cluster)
            inner join w_ratio_1p2p on (nom.attribute = w_ratio_1p2p.c)
	group by stratum, w_ratio_1p2p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est1p2pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1p2pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est1p2pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p2p 
	    from w_est1p2pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_1p2p_ratio_var AS (
	select 
            w_est1p2pr.attribute, 
	    w_ratio_1p2p.val as point1p2p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1p2pr.var1p2p end as var1p2p
	    from     	w_est1p2pr 
	    inner join 	w_ratio_1p2p on (w_est1p2pr.attribute = w_ratio_1p2p.c)
)
select 
	attribute, 
	point1p, var1p,
	point1p2p, var1p2p 
from w_1p2p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_1p2p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_2p1p_ratio_var" function_schema="extschema" src="functions/extschema/fn_2p1p_ratio_var.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p1p_ratio_var(integer, integer)

-- DROP FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_2p1p_ratio_var(
    IN conf_id integer,
    IN conf_id__denom integer
)
  RETURNS TABLE(
	attribute integer,
	point1p double precision,
	var1p double precision,
	point2p double precision,
	var2p double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
-------------------------------------------------------------------------------
------------------------------------2p RATIO-----------------------------------
-------------------------------------------------------------------------------
	with w_conf as (
            select nom.id as nom_conf_id, denom.id as denom_conf_id 
            from (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id || ') as nom 
            , (select * from @extschema@.t_total_estimate_conf where id = ' || conf_id__denom || ') as denom 
        )
------------------------------------nominator-----------------------------------
        , w_data__nom as (
		select * from (select (@extschema@.fn_2p_data(nom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_I__nom AS (
	        SELECT distinct
                        1 AS r,
                        cluster AS c,
                        cluster_is_in_cell::int AS val
                FROM    w_data__nom
                order by r, c
	)
	, w_SIGMA__nom AS (
		SELECT distinct
			1 as r,
			cluster as c,
			case when sigma then (plots_per_cluster^2)::float / plcount::float 
			else 1::float end
			as val
		from	w_data__nom
		order by r, c
	)
	, w_PI__nom AS (
                SELECT distinct
                        1 as r,
                        cluster as c,
                        1.0 / pix as val
                from w_data__nom
                order by r, c
	)
	, w_SIGMA_PI__nom AS (
		SELECT
			1 as r,
			A.c as c,
			A.val * B.val as val
		from w_SIGMA__nom as A inner join w_PI__nom as B on (A.c = B.c) 
		order by r, c
	)
	, w_Y_T__nom AS (
                SELECT
                        cluster as r,
                        attribute as c,
                        ldsity_d_plus as val_d_plus,
                        ldsity_d as val_d -- used for 1p estimete
                from w_data__nom
                order by r, c
	)
	, w_total_1p__nom AS (
                with w_I_PI__nom AS (
                    select 
                        A.r, 
                        A.c AS c,
                        A.val * B.val as val
                        from 
                                   w_I__nom      AS A 
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
                select
                        A.r,
                        B.c, 
                        sum(A.val * B.val_d) as val
                FROM 
                         w_I_PI__nom as A, w_Y_T__nom as B
                WHERE 
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_DELTA_T__G_beta__nom AS (
			select distinct
			1 AS r,
			cluster as c,
			delta_t__g_beta as val
			from w_data__nom
	)
	, w_correction_2p__nom AS (
                with w_DELTA_T__G_beta_PI__nom AS (
                    select
                        A.r,
                        A.c AS c,
                        A.val * B.val as val
                        from
                                   w_DELTA_T__G_beta__nom AS A
                        inner join w_PI__nom     AS B        on (A.c = B.c)
                        order by r, c
                )
		select
                        A.r,
                        B.c,
                        sum(A.val * B.val_d_plus) as val
                FROM
                         w_DELTA_T__G_beta_PI__nom as A, w_Y_T__nom as B
                WHERE
                        A.c = B.r
                GROUP BY
                        A.r, B.c
                order by r, c
	)
	, w_total_2p__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_total_1p__nom 		AS A
                inner join  w_correction_2p__nom    	AS B    on (A.c = B.c)
		order by r, c
	)
        ------------------------------------RESIDUALS----------------------------------
	, w_e__nom AS (
                SELECT
                        attribute as r,
                        cluster as c,
                        ldsity_res_d_plus as val_d_plus,
                        ldsity_res_d as val_d
                from w_data__nom
                order by r, c
	)
	, w_I_e__nom AS (

                select
                        B.r,
                        B.c,
                        A.val * B.val_D as val -- element-wise product
                FROM
                        w_I__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_DELTA_T__G_beta__e__nom AS (
                select
                        B.r,
                        B.c,
                        A.val * B.val_D_plus as val -- element-wise product
                FROM
                        w_DELTA_T__G_beta__nom as A, w_e__nom as B
                WHERE
                        A.c = B.c
	)
	, w_PHI__nom AS (
            select
                A.r,
                A.c AS c,
                A.val + B.val as val
                from
                            w_I_e__nom        		AS A
                inner join  w_DELTA_T__G_beta__e__nom  	AS B     on (A.r = B.r and A.c = B.c)
		order by r, c
        )
, w_data_phi__nom AS (
	select w_data__nom.*, w_PHI__nom.val as phi from w_data__nom inner join w_PHI__nom on (cluster = c and attribute = r)
)
------------------------------------denominator-----------------------------------
        , w_data__denom as (
		select * from (select (@extschema@.fn_1p_data(denom_conf_id)).* from w_conf) as alldata where is_target
	)
	, w_data_agg__denom  AS (
		SELECT
			stratum, attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
			array_agg(cluster order by cluster) as cids,
			array_agg(ldsity_d order by cluster) as ldsitys,
			array_agg(sweight order by cluster) as sweights
		FROM
			w_data__denom 
			group by stratum, attribute, nb_sampling_units, sweight_strata_sum, buffered_area_ha 
			order by attribute
	)
	, w_est1p_stratum__denom  AS (
		SELECT 
			stratum, attribute, 
			(htc_compute(cids, ldsitys, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
		from w_data_agg__denom 
	)
	, w_est1p__denom  AS    (
		SELECT
			attribute, sum(total) as point1p, sum(var) AS var1p 
		from w_est1p_stratum__denom  group by attribute order by attribute
	)
-------------------------------------------------------------------------------
------------------------------------RESULTS------------------------------------
-------------------------------------------------------------------------------
------------------------------------2p var using HTC implemented in C function
, w_ratio_1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_1p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_ratio_2p1p AS (
        SELECT nom.attribute as c, 
            case when denom.total = 0 then NULL::double precision else nom.total / denom.total end as val,
            denom.total as denom_total from 
        (select c as attribute, val::double precision as total from w_total_2p__nom) as nom,
	(select attribute, point1p::double precision as total from w_est1p__denom) as denom
)
, w_data_1pr_agg AS        (
        SELECT
	    stratum, w_ratio_1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.ldsity_d - denom.ldsity_d * w_ratio_1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom) as denom using (cluster)
            inner join w_ratio_1p on (nom.attribute = w_ratio_1p.c)
	group by stratum, w_ratio_1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_data_2p1pr_agg AS        (
        SELECT
	    stratum, w_ratio_2p1p.c as attribute, nb_sampling_units, sweight_strata_sum, lambda_d_plus as buffered_area_ha,
            array_agg(cluster order by cluster) as cids,
            array_agg(nom.phi - denom.ldsity_d * w_ratio_2p1p.val order by cluster) as z,
            array_agg(sweight order by cluster) as sweights
        FROM
            (select * from w_data_phi__nom order by cluster) as nom
            inner join (select cluster, ldsity_d from w_data__denom) as denom using (cluster)
            inner join w_ratio_2p1p on (nom.attribute = w_ratio_2p1p.c)
	group by stratum, w_ratio_2p1p.c, nb_sampling_units, sweight_strata_sum, buffered_area_ha order by attribute
)
, w_est1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_1pr_agg
)
, w_est2p1pr_stratum AS (
	SELECT 
            stratum, attribute, (htc_compute(cids, z, sweights, buffered_area_ha, sweight_strata_sum, nb_sampling_units)).* 
	    from w_data_2p1pr_agg
)
, w_est1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var1p 
	    from w_est1pr_stratum group by attribute order by attribute
)
, w_est2p1pr AS (
	SELECT
	    attribute, sum(total) as point_residual, sum(var) AS var2p1p 
	    from w_est2p1pr_stratum group by attribute order by attribute
)
, w_1p_ratio_var AS (
	select 
            w_est1pr.attribute, 
	    w_ratio_1p.val as point1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est1pr.var1p end as var1p
	    from     	w_est1pr 
	    inner join 	w_ratio_1p on (w_est1pr.attribute = w_ratio_1p.c)
)
, w_2p1p_ratio_var AS (
	select 
            w_est2p1pr.attribute, 
	    w_ratio_2p1p.val as point2p1p, 
            case when denom_total = 0 then NULL::double precision else (1.0 / denom_total^2) * w_est2p1pr.var2p1p end as var2p1p
	    from     	w_est2p1pr 
	    inner join 	w_ratio_2p1p on (w_est2p1pr.attribute = w_ratio_2p1p.c)
)
select 
	attribute, 
	point1p, var1p,
	point2p1p, var2p1p 
from w_2p1p_ratio_var inner join w_1p_ratio_var using (attribute)
;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
;

COMMENT ON FUNCTION @extschema@.fn_2p1p_ratio_var(integer, integer) IS 'Function computing ratio and variance using regression estimate.';

-- </function>

-- <function_name="fn_g_beta" function_schema="extschema" src="functions/extschema/fn_g_beta.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_G_beta(integer)

-- DROP FUNCTION @extschema@.fn_G_beta(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_G_beta(
    IN conf_id integer
)
  RETURNS TABLE(
	r integer,
	c integer,
	val double precision
) AS
$BODY$
DECLARE
	_complete_query text;
BEGIN
	--------------------------------QUERY--------------------------------
	_complete_query := '
WITH
w_configuration AS (
	select 
		t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_aux_conf.sigma, t_model.description,
		array_agg(t_model_variables.variable order by t_model_variables.variable) as aux_attributes
	from @extschema@.t_aux_conf 
	inner join @extschema@.t_model ON t_model.id = t_aux_conf.model
	inner join @extschema@.t_model_variables ON t_model_variables.model = t_model.id
        where t_aux_conf.id = ' || conf_id || '
	group by t_aux_conf.id, t_aux_conf.param_area, t_aux_conf.model, t_model.description
)
, w_param_area_selection AS (
	SELECT
		w_configuration.id as conf_id,
		gid as param_area_gid,
		geom
	FROM @extschema@.f_a_param_area
	INNER JOIN w_configuration ON (w_configuration.param_area = f_a_param_area.gid)
)
, w_plot AS (-------------------------LIST OF PLOTS IN PARAMETRIZATION AREA
	select distinct 
		w_param_area_selection.conf_id, f_p_plot.gid, t_cluster.id as cluster, t_panel.stratum,
		t_panel2aux_conf.panel, 
		f_p_plot.geom
	from w_param_area_selection 
	inner join @extschema@.t_panel2aux_conf on w_param_area_selection.conf_id = t_panel2aux_conf.aux_conf
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join @extschema@.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
	inner join @extschema@.f_p_plot ON f_p_plot."cluster" = t_cluster.id
	inner join @extschema@.cm_plot2param_area_mapping ON cm_plot2param_area_mapping.plot = f_p_plot.gid
	inner join @extschema@.f_a_param_area ON (f_a_param_area.gid = cm_plot2param_area_mapping.param_area and w_param_area_selection.param_area_gid = f_a_param_area.gid)
)
, w_ldsity_plot AS (
	SELECT
		w_plot.conf_id,
		w_plot.gid,
		w_plot.stratum,
                w_plot.panel,
		w_plot.cluster,
		t_variable.id as attribute,
		t_cluster_configuration.plots_per_cluster,
		t_auxiliary_data.value as ldsity,
		w_plot.geom,
		true as is_aux
	FROM w_plot
	inner join @extschema@.cm_plot2cluster_config_mapping on w_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join @extschema@.t_cluster_configuration on cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_auxiliary_data on w_plot.gid = t_auxiliary_data.plot
	inner join @extschema@.t_variable on t_auxiliary_data.auxiliary_variable_category = t_variable.auxiliary_variable_category
	inner join w_configuration on w_plot.conf_id = w_configuration.id
	WHERE t_variable.id = ANY (w_configuration.aux_attributes)
)
, w_ldsity_cluster AS (
 	SELECT
		w_ldsity_plot.conf_id,
		concat(w_ldsity_plot.conf_id, ROW_NUMBER() OVER ())::bigint as gid, 
		w_ldsity_plot.cluster,
		w_ldsity_plot.attribute,
		w_ldsity_plot.stratum,
                w_ldsity_plot.panel,
		w_ldsity_plot.plots_per_cluster,
		count(*) as plcount,
		sum(w_ldsity_plot.ldsity)/w_ldsity_plot.plots_per_cluster AS ldsity_D_plus, -- eq 15,
		w_ldsity_plot.is_aux, --
		st_collect(w_ldsity_plot.geom)::geometry(MultiPoint) as geom
	FROM       w_ldsity_plot
	GROUP BY conf_id, stratum, panel, cluster, plots_per_cluster, attribute, is_aux
	ORDER BY stratum, cluster, attribute
)
, w_X AS ( -------------------------AUX LOCAL DENSITY ON TRACT LEVEL
	SELECT
		conf_id,
		attribute AS r,
		cluster AS c,
		ldsity_D_plus as val
	FROM w_ldsity_cluster
	WHERE is_aux
)
, w_clusters AS (-------------------------LIST OF TRACTS
	select distinct conf_id, stratum, panel, cluster from w_ldsity_cluster
)
, w_strata_sum as (
	select
		w_configuration.id as conf_id,
                t_stratum.id as f_a_sampling_stratum_gid, coalesce(buffered_area_m2, area_m2)/10000 as lambda_d_plus, plots_per_cluster,
		count(*) as nb_sampling_units, sum (sampling_weight) as sweight_strata_sum
	from w_configuration
	inner join @extschema@.t_aux_conf on t_aux_conf.id = w_configuration.id
	inner join @extschema@.t_panel2aux_conf ON t_panel2aux_conf.aux_conf = t_aux_conf.id
	inner join @extschema@.t_panel ON t_panel.id = t_panel2aux_conf.panel
	inner join @extschema@.t_cluster_configuration ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join @extschema@.t_stratum ON t_stratum.id = t_panel.stratum
	inner join @extschema@.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	group by w_configuration.id, f_a_sampling_stratum_gid, lambda_d_plus, plots_per_cluster
)
, w_pix AS (-------------------------INCLUSION DENSITY PIX
	SELECT
		w_clusters.conf_id,
		w_clusters.stratum,
		w_clusters.cluster,
		v_strata_sum.sweight_strata_sum / (v_strata_sum.lambda_d_plus * cm_cluster2panel_mapping.sampling_weight) as pix
	FROM w_clusters
        INNER JOIN @extschema@.cm_cluster2panel_mapping ON (w_clusters.cluster = cm_cluster2panel_mapping.cluster
								and w_clusters.panel = cm_cluster2panel_mapping.panel)
	INNER JOIN w_strata_sum as v_strata_sum ON w_clusters.stratum = v_strata_sum.f_a_sampling_stratum_gid
)
, w_SIGMA AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		case when w_configuration.sigma then (plots_per_cluster^2)::float / plcount::float 
		else 1::float end
		as val
	from (select conf_id, cluster, plots_per_cluster, plcount 
		from w_ldsity_cluster 
		group by conf_id, cluster, plots_per_cluster, plcount) as m
	inner join w_configuration on (m.conf_id = w_configuration.id)
	order by r, c
)
, w_PI AS (
	SELECT
		conf_id,
		1 as r,
		cluster as c,
		1.0 / pix as val
	from w_pix
	order by r, c
)
, w_SIGMA_PI AS (
	SELECT
		A.conf_id,
		1 as r,
		A.c as c,
		A.val * B.val as val
	from w_SIGMA as A inner join w_PI as B on (A.c = B.c and A.conf_id = B.conf_id) 
	order by r, c
)
, w_XT AS (
	SELECT
		conf_id,
		c as r,
		r as c,
		val as val
	from w_X
	order by r, c
)
, w_X_SIGMA_PI AS ( -- element-wise multiplication
	select 
		A.conf_id,
		A.r, 
		A.c,
		A.val * B.val as val
	from w_X as A
	inner join w_SIGMA_PI as B on (A.c = B.c and A.conf_id = B.conf_id)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT AS ( -- matrix multiplication
	SELECT 
		A.conf_id,
		ROW_NUMBER() OVER (partition by A.conf_id order by A.r, B.c) AS mid,
		A.r, 
		B.c, 
		sum(A.val * B.val) as val
	FROM w_X_SIGMA_PI as A, w_XT as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_agg AS (
	select conf_id, r, array_agg(val order by c) as val from w_X_SIGMA_PI_XT group by conf_id, r
)
, w_aggagg AS (
	select conf_id, array_agg(val order by r) as val from w_agg group by conf_id
)
, w_inv AS (
	select conf_id, @extschema@.fn_inverse(val) AS val from w_aggagg
)
, w_inv_id AS (
	select conf_id, mid, invval from w_inv, unnest(w_inv.val) WITH ORDINALITY AS t(invval, mid)
)
, w_X_SIGMA_PI_XT_inv AS (
	SELECT 
		conf_id, r, c, invval as val 
	FROM w_X_SIGMA_PI_XT 
	inner join w_inv_id using (conf_id, mid)
	ORDER BY r, c
)
, w_X_SIGMA_PI_XT_inv_X AS (
	select
		A.conf_id,
		A.r,
		B.c,
		sum(A.val * B.val) as val
	FROM
		w_X_SIGMA_PI_XT_inv as A, w_X as B
	WHERE A.c = B.r and A.conf_id = B.conf_id
	GROUP BY A.r, B.c, A.conf_id
	ORDER BY r, c
)
, w_G_beta AS (
	select
		A.conf_id,
		A.r,
		A.c,
		A.val * B.val as val
	FROM
		w_X_SIGMA_PI_XT_inv_X as A, w_SIGMA as B
	WHERE A.c = B.c and A.conf_id = B.conf_id
	ORDER BY r, c
)
select r, c, val from w_G_beta;
        ';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
;

COMMENT ON FUNCTION @extschema@.fn_G_beta(integer) IS 'Function computing matrix G_beta used for regression estimators. G_beta is dependent on model and parametrization domain (not cell). Matrix is represented by r(ow) and c(column) indices.';

-- </function>
