--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
/*
	unique index and configuration function 
 */

CREATE UNIQUE INDEX uidx__t_variable__all_columns ON @extschema@.t_variable (coalesce(target_variable,0), coalesce(area_domain_category,0), coalesce(sub_population_category,0), coalesce(auxiliary_variable_category,0));

-- for now, parametrization is not distinguished here
CREATE UNIQUE INDEX uidx__t_total_estimate_conf__all_columns ON @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, target_variable, phase_estimate_type)
WHERE phase_estimate_type = 1;
-- test data dont contain estimation-cell within varchar total_estimate_conf
--ALTER TABLE @extschema@.t_total_estimate_conf ADD CONSTRAINT ukey__t_total_estimate_conf__total_estimate_conf UNIQUE (total_estimate_conf);

ALTER TABLE @extschema@.t_total_estimate_conf ALTER COLUMN target_variable SET NOT NULL; 
ALTER TABLE @extschema@.t_total_estimate_conf ALTER COLUMN phase_estimate_type SET NOT NULL; 

CREATE UNIQUE INDEX  ukey__t_estimate_conf__all_columns ON @extschema@.t_estimate_conf (total_estimate_conf, coalesce(denominator,0));



-- <function name="fn_1p_est_configuration" schema="extschema" src="functions/extschema/fn_1p_est_configuration.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_1p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_1p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_1p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _phase_estimate_type integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_total_estimate_data_2nd	integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_target_label			varchar;
_cell				varchar;
BEGIN
_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type)
VALUES
	($1, $2, $3, concat('1p;T=',_target_label,';Cell=',_cell,_note), $5, 1)
RETURNING id
INTO _total_estimate_conf;

-- insert into table t_total_estimate_data
INSERT INTO @extschema@.t_total_estimate_data(total_estimate_conf, variable, phase, aux_phase_type, cor_phase, aux_conf)
VALUES
	(_total_estimate_conf, $5, 2, NULL, NULL, NULL)
RETURNING id
INTO _total_estimate_data_2nd;


-- test on cell coverage

	-- which stratas covers the cell (fully?)
	SELECT
		array_agg(t1.id ORDER BY t1.id)
	FROM
		@extschema@.t_stratum AS t1
	INNER JOIN
		@extschema@.f_a_cell AS t2
	ON
		-- buffered stratum?
		-- no, if only buffer of the stratum would intersect the cell, 
		-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
		ST_Intersects(t1.geom, t2.geom)
	WHERE
		t2.estimation_cell = $1
	INTO _stratas;

	-- panels with target variable in specified stratas (panels are the ones with the less granularity, hence 1 stratum can have e.g. 4 panels which together results in 1 big panel)

	WITH w_data AS (
		SELECT
			t1.id AS stratum, t2.id AS panel, t6.reference_year_set, count(*) AS total
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.t_panel AS t2
		ON
			t1.id = t2.stratum
		INNER JOIN
			@extschema@.cm_cluster2panel_mapping AS t3
		ON
			t2.id = t3.panel
		INNER JOIN
			@extschema@.t_cluster AS t4
		ON
			t3.cluster = t4.id
		INNER JOIN
			@extschema@.f_p_plot AS t5
		ON
			t4.id = t5.cluster
		INNER JOIN
			@extschema@.t_target_data AS t6
		ON
			t5.gid = t6.plot
		INNER JOIN
			@extschema@.t_variable AS t7
		ON
			t6.target_variable = t7.target_variable AND
			CASE WHEN t6.area_domain_category IS NULL THEN t7.area_domain_category IS NULL
			ELSE t6.area_domain_category = t7.area_domain_category END AND
			CASE WHEN t6.sub_population_category IS NULL THEN t7.sub_population_category IS NULL
			ELSE t6.sub_population_category = t7.sub_population_category END
		INNER JOIN
			@extschema@.t_reference_year_set AS t8
		ON
			t6.reference_year_set = t8.id
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t9
		ON
			t2.id = t9.panel AND
			t8.id = t9.reference_year_set
		WHERE
			array[t1.id] <@ _stratas AND
			t7.id = 1 AND 
			(t8.reference_date_begin >= $2 AND
			t8.reference_date_end <= $3)
		GROUP BY
			t1.id, t2.id, t6.reference_year_set
	)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel) AS panels,
		array_agg(reference_year_set) AS refyearsets
	FROM
		(SELECT
			stratum, panel, reference_year_set,
			total,
			max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
		FROM
			w_data
		) AS t1
	WHERE
		-- pick up the most dense panel with target variable
		total = max_total
	INTO _stratas_wp, _panels, _refyearsets;

	IF _stratas != _stratas_wp
	THEN
		RAISE EXCEPTION 'Not all stratas are covered with the specified target variable!';
	END IF;

-- insert into table t_panel2total_2ndph_est_data
INSERT INTO @extschema@.t_panel2total_2ndph_est_data (total_estimate_data, panel, reference_year_set)
SELECT
	_total_estimate_data_2nd, panel, reference_year_set
FROM
	unnest(_panels) WITH ORDINALITY AS t1(panel, id)
INNER JOIN
	unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
ON
	t1.id = t2.id;


INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
SELECT 1, _total_estimate_conf, NULL;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_1p_est_configuration() IS '.';

-- </function>

-- <function name="fn_2p_est_configuration" schema="extschema" src="functions/extschema/fn_2p_est_configuration.sql">
--
-- Copyright 2017, 2020 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: @extschema@.fn_2p_est_configuration(integer, regclass)
--DROP FUNCTION @extschema@.fn_2p_est_configuration(integer,date,date,character varying,integer,integer);
CREATE OR REPLACE FUNCTION @extschema@.fn_2p_est_configuration(_estimation_cell integer, _estimate_date_begin date, _estimate_date_end date, _note varchar, _target_variable integer, _aux_conf integer)
RETURNS integer
AS
$function$
DECLARE
_total_estimate_conf		integer;
_total_estimate_data_2nd	integer;
_total_estimate_data_aux	integer;
_stratas			integer[];
_stratas_wp			integer[];
_panels				integer[];
_refyearsets			integer[];
_panels_aux			integer[];
_param_area			integer;
_param_area_code		varchar;
_target_label			varchar;
_model				integer;
_cell				varchar;
BEGIN

_target_label := (
		SELECT		replace(
					replace(
						concat(coalesce(t2.label,'x'), ',', coalesce(t3.label,'x'), ',', coalesce(t4.label,'x'), ',', coalesce(t5.label,'x')),
					'x,',''),
			       	',x','') AS label
		FROM		@extschema@.t_variable AS t1
		LEFT JOIN 	@extschema@.c_target_variable AS t2 ON t1.target_variable = t2.id
		LEFT JOIN	@extschema@.c_sub_population_category AS t3 ON t1.sub_population_category = t3.id
		LEFT JOIN	@extschema@.c_area_domain_category AS t4 ON t1.area_domain_category = t4.id
		LEFT JOIN	@extschema@.c_auxiliary_variable_category AS t5 ON t1.auxiliary_variable_category = t5.id
		WHERE
			t1.id = $5
		);

_param_area := (SELECT param_area FROM @extschema@.t_aux_conf WHERE id = $6);
_model := (SELECT model FROM @extschema@.t_aux_conf WHERE id = $6);
_param_area_code := (SELECT param_area_code FROM @extschema@.f_a_param_area WHERE gid = _param_area);
_cell := (SELECT label FROM @extschema@.c_estimation_cell WHERE id = $1);

-- insert into table t_total_estimate_conf
INSERT INTO @extschema@.t_total_estimate_conf (estimation_cell, estimate_date_begin, estimate_date_end, total_estimate_conf, target_variable, phase_estimate_type)
VALUES
	($1, $2, $3, concat('2p;T=',_target_label,';Cell=',_cell,';PA_code=',_param_area_code, ';model=',_model,_note), $5, 2 )
RETURNING id
INTO _total_estimate_conf;

-- insert into table t_total_estimate_data
-- 2nd phase
INSERT INTO @extschema@.t_total_estimate_data(total_estimate_conf, variable, phase, aux_phase_type, cor_phase, aux_conf)
VALUES
	(_total_estimate_conf, $5, 2, NULL, NULL, NULL)
RETURNING id
INTO _total_estimate_data_2nd;


-- zero phase
INSERT INTO @extschema@.t_total_estimate_data(total_estimate_conf, variable, phase, aux_phase_type, cor_phase, aux_conf)
VALUES
	(_total_estimate_conf, $5, 0, 1, 2, $6)
RETURNING id
INTO _total_estimate_data_aux;


-- test on param_area_coverage
	SELECT
		array_agg(t1.id ORDER BY t1.id)
	FROM
		@extschema@.t_stratum AS t1
	INNER JOIN
		@extschema@.f_a_param_area AS t2
	ON
		-- buffered stratum?
		-- no, if only buffer of the stratum would intersect the cell, 
		-- no consequences on estimate would be introduced, the buffer is in general used only for the edge effect compensation (the compensated area is within non-buffered stratum)
		ST_Intersects(t1.geom, t2.geom)
	WHERE
		t2.gid = _param_area
	INTO _stratas;


-- existing panels configured in panel2aux_conf
	SELECT
		array_agg(panel ORDER BY panel)
	FROM
		@extschema@.t_panel2aux_conf AS t1
	WHERE
		t1.aux_conf = $6
	INTO _panels_aux;


-- check of panel2total_2ndph
-- and addition of panels from param_area - is the target variable available not only in cell?

	WITH w_data AS (
		SELECT
			t1.id AS stratum, t2.id AS panel, t6.reference_year_set, count(*) AS total
		FROM
			@extschema@.t_stratum AS t1
		INNER JOIN
			@extschema@.t_panel AS t2
		ON
			t1.id = t2.stratum
		INNER JOIN
			@extschema@.cm_cluster2panel_mapping AS t3
		ON
			t2.id = t3.panel
		INNER JOIN
			@extschema@.t_cluster AS t4
		ON
			t3.cluster = t4.id
		INNER JOIN
			@extschema@.f_p_plot AS t5
		ON
			t4.id = t5.cluster
		INNER JOIN
			@extschema@.t_target_data AS t6
		ON
			t5.gid = t6.plot
		INNER JOIN
			@extschema@.t_variable AS t7
		ON
			t6.target_variable = t7.target_variable AND
			CASE WHEN t6.area_domain_category IS NULL THEN t7.area_domain_category IS NULL
			ELSE t6.area_domain_category = t7.area_domain_category END AND
			CASE WHEN t6.sub_population_category IS NULL THEN t7.sub_population_category IS NULL
			ELSE t6.sub_population_category = t7.sub_population_category END
		INNER JOIN
			@extschema@.t_reference_year_set AS t8
		ON
			t6.reference_year_set = t8.id
		INNER JOIN
			@extschema@.cm_refyearset2panel_mapping AS t9
		ON
			t2.id = t9.panel AND
			t8.id = t9.reference_year_set
		WHERE
			array[t1.id] <@ _stratas AND
			t7.id = 1 AND 
			(t8.reference_date_begin >= $2 AND
			t8.reference_date_end <= $3)
		GROUP BY
			t1.id, t2.id, t6.reference_year_set
	)
	SELECT
		array_agg(DISTINCT stratum ORDER BY stratum) AS stratas,
		array_agg(panel) AS panels,
		array_agg(reference_year_set) AS refyearsets
	FROM
		(SELECT
			stratum, panel, reference_year_set,
			total,
			max(total) OVER(PARTITION BY stratum, panel, reference_year_set) AS max_total
		FROM
			w_data
		) AS t1
	WHERE
		-- pick up the most dense panel with target variable
		total = max_total
	INTO _stratas_wp, _panels, _refyearsets;

	IF _panels != _panels_aux
	THEN
		RAISE EXCEPTION 'Not all panels coming from g_beta have available target variable!';
	END IF;

-- insert into table t_panel2total_2ndph_est_data
INSERT INTO @extschema@.t_panel2total_2ndph_est_data (total_estimate_data, panel, reference_year_set)
SELECT
	_total_estimate_data_2nd, panel, reference_year_set
FROM
	unnest(_panels) WITH ORDINALITY AS t1(panel, id)
INNER JOIN
	unnest(_refyearsets) WITH ORDINALITY AS t2(reference_year_set,id)
ON
	t1.id = t2.id;

INSERT INTO @extschema@.t_estimate_conf (estimate_type, total_estimate_conf, denominator)
SELECT 1, _total_estimate_conf, NULL;

RETURN _total_estimate_conf;


END;
$function$
LANGUAGE plpgsql
PARALLEL SAFE;

--COMMENT ON FUNCTION @extschema@.fn_2p_est_configuration() IS '.';

-- </function>


